import React from 'react'
import MainLayout from '../../layout/MainLayout'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'
import DashboardAdmin from './DashboardAdmin'
import DashboardShop from './DashboardShop'

const Dashboard = () => {
  const role = StorageUtil.get(STORAGE_KEY.ROLE)

  return (
    <MainLayout>
      {role === 'admin' ? <DashboardAdmin /> : <DashboardShop />}
    </MainLayout>
  )
}

export default Dashboard
