import { Grid } from '@mui/material'
import React, { useEffect, useState } from 'react'
import TopOneShop from './TopOneShop'
import TopTenShop from './TopTenShop'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getTopTurnoverShop,
  selectTopTurnoverShop,
} from '../../features/dashboard/dashboardSlice'

const DashboardShop = () => {
  const dispatch = useAppDispatch()
  const [value, setValue] = useState(new Date().getMonth())

  const topTurnover = useAppSelector(selectTopTurnoverShop)

  useEffect(() => {
    const date = new Date()
    const firstDay = new Date(date.getFullYear(), value, 1).getTime()
    const lastDay = new Date(date.getFullYear(), value + 1, 0).getTime()

    dispatch(
      getTopTurnoverShop({
        dateFrom: Number(firstDay / 1000),
        dateTo: Number(lastDay / 1000),
      })
    )
  }, [value])

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={8}>
        <TopOneShop data={topTurnover} value={value} setValue={setValue} />
      </Grid>
      <Grid item xs={12} md={4}>
        <TopTenShop data={topTurnover} />
      </Grid>
    </Grid>
  )
}

export default DashboardShop
