import { Grid } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getTopTurnover,
  selectTopTurnover,
} from '../../features/dashboard/dashboardSlice'
import TopOne from './TopOne'
import TopTen from './TopTen'

const DashboardAdmin = () => {
  const dispatch = useAppDispatch()
  const [value, setValue] = useState(new Date().getMonth())
  const [position, setPosition] = useState(0)

  const topTurnover = useAppSelector(selectTopTurnover)

  useEffect(() => {
    const date = new Date()
    const firstDay = new Date(date.getFullYear(), value, 1).getTime()
    const lastDay = new Date(date.getFullYear(), value + 1, 0).getTime()

    dispatch(
      getTopTurnover({
        dateFrom: Number(firstDay / 1000),
        dateTo: Number(lastDay / 1000),
      })
    )
  }, [value])

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={8}>
        <TopOne
          data={topTurnover?.content}
          value={value}
          setValue={setValue}
          position={position}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <TopTen data={topTurnover?.content} setPosition={setPosition} />
      </Grid>
    </Grid>
  )
}

export default DashboardAdmin
