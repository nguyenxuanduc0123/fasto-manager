import { CardContent, Divider, Grid, Typography } from '@mui/material'
import React from 'react'
import MainCard from '../../ui-component/cards/MainCard'
import { formatPrice } from '../../util/helpers'

const TopTenShop = ({ data }) => {
  return (
    <MainCard content={false}>
      <CardContent>
        <Grid container spacing={2} justifyContent="center">
          <Grid item xs={12}>
            <Grid
              container
              alignContent="center"
              justifyContent="space-between"
            >
              <Grid item>
                <Typography variant="h4">Top 10 Product</Typography>
              </Grid>
            </Grid>
          </Grid>
          {data?.productResponseDto?.length > 0 ? (
            <>
              <Grid item xs={12}>
                {data?.productResponseDto?.map((item, i) => {
                  return (
                    <>
                      <Grid container direction="column">
                        <Grid item>
                          <Grid
                            container
                            alignItems="center"
                            justifyContent="space-between"
                          >
                            <Grid item>
                              <Typography variant="subtitle1" color="inherit">
                                {item?.productName}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Grid
                                container
                                alignItems="center"
                                justifyContent="space-between"
                              >
                                <Grid item>
                                  <Typography
                                    variant="subtitle1"
                                    color="inherit"
                                  >
                                    {item?.quantity}
                                  </Typography>
                                </Grid>
                              </Grid>
                            </Grid>
                            <Grid item>
                              <Grid
                                container
                                alignItems="center"
                                justifyContent="space-between"
                              >
                                <Grid item>
                                  <Typography
                                    variant="subtitle1"
                                    color="inherit"
                                  >
                                    {formatPrice(item?.totalOriginPrice)} VND
                                  </Typography>
                                </Grid>
                              </Grid>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                      {data.length - 1 !== i && <Divider sx={{ my: 1.5 }} />}
                    </>
                  )
                })}
              </Grid>
            </>
          ) : (
            <Grid item xs={12}>
              No Row
            </Grid>
          )}
        </Grid>
      </CardContent>
    </MainCard>
  )
}

export default TopTenShop
