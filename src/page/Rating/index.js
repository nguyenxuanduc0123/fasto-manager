import React from 'react'
import { useState } from 'react'
import RatingAPI from '../../api/rating/api'
import { useEffect } from 'react'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import { disableMenu } from '../../app/constant'
import { CardMedia } from '@mui/material'

const Rating = () => {
  const [listRating, setListRating] = useState({})

  const fetchListRating = async (page = 0, size = 10) => {
    const data = await RatingAPI.getListRating({ page, size })
    setListRating(data.data)
  }

  useEffect(() => {
    fetchListRating()
  }, [])

  const columns = [
    {
      field: 'userFirstName',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'userImage',
      headerName: 'Image',
      flex: 1,
      ...disableMenu,
      renderCell: (params) => (
        <CardMedia
          sx={{ objectFit: 'cover', width: 50, height: 45 }}
          component="img"
          src={`${params.value || `/images/logo.jpg`}`}
        />
      ),
    },
    {
      field: 'content',
      headerName: 'Content',
      flex: 2,
      disableColumnMenu: true,
    },
    {
      field: 'ratings',
      headerName: 'Rating',
      flex: 1,
      disableColumnMenu: true,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="List Rating">
        <AppDataGrid
          rows={listRating?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listRating?.totalElements || 0}
          page={listRating?.number}
          onPageChange={(newPage) => {
            fetchListRating({ page: newPage })
          }}
          rowsPerPageOptions={[10]}
          getRowId={(row) => row.userId + new Date().getTime()}
        />
      </MainCard>
    </MainLayout>
  )
}

export default Rating
