import { LoadingButton } from '@mui/lab'
import { Box, Button, Grid } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { useAppDispatch } from '../../app/hooks'
import { changePassword } from '../../features/authentication/authSlice'
import MainLayout from '../../layout/MainLayout'
import AppTextField from '../../ui-component/AppTextField'
import MainCard from '../../ui-component/cards/MainCard'

const ChangePassword = () => {
  const dispatch = useAppDispatch()
  const status = { processing: false }

  const formik = useFormik({
    initialValues: {
      confirmNewPassword: '',
      newPassword: '',
      currentPassword: '',
    },
    onSubmit: (values, { resetForm }) => {
      dispatch(changePassword(values))
      resetForm()
    },
  })

  return (
    <MainLayout>
      <MainCard content title="Change Password">
        <Box
          component="form"
          onSubmit={formik.handleSubmit}
          sx={{ p: 1 }}
          noValidate
        >
          <Grid container rowSpacing={2} xs={12}>
            <Grid item xs={12}>
              <AppTextField
                formik={formik}
                name="currentPassword"
                label="Current Password"
                type="password"
                required
              />
            </Grid>
            <Grid item xs={12}>
              <AppTextField
                formik={formik}
                name="newPassword"
                label="New Password"
                type="password"
                required
              />
            </Grid>
            <Grid item xs={12}>
              <AppTextField
                formik={formik}
                name="confirmNewPassword"
                label="Confirm New Password"
                type="password"
                required
              />
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
            marginTop={0.5}
            spacing={2}
          >
            <Grid item xs={2}>
              <Button
                color="primary"
                variant="outlined"
                fullWidth
                type="button"
                disabled={status.processing}
                onClick={() => formik.resetForm()}
              >
                Clear
              </Button>
            </Grid>

            <Grid item xs={2}>
              <LoadingButton
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                startIcon={<div></div>}
                disabled={status.processing}
                loading={status.processing}
                loadingPosition="start"
              >
                Submit
              </LoadingButton>
            </Grid>
          </Grid>
        </Box>
      </MainCard>
    </MainLayout>
  )
}

export default ChangePassword
