import { Search } from '@mui/icons-material'
import { Box, CardMedia, InputAdornment, TextField } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { disableMenu, statusStore } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  activeStore,
  blockStore,
  getListStore,
  selectListStore,
  selectListStoreQuery,
  setQuery,
} from '../../features/store/storeSlice'
import TableAction from '../../features/TableActions'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'
import DropdownSelect from '../../ui-component/DropdownSelect'

const Store = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [searchValue, setSearchValue] = useState('')
  const [idDelete, setIdDelete] = useState(0)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
  const [typeUpdate, setTypeUpdate] = useState('')

  const debouncedValue = useDebounce(searchValue)
  const listStoreQuery = useAppSelector(selectListStoreQuery)
  const listStore = useAppSelector(selectListStore)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      if (typeUpdate === 'ACTIVATED') {
        dispatch(blockStore({ id: idDelete }))
      } else {
        dispatch(activeStore({ id: idDelete }))
      }
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  useEffect(() => {
    fetchDataList({
      ...listStoreQuery,
      page: 0,
      query: debouncedValue,
      status: '',
    })
    return () => {
      dispatch(setQuery({}))
    }
  }, [dispatch, debouncedValue])

  const fetchDataList = ({
    page = 0,
    size = 10,
    query = '',
    sort = 'id,desc',
    ...other
  }) => {
    dispatch(getListStore({ page, size, query, sort, ...other }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'banner',
      headerName: 'Banner',
      flex: 1,
      ...disableMenu,
      renderCell: (params) => (
        <CardMedia
          sx={{ objectFit: 'cover', width: 50, height: 45 }}
          component="img"
          src={`${params.value || `/images/logo.jpg`}`}
        />
      ),
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'description',
      headerName: 'Description',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'phone',
      headerName: 'Phone',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      ...disableMenu,
      renderHeader: () => {
        return (
          <DropdownSelect
            label="Status"
            options={statusStore}
            value={listStoreQuery.status || ''}
            onChange={(value) =>
              dispatch(
                getListStore({ ...listStoreQuery, status: value, page: 0 })
              )
            }
          />
        )
      },
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showRecoverButton={row.status === 'BLOCKED'}
          showDeleteButton={row.status === 'ACTIVATED'}
          deleteAction={() => {
            setIdDelete(row.id)
            setTypeUpdate(row.status)
            setOpenDeleteDialog(true)
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="List Store">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search Store"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <AppDataGrid
          rows={listStore?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listStore?.totalElements || 0}
          page={listStore?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...listStoreQuery, page: newPage })
          }}
          rowsPerPageOptions={[10]}
          onRowClick={(row) => {
            navigate(`/store-detail/${row.id}`)
          }}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Delete Group"
          description="Are you sure, you want to block this user?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default Store
