import { Search } from '@mui/icons-material'
import { InputAdornment, TextField } from '@mui/material'
import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import { disableMenu } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  activeStore,
  getListStore,
  selectListStore,
  selectListStoreQuery,
  setQuery,
} from '../../features/store/storeSlice'
import TableAction from '../../features/TableActions'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'

const StoreRegister = () => {
  const dispatch = useAppDispatch()
  const listStoreRegister = useAppSelector(selectListStore)
  const query = useAppSelector(selectListStoreQuery)

  const [searchValue, setSearchValue] = useState('')

  const debouncedValue = useDebounce(searchValue)

  useEffect(() => {
    fetchDataList({
      ...query,
      page: 0,
      status: 'INACTIVE',
      query: debouncedValue,
    })
    return () => {
      dispatch(setQuery({}))
    }
  }, [dispatch, debouncedValue])

  const fetchDataList = ({ page = 0, size = 10, query = '', status = '' }) => {
    dispatch(getListStore({ page, size, query, status }))
  }

  const [idActive, setIdActive] = useState(0)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      dispatch(activeStore({ id: idActive }))
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'description',
      headerName: 'Description',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'phone',
      headerName: 'Phone',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showRecoverButton={row.status === 'INACTIVE'}
          deleteAction={() => {
            setIdActive(row.id)
            setOpenDeleteDialog(true)
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="List Store Register">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search Store Register"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <AppDataGrid
          rows={listStoreRegister?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listStoreRegister?.totalElements || 0}
          page={listStoreRegister?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...query, page: newPage })
          }}
          rowsPerPageOptions={[10]}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Active Group"
          description="Are you sure, you want to active this store?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default StoreRegister
