import { Search } from '@mui/icons-material'
import { Box, InputAdornment, TextField } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { disableMenu, statusUser } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import TableAction from '../../features/TableActions'
import {
  blockUser,
  getListUser,
  selectListUser,
  selectListUserQuery,
} from '../../features/user/userSlice'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'
import DropdownSelect from '../../ui-component/DropdownSelect'
import MainCard from '../../ui-component/cards/MainCard'

const User = () => {
  const dispatch = useAppDispatch()
  const [searchValue, setSearchValue] = useState('')
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)
  const [idDelete, setIdDelete] = useState(0)

  const debouncedValue = useDebounce(searchValue)

  const listUserQuery = useAppSelector(selectListUserQuery)
  const listUser = useAppSelector(selectListUser)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      dispatch(blockUser({ id: idDelete }))
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  useEffect(() => {
    fetchDataList({ ...listUserQuery, page: 0, query: debouncedValue })
  }, [dispatch, debouncedValue])

  const fetchDataList = ({ page = 0, size = 10, query = '', ...other }) => {
    dispatch(getListUser({ page, size, query, ...other }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'email',
      headerName: 'Email',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'phoneNumber',
      headerName: 'Phone Number',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'provider',
      headerName: 'Provider',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      renderHeader: () => {
        return (
          <DropdownSelect
            label="Status"
            options={statusUser}
            value={listUserQuery.status || ''}
            onChange={(value) =>
              fetchDataList({ ...listUserQuery, status: value, page: 0 })
            }
          />
        )
      },
      ...disableMenu,
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showDeleteButton={row.status === 'ACTIVATED'}
          deleteAction={() => {
            setIdDelete(row.id)
            setOpenDeleteDialog(true)
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="List User">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search user"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
        </Box>
        <AppDataGrid
          rows={listUser?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listUser?.totalElements || 0}
          page={listUser?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...listUserQuery, page: newPage })
          }}
          rowsPerPageOptions={[10]}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Delete Group"
          description="Are you sure, you want to block this user?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default User
