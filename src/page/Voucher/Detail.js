import { Avatar, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getDetailVoucherStore,
  selectDetailVoucherStore,
} from '../../features/store/storeSlice'
import { useAppTheme } from '../../features/theme/hooks'
import {
  getDetailVoucher,
  selectDetailVoucher,
} from '../../features/voucher/voucherSlice'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'

const Detail = ({ isAdmin }) => {
  const dispatch = useAppDispatch()
  const theme = useAppTheme()

  const detailVoucher = useAppSelector(
    isAdmin ? selectDetailVoucherStore : selectDetailVoucher
  )

  const { voucherId } = useParams()
  useEffect(() => {
    if (voucherId) {
      if (isAdmin) {
        dispatch(getDetailVoucherStore({ id: +voucherId }))
      } else {
        dispatch(getDetailVoucher({ id: +voucherId }))
      }
    }
  }, [dispatch, voucherId])

  return (
    <MainLayout>
      <MainCard content isBack title="Voucher Detail">
        <Box
          sx={{
            borderRadius: '5px',
            border: `2px dashed ${theme.palette.grey[500]}`,
            height: '100%',
          }}
          pt={3}
          pb={2}
          mb={3}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            mb={2}
          >
            <Stack direction="row" justifyContent="center">
              <Avatar
                sx={{ width: '200px', height: '200px' }}
                src={detailVoucher?.image || '-'}
                alt="user_image"
              >
                {detailVoucher?.name || '-'}
              </Avatar>
            </Stack>
            <Typography variant="h2" p={2}>
              {detailVoucher?.name || '-'}
            </Typography>
          </Box>

          <Box
            sx={{
              display: 'flex',
              textAlign: 'center',
              justifyContent: 'space-evenly',
              flexWrap: 'wrap',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Count User Voucher
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.countUserVoucher || 0}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                limitPerUser
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.limitPerUser || 0}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Max Discount
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.maxDiscount || '-'}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Status
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.status || '-'}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Quantity
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.quantity || 0}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Value Discount
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.valueDiscount || 0}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Value Need
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.valueNeed || 0}
              </Typography>
            </Box>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Voucher Type
              </Typography>
              <Typography variant="body1" noWrap>
                {detailVoucher?.voucherType || 0}
              </Typography>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
          p={1}
        >
          <Typography
            variant="h4"
            sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
          >
            Description
          </Typography>
          <Typography variant="body1" pl={2}>
            {detailVoucher?.description || '-'}
          </Typography>
        </Box>
      </MainCard>
    </MainLayout>
  )
}

export default Detail
