import { Search } from '@mui/icons-material'
import { Box, InputAdornment, TextField } from '@mui/material'
import { format } from 'date-fns'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { disableMenu, statusVoucher } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import TableAction from '../../features/TableActions'
import {
  deleteVoucher,
  getListVoucher,
  selectListVoucher,
  selectListVoucherQuery,
} from '../../features/voucher/voucherSlice'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import { RedirectButton } from '../../ui-component/Button'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'
import DropdownSelect from '../../ui-component/DropdownSelect'
import MainCard from '../../ui-component/cards/MainCard'

const Voucher = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [searchValue, setSearchValue] = useState('')
  const [idDelete, setIdDelete] = useState(0)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

  const debouncedValue = useDebounce(searchValue)

  const listVoucher = useAppSelector(selectListVoucher)
  const query = useAppSelector(selectListVoucherQuery)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      dispatch(deleteVoucher({ id: idDelete }))
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  useEffect(() => {
    fetchDataList({ ...query, page: 0, query: debouncedValue })
  }, [dispatch, debouncedValue])

  const fetchDataList = ({
    sort = 'id,desc',
    page = 0,
    size = 10,
    query = '',
  }) => {
    dispatch(getListVoucher({ page, size, query, sort }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'voucherType',
      headerName: 'Type',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'valueDiscount',
      headerName: 'Value Discount',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'startedAt',
      headerName: 'Started At',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        const modifiedDate = new Date(params?.row.startedAt).getTime()
        return `${format(modifiedDate || new Date(), 'yyyy-MM-dd')} `
      },
    },
    {
      field: 'endedAt',
      headerName: 'Ended At',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        const modifiedDate = new Date(params?.row.endedAt).getTime()
        return `${format(modifiedDate || new Date(), 'yyyy-MM-dd')} `
      },
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      ...disableMenu,
      renderHeader: () => {
        return (
          <DropdownSelect
            label="Status"
            options={statusVoucher}
            value={query.status || ''}
            onChange={(value) =>
              dispatch(getListVoucher({ ...query, status: value, page: 0 }))
            }
          />
        )
      },
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showDeleteButton={row.status !== 'DELETED'}
          // showRecoverButton={row.deleteFlag === true}
          deleteAction={() => {
            setIdDelete(row.id)
            setOpenDeleteDialog(true)
          }}
          edit={{
            href: `/voucher/edit/${row.id}`,
            as: `/voucher/edit/${row.id}`,
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="Voucher">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search Voucher"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          <RedirectButton href="/voucher/create" text="Make Voucher" />
        </Box>
        <AppDataGrid
          rows={listVoucher?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listVoucher?.totalElements || 0}
          page={listVoucher?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...query, page: newPage })
          }}
          rowsPerPageOptions={[10]}
          onRowClick={(row) => {
            navigate(`/voucher/detail/${row?.id}`)
          }}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Delete Group"
          description="Are you sure, you want to delete this voucher?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default Voucher
