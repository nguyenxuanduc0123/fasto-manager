import DateRangeOutlinedIcon from '@mui/icons-material/DateRangeOutlined'
import { LoadingButton } from '@mui/lab'
import {
  Box,
  Button,
  ClickAwayListener,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  MenuItem,
  TextField,
} from '@mui/material'
import { MobileDateTimePicker } from '@mui/x-date-pickers'
import { useFormik } from 'formik'
import React, { useState } from 'react'
import { imageType, statusDiscountType } from '../../../app/constant'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  createVoucher,
  editVoucher,
  selectStatusCreateVoucher,
  selectStatusEditVoucher,
} from '../../../features/voucher/voucherSlice'
import AppTextField from '../../../ui-component/AppTextField'
import Dropzone from '../../../ui-component/Dropzone/Dropzone'
import { NumberFormatCustom } from '../../../ui-component/NumberFormatField'
import { handleRemoveImage } from '../../../util/helpers'
import { voucherSchema } from '../../../validate/voucher'

const initialValues = {
  description: '',
  endedAt: new Date().getTime(),
  startedAt: new Date().getTime(),
  limitPerUser: 0,
  maxDiscount: 0,
  name: '',
  quantity: 0,
  status: 'ACTIVATED',
  valueDiscount: 0,
  valueNeed: 0,
  voucherType: 'PERCENT',
  image: [
    {
      url: '',
      type: '',
    },
  ],
}

const UpdateForm = ({ initValue }) => {
  const dispatch = useAppDispatch()

  const [showSelectDateFrom, setShowSelectDateFrom] = useState(false)
  const [showSelectDateTo, setShowSelectDateTo] = useState(false)

  const convertToFormValues = (values) => {
    const endedAt = new Date(values.endedAt).getTime()
    const startedAt = new Date(values.startedAt).getTime()
    const image = [
      {
        url: values.image,
        type: '',
      },
    ]
    return { ...values, image, endedAt, startedAt }
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initValue ? convertToFormValues(initValue) : initialValues,
    validationSchema: voucherSchema,
    onSubmit: (values) => {
      const newForm = convertData(values)
      if (initValue) {
        dispatch(editVoucher({ ...newForm }))
      } else {
        dispatch(createVoucher({ ...newForm }))
      }
    },
  })

  const convertData = (values) => {
    const image = values.image[0].url
    const endedAt = Math.round(values.endedAt / 1000)
    const startedAt = Math.round(values.startedAt / 1000)
    return {
      ...values,
      endedAt,
      startedAt,
      image,
    }
  }

  const status = useAppSelector(
    initValue ? selectStatusEditVoucher : selectStatusCreateVoucher
  )

  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ p: 1 }}
      noValidate
    >
      <Grid container spacing={2} xs={12}>
        <Grid item xs={12}>
          <Box
            display="flex"
            flexWrap="wrap"
            sx={{
              justifyContent: 'center',
              marginBottom: { xs: '20px', sm: '0px' },
            }}
          >
            {formik.values.image.map((media, index) => (
              <Box key={index} sx={{ position: 'relative' }}>
                <Dropzone
                  values={formik.values}
                  setFieldValue={formik.setFieldValue}
                  setFieldError={formik.setFieldError}
                  fieldValue="image"
                  acceptFileType={imageType}
                  maxFile={1}
                  media={media}
                  index={index}
                  handleClickMedia={null}
                  handleRemoveMedia={handleRemoveImage('image', formik)}
                  handleEditImage={null}
                  label="Voucher image"
                />
              </Box>
            ))}
          </Box>
          <Box justifyContent="center" display="flex">
            {formik.errors['image'] && (
              <FormHelperText error={Boolean(formik.errors['image'])}>
                {formik.errors.image}
              </FormHelperText>
            )}
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <AppTextField formik={formik} name="name" label="Name" required />
        </Grid>

        <Grid item xs={12} md={6}>
          <TextField
            label="Value Need"
            value={formik.values['valueNeed']}
            onChange={(event) => {
              const value = event.target.value
              formik.setFieldValue('valueNeed', Number(value))
            }}
            name="valueNeed"
            required
            fullWidth
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
          {formik.touched['valueNeed'] && (
            <FormHelperText error={Boolean(formik.errors['valueNeed'])}>
              {formik.errors['valueNeed']}
            </FormHelperText>
          )}
        </Grid>

        <Grid item xs={12} md={6}>
          <TextField
            label="Limit per User"
            value={formik.values['limitPerUser']}
            onChange={(event) => {
              const value = event.target.value
              formik.setFieldValue('limitPerUser', Number(value))
            }}
            name="limitPerUser"
            required
            fullWidth
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
          {formik.touched['limitPerUser'] && (
            <FormHelperText error={Boolean(formik.errors['limitPerUser'])}>
              {formik.errors['limitPerUser']}
            </FormHelperText>
          )}
        </Grid>

        <Grid item xs={12} md={6}>
          <TextField
            label="MaxDiscount"
            value={formik.values['maxDiscount']}
            onChange={(event) => {
              const value = event.target.value
              formik.setFieldValue('maxDiscount', Number(value))
            }}
            name="maxDiscount"
            required
            fullWidth
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
          {formik.touched['maxDiscount'] && (
            <FormHelperText error={Boolean(formik.errors['maxDiscount'])}>
              {formik.errors['maxDiscount']}
            </FormHelperText>
          )}
        </Grid>

        <Grid item xs={12} md={6}>
          <TextField
            label="Quantity"
            value={formik.values['quantity']}
            onChange={(event) => {
              const value = event.target.value
              formik.setFieldValue('quantity', Number(value))
            }}
            name="quantity"
            required
            fullWidth
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
          />
          {formik.touched['quantity'] && (
            <FormHelperText error={Boolean(formik.errors['quantity'])}>
              {formik.errors['quantity']}
            </FormHelperText>
          )}
        </Grid>

        {formik.values.voucherType === 'PERCENT' ? (
          <ClickAwayListener
            mouseEvent="onMouseDown"
            touchEvent="onTouchStart"
            onClickAway={() =>
              !formik.values['valueDiscount'] &&
              formik.setFieldValue('valueDiscount', 1)
            }
          >
            <Grid item xs={12} md={6}>
              <AppTextField
                formik={formik}
                label="Value Discount"
                name="valueDiscount"
                value={formik.values['valueDiscount']}
                inputProps={{
                  min: 1,
                  max: 100,
                }}
                onChange={(e) => {
                  if (parseInt(e.target.value) <= 0)
                    formik.setFieldValue('valueDiscount', 0)
                  else if (parseInt(e.target.value) > 100)
                    formik.setFieldValue('valueDiscount', 100)
                  else
                    formik.setFieldValue(
                      'valueDiscount',
                      parseInt(e.target.value || 0)
                    )
                }}
                required
                endAdornment={
                  <InputAdornment position="end">
                    <TextField
                      select
                      disabled={formik.values['valueDiscount'] > 100}
                      value={formik.values['voucherType'] || 'PERCENT'}
                      onChange={(event) => {
                        formik.setFieldValue('voucherType', event.target.value)
                      }}
                      variant="standard"
                      sx={{
                        '& .MuiInput-root': {
                          '&:before, :after, :hover:not(.Mui-disabled):before':
                            {
                              borderBottom: 0,
                            },
                        },
                      }}
                    >
                      {statusDiscountType.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </InputAdornment>
                }
              />
            </Grid>
          </ClickAwayListener>
        ) : (
          <Grid item xs={12} md={6}>
            <TextField
              label="Value Discount"
              value={Number(formik.values['valueDiscount'])}
              onChange={(event) =>
                formik.setFieldValue(
                  'valueDiscount',
                  Math.abs(Number(event.target.value))
                )
              }
              required
              fullWidth
              InputProps={{
                inputComponent: NumberFormatCustom,
                endAdornment: (
                  <InputAdornment position="end">
                    <TextField
                      select
                      disabled={formik.values['discount'] > 100}
                      value={formik.values['voucherType'] || 'PERCENT'}
                      onChange={(event) => {
                        formik.setFieldValue('voucherType', event.target.value)
                      }}
                      variant="standard"
                      sx={{
                        '& .MuiInput-root': {
                          '&:before, :after, :hover:not(.Mui-disabled):before':
                            {
                              borderBottom: 0,
                            },
                        },
                      }}
                    >
                      {statusDiscountType.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  </InputAdornment>
                ),
              }}
            />
            {formik.touched['valueDiscount'] && (
              <FormHelperText error={Boolean(formik.errors['valueDiscount'])}>
                {formik.errors['valueDiscount']}
              </FormHelperText>
            )}
          </Grid>
        )}

        {!initValue && (
          <Grid item xs={12} md={6}>
            <MobileDateTimePicker
              ampm={false}
              label="Started At"
              inputFormat="yyyy/MM/dd - HH:mm"
              value={formik.values.startedAt}
              onChange={(newValue) => {
                formik.setFieldValue('startedAt', newValue.getTime())
              }}
              open={showSelectDateFrom}
              onOpen={() => setShowSelectDateFrom(true)}
              onClose={() => setShowSelectDateFrom(false)}
              showToolbar={false}
              InputProps={{
                endAdornment: (
                  <InputAdornment
                    position="end"
                    onClick={() => setShowSelectDateFrom(true)}
                  >
                    <IconButton edge="end" size="large" sx={{ mr: '5px' }}>
                      <DateRangeOutlinedIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              renderInput={(params) => (
                <TextField
                  required
                  fullWidth
                  name="startedAt"
                  {...params}
                  error={
                    formik.touched['startedAt'] &&
                    Boolean(formik.errors['startedAt'])
                  }
                  helperText={
                    formik.touched['startedAt'] && formik.errors['startedAt']
                  }
                />
              )}
            />
          </Grid>
        )}

        <Grid item xs={12} md={6}>
          <MobileDateTimePicker
            ampm={false}
            label="Ended At"
            inputFormat="yyyy/MM/dd - HH:mm"
            value={formik.values.endedAt}
            onChange={(newValue) => {
              formik.setFieldValue('endedAt', newValue.getTime())
            }}
            open={showSelectDateTo}
            onOpen={() => setShowSelectDateTo(true)}
            onClose={() => setShowSelectDateTo(false)}
            showToolbar={false}
            InputProps={{
              endAdornment: (
                <InputAdornment
                  position="end"
                  onClick={() => setShowSelectDateTo(true)}
                >
                  <IconButton edge="end" size="large" sx={{ mr: '5px' }}>
                    <DateRangeOutlinedIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
            renderInput={(params) => (
              <TextField
                required
                fullWidth
                name="endedAt"
                {...params}
                error={
                  formik.touched['endedAt'] && Boolean(formik.errors['endedAt'])
                }
                helperText={
                  formik.touched['endedAt'] && formik.errors['endedAt']
                }
              />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <AppTextField
            formik={formik}
            name="description"
            label="Description"
            required
          />
        </Grid>

        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
            marginTop={0.5}
            spacing={2}
            xs={12}
          >
            <Grid item xs={3}>
              <Button
                color="primary"
                variant="outlined"
                fullWidth
                type="button"
                disabled={status.processing}
                onClick={() => formik.resetForm()}
              >
                Clear
              </Button>
            </Grid>

            <Grid item xs={3}>
              <LoadingButton
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                startIcon={<div></div>}
                disabled={status.processing}
                loading={status.processing}
                loadingPosition="start"
              >
                Submit
              </LoadingButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default UpdateForm
