import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  getDetailVoucher,
  selectDetailVoucher,
} from '../../../features/voucher/voucherSlice'
import MainLayout from '../../../layout/MainLayout'
import MainCard from '../../../ui-component/cards/MainCard'
import UpdateForm from './UpdateForm'

const Edit = () => {
  const dispatch = useAppDispatch()
  const { voucherId } = useParams()

  const voucherDetail = useAppSelector(selectDetailVoucher)

  useEffect(() => {
    if (voucherId) {
      dispatch(getDetailVoucher({ id: +voucherId }))
    }
  }, [dispatch, voucherId])

  return (
    <MainLayout>
      <MainCard content isBack title="Edit Voucher">
        {voucherDetail?.id && <UpdateForm initValue={voucherDetail} />}
      </MainCard>
    </MainLayout>
  )
}

export default Edit
