import React from 'react'
import MainLayout from '../../../layout/MainLayout'
import MainCard from '../../../ui-component/cards/MainCard'
import UpdateForm from './UpdateForm'

const Create = () => {
  return (
    <MainLayout>
      <MainCard content isBack title="Make Voucher">
        <UpdateForm />
      </MainCard>
    </MainLayout>
  )
}

export default Create
