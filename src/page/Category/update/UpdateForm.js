import { LoadingButton } from '@mui/lab'
import { Box, Button, FormHelperText, Grid } from '@mui/material'
import { useFormik } from 'formik'
import React from 'react'
import { imageType } from '../../../app/constant'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  createCategory,
  editCategory,
  selectStatusCreateCategory,
  selectStatusEditCategory,
} from '../../../features/category/categorySlice'
import AppTextField from '../../../ui-component/AppTextField'
import Dropzone from '../../../ui-component/Dropzone/Dropzone'
import { handleRemoveImage } from '../../../util/helpers'
import { categorySchema } from '../../../validate/category'

const initialValues = {
  name: '',
  image: [
    {
      url: '',
      type: '',
    },
  ],
}

const UpdateForm = ({ initValue }) => {
  const dispatch = useAppDispatch()

  const convertToFormValues = (values) => {
    const image = [
      {
        url: values.categoryImage,
        type: '',
      },
    ]
    return { ...values, image }
  }

  const status = useAppSelector(
    initValue ? selectStatusEditCategory : selectStatusCreateCategory
  )

  const convertData = (values) => {
    const categoryImage = values.image[0].url
    return {
      ...values,
      categoryImage,
    }
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initValue ? convertToFormValues(initValue) : initialValues,
    validationSchema: categorySchema,
    onSubmit: (values) => {
      const formData = convertData(values)
      if (initValue) {
        dispatch(editCategory({ ...formData }))
      } else {
        dispatch(createCategory({ ...formData }))
      }
    },
  })
  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ p: 1 }}
      noValidate
    >
      <Grid container spacing={2}>
        <Grid flex={1} minWidth="180px">
          <Grid item xs={12}>
            <Box
              display="flex"
              flexWrap="wrap"
              sx={{
                justifyContent: 'center',
                marginBottom: { xs: '20px', sm: '0px' },
              }}
            >
              {formik.values.image.map((media, index) => (
                <Box sx={{ position: 'relative', mr: 1, mb: 1 }} key={index}>
                  <Dropzone
                    values={formik.values}
                    setFieldValue={formik.setFieldValue}
                    setFieldError={formik.setFieldError}
                    fieldValue="image"
                    acceptFileType={imageType}
                    maxFile={1}
                    media={media}
                    index={index}
                    handleClickMedia={null}
                    handleRemoveMedia={handleRemoveImage('image', formik)}
                    handleEditImage={null}
                    label={'Image'}
                  />
                </Box>
              ))}
            </Box>
            <Box
              sx={{ justifyContent: { xs: 'center', sm: 'start' } }}
              display="flex"
              mb={2}
            >
              {formik.errors['image'] && (
                <FormHelperText error={Boolean(formik.errors['image'])}>
                  {formik.errors.image}
                </FormHelperText>
              )}
            </Box>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <AppTextField formik={formik} name="name" label="Name" />
        </Grid>

        <Grid
          container
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
          marginTop={0.5}
          spacing={2}
        >
          <Grid item xs={2}>
            <Button
              color="primary"
              variant="outlined"
              fullWidth
              type="button"
              disabled={status.processing}
              onClick={() => formik.resetForm()}
            >
              Clear
            </Button>
          </Grid>

          <Grid item xs={2}>
            <LoadingButton
              color="primary"
              variant="contained"
              fullWidth
              type="submit"
              startIcon={<div></div>}
              disabled={status.processing}
              loading={status.processing}
              loadingPosition="start"
            >
              Submit
            </LoadingButton>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default UpdateForm
