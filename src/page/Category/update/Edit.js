import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  getDetailCategory,
  selectDetailCategory,
} from '../../../features/category/categorySlice'
import MainLayout from '../../../layout/MainLayout'
import MainCard from '../../../ui-component/cards/MainCard'
import UpdateForm from './UpdateForm'

const Edit = () => {
  const dispatch = useAppDispatch()
  const { categoryId } = useParams()

  const categoryDetail = useAppSelector(selectDetailCategory)

  useEffect(() => {
    if (categoryId) {
      dispatch(getDetailCategory({ id: +categoryId }))
    }
  }, [dispatch, categoryId])

  return (
    <MainLayout>
      <MainCard content isBack title="Edit Category">
        {categoryDetail?.id && <UpdateForm initValue={categoryDetail} />}
      </MainCard>
    </MainLayout>
  )
}

export default Edit
