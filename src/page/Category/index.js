import { Box, CardMedia, InputAdornment, TextField } from '@mui/material'
import React, { useEffect, useState } from 'react'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import { RedirectButton } from '../../ui-component/Button'
import { Search } from '@mui/icons-material'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import useDebounce from '../../hooks/useDebounce'
import {
  deleteCategory,
  getListCategory,
  selectListCategory,
  selectListCategoryQuery,
} from '../../features/category/categorySlice'
import TableAction from '../../features/TableActions'
import { disableMenu } from '../../app/constant'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'
import { useNavigate } from 'react-router-dom'

const Category = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const [searchValue, setSearchValue] = useState('')

  const [idDelete, setIdDelete] = useState(0)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      dispatch(deleteCategory({ id: idDelete }))
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  const debouncedValue = useDebounce(searchValue)

  const query = useAppSelector(selectListCategoryQuery)
  const listCategory = useAppSelector(selectListCategory)

  useEffect(() => {
    fetchDataList({ ...query, page: 0, query: debouncedValue })
  }, [dispatch, debouncedValue])

  const fetchDataList = ({
    page = 0,
    size = 10,
    query = '',
    sort = 'id,desc',
  }) => {
    dispatch(getListCategory({ page, size, query, sort }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'categoryImage',
      headerName: 'Image',
      flex: 1,
      ...disableMenu,
      renderCell: (params) => (
        <CardMedia
          sx={{ objectFit: 'cover', width: 50, height: 45 }}
          component="img"
          src={`${params.value || `/images/logo.jpg`}`}
        />
      ),
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'deleteFlag',
      headerName: 'Delete',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showDeleteButton={row.deleteFlag === false}
          // showRecoverButton={row.deleteFlag === true}
          deleteAction={() => {
            setIdDelete(row.id)
            setOpenDeleteDialog(true)
          }}
          edit={{
            href: `/category/edit/${row.id}`,
            as: `/category/edit/${row.id}`,
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="List Category">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search category"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          <RedirectButton href="/category/create" text="Make Category" />
        </Box>
        <AppDataGrid
          rows={listCategory?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listCategory?.totalElements || 0}
          page={listCategory?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...query, page: newPage })
          }}
          rowsPerPageOptions={[10]}
          onRowClick={(row) => {
            navigate(`/category/edit/${row?.id}`)
          }}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Delete Group"
          description="Are you sure, you want to delete this category?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default Category
