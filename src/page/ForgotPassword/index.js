import * as React from 'react'
import Box from '@mui/material/Box'
import Stepper from '@mui/material/Stepper'
import Step from '@mui/material/Step'
import StepLabel from '@mui/material/StepLabel'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import { FormControl, Grid, InputLabel, OutlinedInput } from '@mui/material'
import AuthWrapper1 from '../../features/authentication/AuthWrapper'
import MainCard from '../../ui-component/cards/MainCard'
import { useAppTheme } from '../../features/theme/hooks'
import { useState } from 'react'
import UserAPI from '../../api/authentication/api'
import { useNavigate } from 'react-router-dom'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'

const steps = ['Enter Email', 'Verify Code', 'Reset Password', 'Finish']

const ForgotPassword = () => {
  const theme = useAppTheme()
  const navigate = useNavigate()

  const [activeStep, setActiveStep] = useState(0)
  const [inputValue, setInputValue] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const handleNext = async () => {
    switch (activeStep) {
      case 0:
        await UserAPI.forgotPassword({ email: inputValue })
        setInputValue('')
        break
      case 1:
        const res = await UserAPI.verifyCode({ code: inputValue })
        setInputValue('')
        StorageUtil.set(STORAGE_KEY.JWT, res.data)
        break
      case 2:
        await UserAPI.resetPassword({
          confirmNewPassword: newPassword,
          newPassword: confirmPassword,
        })
        break
      case 3:
        navigate('/login')
        break
      default:
        break
    }
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const renderStep = () => {
    switch (activeStep) {
      case 0:
        return (
          <Box width="70%">
            <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
              <InputLabel htmlFor="outlined-adornment-email-login">
                Email Address
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-email-login"
                type="email"
                name="email"
                label="Email Address"
                required
                value={inputValue}
                onChange={(e) => setInputValue(e.target.value)}
              />
            </FormControl>
          </Box>
        )
      case 1:
        return (
          <Box width="70%">
            <Typography>Check your email to get verify code</Typography>
            <Box>
              <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Verify code
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="text"
                  name="Verify Code"
                  label="Verify Code"
                  required
                  value={inputValue}
                  onChange={(e) => setInputValue(e.target.value)}
                />
              </FormControl>
            </Box>
          </Box>
        )
      case 2:
        return (
          <Box width="70%">
            <Box>
              <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  New Password
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="password"
                  name="New Password"
                  label="New Password"
                  required
                  value={newPassword}
                  onChange={(e) => setNewPassword(e.target.value)}
                />
              </FormControl>
            </Box>

            <Box>
              <FormControl fullWidth sx={{ ...theme.typography.customInput }}>
                <InputLabel htmlFor="outlined-adornment-email-login">
                  Confirm New password
                </InputLabel>
                <OutlinedInput
                  id="outlined-adornment-email-login"
                  type="password"
                  name="Confirm New password"
                  label="Confirm New password"
                  required
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </FormControl>
            </Box>
          </Box>
        )
      case 3:
        return (
          <Box>
            <Typography variant="h3">Finished</Typography>
          </Box>
        )
      default:
        return (
          <Box>
            <Typography>{activeStep}</Typography>
          </Box>
        )
    }
  }

  return (
    <AuthWrapper1>
      <head>
        <title>Forgot Password</title>
      </head>
      <Grid
        container
        direction="column"
        justifyContent="flex-end"
        sx={{ minHeight: '100vh' }}
      >
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: 'calc(100vh - 68px)' }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <MainCard sx={{ width: '50vw' }}>
                <Box sx={{ width: '100%' }}>
                  <Stepper activeStep={activeStep}>
                    {steps.map((label, index) => {
                      const stepProps = {}
                      const labelProps = {}
                      return (
                        <Step key={label} {...stepProps}>
                          <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                      )
                    })}
                  </Stepper>
                  <React.Fragment>
                    <Box
                      mt={4}
                      sx={{ display: 'flex', justifyContent: 'center' }}
                    >
                      {renderStep()}
                    </Box>
                    <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                      <Button
                        color="inherit"
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        sx={{ mr: 1 }}
                      >
                        Back
                      </Button>
                      <Box sx={{ flex: '1 1 auto' }} />

                      <Button onClick={handleNext}>
                        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                      </Button>
                    </Box>
                  </React.Fragment>
                </Box>
              </MainCard>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </AuthWrapper1>
  )
}

export default ForgotPassword
