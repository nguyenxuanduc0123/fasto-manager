// material-ui
import {
  Avatar,
  Box,
  Divider,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
} from '@mui/material'

// project imports
import AuthLogin from '../../features/authentication/auth-forms/AuthLogin'
import AuthCardWrapper from '../../features/authentication/AuthCardWrapper'
import AuthWrapper1 from '../../features/authentication/AuthWrapper'
import { useAppTheme } from '../../features/theme/hooks'

// assets
import AuthFooter from '../../ui-component/cards/AuthFooter'

const Login = () => {
  const theme = useAppTheme()
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'))

  return (
    <AuthWrapper1>
      <head>
        <title>FastO | Sign in</title>
      </head>
      <Grid
        container
        direction="column"
        justifyContent="flex-end"
        sx={{ minHeight: '100vh' }}
      >
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: 'calc(100vh - 68px)' }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <AuthCardWrapper>
                <Grid container alignItems="center" justifyContent="center">
                  <Grid item>
                    <Avatar
                      alt="Logo"
                      src="images/logo2.png"
                      sx={{
                        width: '100px',
                        height: '100px',
                        marginBottom: '16px',
                      }}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Box display="flex" alignItems="center">
                      <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                    </Box>
                  </Grid>

                  <Grid item xs={12} mt={2}>
                    <Grid
                      container
                      direction={matchDownSM ? 'column-reverse' : 'row'}
                      alignItems="center"
                      justifyContent="center"
                    >
                      <Grid item>
                        <Stack
                          alignItems="center"
                          justifyContent="center"
                          spacing={1}
                        >
                          <Typography
                            color={theme.palette.secondary.main}
                            gutterBottom
                            variant={matchDownSM ? 'h3' : 'h2'}
                          >
                            Hi, Welcome Back
                          </Typography>
                        </Stack>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid item xs={12}>
                    <AuthLogin />
                  </Grid>
                </Grid>
              </AuthCardWrapper>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ m: 3, mt: 1 }}>
          <AuthFooter />
        </Grid>
      </Grid>
    </AuthWrapper1>
  )
}

export default Login
