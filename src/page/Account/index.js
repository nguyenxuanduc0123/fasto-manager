import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { getInfo, selectInfo } from '../../features/account/accountSlice'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import Edit from './Edit'

const Account = () => {
  const dispatch = useAppDispatch()

  const storeInfo = useAppSelector(selectInfo)

  useEffect(() => {
    dispatch(getInfo())
  }, [dispatch])

  return (
    <MainLayout>
      <MainCard content title="Account">
        {storeInfo && <Edit storeInfo={storeInfo} />}
      </MainCard>
    </MainLayout>
  )
}

export default Account
