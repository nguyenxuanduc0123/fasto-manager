import { Delete, LocationOn } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import {
  Box,
  Button,
  FormHelperText,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material'
import { FieldArray, FormikProvider, useFormik } from 'formik'
import React from 'react'
import { imageType } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  selectStatusUpdateInfo,
  updateInfo,
} from '../../features/account/accountSlice'
import { enqueueSnackbar } from '../../features/snackbar/snackbarSlice'
import { useAppTheme } from '../../features/theme/hooks'
import AppTextField from '../../ui-component/AppTextField'
import Dropzone from '../../ui-component/Dropzone/Dropzone'
import GoogleMaps from '../../ui-component/FormPlaceInput/GoogleMaps'
import { handleRemoveImage } from '../../util/helpers'

const Edit = ({ storeInfo }) => {
  const theme = useAppTheme()

  const dispatch = useAppDispatch()

  const status = useAppSelector(selectStatusUpdateInfo)

  const convertValues = (values) => {
    const address = values.location[0].name.split(', ')
    const latitude = values.location[0].latitude.split(' , ')
    const [street, , province, city, country] = address
    const x = Number(latitude[0])
    const y = Number(latitude[1])

    return {
      banner: values?.image[0].url,
      city,
      country,
      images: [],
      name: values?.name,
      province,
      street,
      x,
      y,
    }
  }

  const formik = useFormik({
    initialValues: {
      image: [
        {
          url: storeInfo?.logo,
          type: '',
        },
      ],
      images: [],
      name: storeInfo?.name,
      location: [
        {
          latitude: `${storeInfo?.x} , ${storeInfo?.y}`,
          name: `${storeInfo?.street}, , ${storeInfo?.province}, ${storeInfo?.city}, ${storeInfo?.country}`,
        },
      ],
    },
    onSubmit: (values) => {
      const formData = convertValues(values)
      try {
        dispatch(updateInfo(formData))
      } catch (err) {
        console.error(err)
      }
    },
  })

  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ p: 1 }}
      noValidate
    >
      <Grid container spacing={2} xs={12}>
        <Grid item xs={4}>
          <Box
            display="flex"
            flexWrap="wrap"
            sx={{
              justifyContent: 'center',
              marginBottom: { xs: '20px', sm: '0px' },
            }}
          >
            {formik.values.image.map((media, index) => (
              <Box key={index} sx={{ position: 'relative' }}>
                <Dropzone
                  values={formik.values}
                  setFieldValue={formik.setFieldValue}
                  setFieldError={formik.setFieldError}
                  fieldValue="image"
                  acceptFileType={imageType}
                  maxFile={1}
                  media={media}
                  index={index}
                  handleClickMedia={null}
                  handleRemoveMedia={handleRemoveImage('image', formik)}
                  handleEditImage={null}
                  label="image"
                />
              </Box>
            ))}
          </Box>
          <Box justifyContent="center" display="flex">
            {formik.errors['image'] && (
              <FormHelperText error={Boolean(formik.errors['image'])}>
                {formik.errors.image}
              </FormHelperText>
            )}
          </Box>
        </Grid>

        <Grid item xs={8}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <AppTextField formik={formik} name="name" label="Name" required />
            </Grid>
            <Grid item xs={12}>
              <FormikProvider value={formik}>
                <FieldArray
                  name="location"
                  render={(arrayHelpers) => (
                    <Box>
                      <GoogleMaps
                        setFieldValue={(_, value) => {
                          const { locations } = arrayHelpers.form.values
                          formik.setFieldValue('location', [value])
                          const isExternal = locations?.some(
                            (e) => e.name === value.name
                          )
                          if (isExternal) {
                            enqueueSnackbar(
                              `Can't choose 1 location more than once`,
                              { variant: 'warning' }
                            )
                          }
                        }}
                        defaultValue={formik.values.location[0]?.name}
                        name="location"
                      />

                      {formik.values.location &&
                      formik.values.location.length > 0 ? (
                        <List>
                          {formik.values.location.map((location, index) => (
                            <ListItem
                              key={location.latitude}
                              sx={{
                                '&:hover': {
                                  backgroundColor: theme.palette.grey[200],
                                  transition: '0.2s ease-in-out',
                                  color: 'white',
                                },
                              }}
                              secondaryAction={
                                <IconButton
                                  edge="end"
                                  aria-label="delete"
                                  onClick={() => arrayHelpers.remove(index)}
                                >
                                  <Delete />
                                </IconButton>
                              }
                            >
                              <ListItemIcon>
                                <LocationOn />
                              </ListItemIcon>
                              <ListItemText primary={location.name} />
                            </ListItem>
                          ))}
                        </List>
                      ) : (
                        'The currently has no location'
                      )}
                      <Box sx={{ marginLeft: '14px' }}>
                        {formik.touched['location'] && (
                          <FormHelperText
                            error={Boolean(formik.errors['location'])}
                          >
                            {formik.errors.location}
                          </FormHelperText>
                        )}
                      </Box>
                    </Box>
                  )}
                />
              </FormikProvider>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="flex-end"
        marginTop={0.5}
        spacing={2}
      >
        <Grid item xs={2}>
          <Button
            color="primary"
            variant="outlined"
            fullWidth
            type="button"
            disabled={status.process}
            onClick={() => formik.resetForm()}
          >
            Clear
          </Button>
        </Grid>

        <Grid item xs={2}>
          <LoadingButton
            color="primary"
            variant="contained"
            fullWidth
            type="submit"
            startIcon={<div></div>}
            disabled={status.process}
            loading={status.process}
            loadingPosition="start"
          >
            Submit
          </LoadingButton>
        </Grid>
      </Grid>
    </Box>
  )
}

export default Edit
