import { Search } from '@mui/icons-material'
import {
  Avatar,
  Box,
  Button,
  FormControl,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material'
import { format } from 'date-fns'
import React, { useEffect, useState } from 'react'
import { disableMenu, statusOfBill } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getListBill,
  selectListBill,
  selectListBillQuery,
} from '../../features/bill/billSlice'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import MainCard from '../../ui-component/cards/MainCard'
import { formatPrice } from '../../util/helpers'
import ConfirmDialog from '../../ui-component/ConfirmDialog/ConfirmDialog'

const Bill = () => {
  const dispatch = useAppDispatch()

  const [searchValue, setSearchValue] = useState('')
  const [statusBill, setStatusBill] = useState('PAID')
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false)
  const [idDetail, setIdDetail] = useState(undefined)

  const debouncedValue = useDebounce(searchValue)

  const query = useAppSelector(selectListBillQuery)
  const listBill = useAppSelector(selectListBill)

  useEffect(() => {
    fetchDataList({
      ...query,
      page: 0,
      query: debouncedValue,
      status: statusBill,
    })
  }, [dispatch, debouncedValue, statusBill])

  const fetchDataList = ({
    sort = 'id,desc',
    page = 0,
    size = 10,
    query = '',
    status,
  }) => {
    dispatch(getListBill({ page, size, query, status, sort }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'userImage',
      headerName: 'Image',
      flex: 1,
      ...disableMenu,
      renderCell: (params) => (
        <Avatar
          sx={{ objectFit: 'cover', width: 50, height: 45 }}
          src={`${params.value || `/images/logo.jpg`}`}
        />
      ),
    },
    {
      field: 'userFirstName',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'totalOrigin',
      headerName: 'totalOrigin',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        return `${formatPrice(params?.row.totalOrigin)} đ`
      },
    },
    {
      field: 'totalDiscount',
      headerName: 'totalDiscount',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        return `${formatPrice(params?.row.totalDiscount)} đ`
      },
    },
    {
      field: 'totalPayment',
      headerName: 'totalPayment',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        return `${formatPrice(params?.row.totalPayment)} đ`
      },
    },
    {
      field: 'createdAt',
      headerName: 'Created At',
      flex: 1,
      disableColumnMenu: true,
      valueGetter: (params) => {
        const modifiedDate = new Date(params?.row.createdAt).getTime()
        return `${format(modifiedDate || new Date(), 'dd-MM-yyy')} `
      },
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="Bill">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search Bill"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          <FormControl sx={{ minWidth: '250px' }}>
            <InputLabel id="demo-simple-select-helper-label">
              {'Status'}
            </InputLabel>
            <Select
              value={statusBill}
              label={'Status'}
              onChange={(event) => {
                setStatusBill(event.target.value)
              }}
            >
              {statusOfBill.map((item) => (
                <MenuItem key={item.value} value={item.value}>
                  {item?.label || ''}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
        <AppDataGrid
          rows={listBill?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listBill?.totalElements || 0}
          page={listBill?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...query, page: newPage })
          }}
          rowsPerPageOptions={[10]}
          onRowClick={(row) => {
            setOpenConfirmDialog(true)
            setIdDetail(row?.id)
          }}
        />
        <ConfirmDialog
          openConfirmDialog={openConfirmDialog}
          setOpenConfirmDialog={setOpenConfirmDialog}
          idDetail={idDetail}
        />
      </MainCard>
    </MainLayout>
  )
}

export default Bill
