import { Avatar, Box, Stack, Typography } from '@mui/material'
import React from 'react'
import { useLocation, useParams } from 'react-router-dom'
import { useAppTheme } from '../../features/theme/hooks'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import { formatPrice } from '../../util/helpers'

const ProductDetail = () => {
  const { state } = useLocation()
  const { listProduct } = state
  const theme = useAppTheme()
  const { productId } = useParams()

  const [detailProduct] = listProduct?.content?.filter(
    (item) => productId == item.id
  )

  return (
    <MainLayout>
      <MainCard content isBack title="Detail Product">
        <Box
          sx={{
            borderRadius: '5px',
            border: `2px dashed ${theme.palette.grey[500]}`,
            height: '100%',
          }}
          pt={3}
          pb={2}
          mb={3}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            mb={2}
          >
            <Stack direction="row" justifyContent="center">
              <Avatar
                sx={{ width: '200px', height: '200px' }}
                src={detailProduct?.image}
                alt="user_image"
              >
                {detailProduct?.name}
              </Avatar>
            </Stack>
            <Typography variant="h2" p={2}>
              {detailProduct?.name}
            </Typography>
          </Box>

          <Box
            sx={{
              display: 'flex',
              textAlign: 'center',
              justifyContent: 'space-evenly',
              flexWrap: 'wrap',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Category Name
              </Typography>
              <Typography variant="body1" noWrap>
                {detailProduct?.categoryName || '-'}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Count Pay
              </Typography>
              <Typography variant="body1" noWrap>
                {detailProduct?.countPay || '-'}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Price
              </Typography>
              <Typography variant="body1" noWrap>
                {`${formatPrice(detailProduct?.price)} VND` || '-'}
              </Typography>
            </Box>

            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
              }}
              p={1}
            >
              <Typography
                variant="h4"
                sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
              >
                Status
              </Typography>
              <Typography variant="body1" noWrap>
                {detailProduct?.status || '-'}
              </Typography>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
          p={1}
        >
          <Typography
            variant="h4"
            sx={{ pr: 0, width: 'auto', minWidth: '100px' }}
          >
            Description
          </Typography>
          <Typography variant="body1" ml={2}>
            {detailProduct?.description || '-'}
          </Typography>
        </Box>
      </MainCard>
    </MainLayout>
  )
}

export default ProductDetail
