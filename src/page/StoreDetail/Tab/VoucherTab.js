import { Box } from '@mui/material'
import { format } from 'date-fns'
import React from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { disableMenu } from '../../../app/constant'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { formatPrice } from '../../../util/helpers'
import {
  getListVoucher,
  selectListVoucher,
} from '../../../features/store/storeSlice'
import AppDataGrid from '../../../ui-component/DataGrid/AppDataGrid'

const VoucherTab = ({ storeId }) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const listVoucher = useAppSelector(selectListVoucher)

  useEffect(() => {
    if (storeId) {
      fetchDataList({ page: 0, size: 10, id: +storeId })
    }
  }, [dispatch, storeId])

  const fetchDataList = ({ page = 0, size = 10, query = '', id = '' }) => {
    dispatch(getListVoucher({ page, size, query, id }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'limitPerUser',
      headerName: 'Limit Per User',
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'valueNeed',
      headerName: 'Value Need',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        return params.value ? `${formatPrice(params.value)} VND` : '-'
      },
    },
    {
      field: 'valueDiscount',
      headerName: 'Value Discount',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        return params.value ? `${formatPrice(params.value)} VND` : '-'
      },
    },
    {
      field: 'maxDiscount',
      headerName: 'Max Discount',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        return params.value ? `${formatPrice(params.value)} VND` : '-'
      },
    },
    {
      field: 'createdAt',
      headerName: 'Created At',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        const modifiedDate = new Date(params?.row.createdAt).getTime()
        return `${format(modifiedDate || new Date(), 'yyyy-MM-dd')} `
      },
    },
    {
      field: 'endedAt',
      headerName: 'Ended At',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        const modifiedDate = new Date(params?.row.endedAt).getTime()
        return `${format(modifiedDate || new Date(), 'yyyy-MM-dd')} `
      },
    },
  ]
  return (
    <Box sx={{ height: '65vh' }}>
      <AppDataGrid
        rows={listVoucher?.content || []}
        columns={columns}
        pageSize={10}
        rowCount={listVoucher?.totalElements || 0}
        page={listVoucher?.number}
        onPageChange={(newPage) => {
          fetchDataList({ page: newPage, id: +storeId })
        }}
        rowsPerPageOptions={[10]}
        onRowClick={(row) => {
          navigate(`/store/voucher/${row.id}`)
        }}
      />
    </Box>
  )
}

export default VoucherTab
