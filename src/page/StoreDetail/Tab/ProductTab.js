import { Box, CardMedia } from '@mui/material'
import { useEffect } from 'react'
import { disableMenu } from '../../../app/constant'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { formatPrice } from '../../../util/helpers'
import {
  getListProduct,
  selectListProduct,
} from '../../../features/store/storeSlice'
import { useNavigate } from 'react-router-dom'
import AppDataGrid from '../../../ui-component/DataGrid/AppDataGrid'

const ProductTab = ({ storeId }) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const listProduct = useAppSelector(selectListProduct)

  useEffect(() => {
    if (storeId) {
      fetchDataList({ page: 0, size: 10, id: +storeId })
    }
  }, [dispatch, storeId])

  const fetchDataList = ({ page = 0, size = 10, query = '', id = '' }) => {
    dispatch(getListProduct({ page, size, query, id }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'image',
      headerName: 'Image',
      flex: 1,
      ...disableMenu,
      renderCell: (params) => (
        <CardMedia
          sx={{ objectFit: 'cover', width: 50, height: 45 }}
          component="img"
          src={params.value}
        />
      ),
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'categoryName',
      headerName: 'Category',
      flex: 1,
      ...disableMenu,
    },
    {
      field: 'price',
      headerName: 'Price',
      flex: 1,
      ...disableMenu,
      valueGetter: (params) => {
        return params.value ? `${formatPrice(params.value)} VND` : '-'
      },
    },
    {
      field: 'status',
      headerName: 'Status',
      flex: 1,
      ...disableMenu,
    },
  ]

  return (
    <Box sx={{ height: '65vh' }}>
      <AppDataGrid
        rows={listProduct?.content || []}
        columns={columns}
        pageSize={10}
        rowCount={listProduct?.totalElements || 0}
        page={listProduct?.number}
        onPageChange={(newPage) => {
          fetchDataList({ page: newPage, id: +storeId })
        }}
        rowsPerPageOptions={[10]}
        onRowClick={(row) => {
          navigate(`/store/product/${row.id}`, { state: { listProduct } })
        }}
      />
    </Box>
  )
}

export default ProductTab
