import styled from '@emotion/styled'
import {
  Avatar,
  Box,
  Grid,
  MenuItem,
  TextField,
  Typography,
} from '@mui/material'
import React from 'react'
import { useAppTheme } from '../../../features/theme/hooks'
import MainCard from '../../../ui-component/cards/MainCard'
import { formatPrice } from '../../../util/helpers'
import AttachMoneyOutlinedIcon from '@mui/icons-material/AttachMoneyOutlined'
import { statusTurnover } from '../../../app/constant'
import { useState } from 'react'
import { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  getTurnoverStore,
  selectTurnoverStore,
} from '../../../features/store/storeSlice'

const CardWrapper = styled(MainCard)(({ theme }) => ({
  backgroundColor: theme.palette.secondary.dark,
  color: '#fff',
  overflow: 'hidden',
  position: 'relative',
  '&:after': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: theme.palette.secondary[800],
    borderRadius: '50%',
    top: -85,
    right: -95,
    [theme.breakpoints.down('sm')]: {
      top: -105,
      right: -140,
    },
  },
  '&:before': {
    content: '""',
    position: 'absolute',
    width: 210,
    height: 210,
    background: theme.palette.secondary[800],
    borderRadius: '50%',
    top: -125,
    right: -15,
    opacity: 0.5,
    [theme.breakpoints.down('sm')]: {
      top: -155,
      right: -70,
    },
  },
}))

const TurnoverTab = ({ storeId }) => {
  const dispatch = useAppDispatch()
  const theme = useAppTheme()

  const [value, setValue] = useState(new Date().getMonth())

  const turnoverStore = useAppSelector(selectTurnoverStore)

  useEffect(() => {
    if (storeId) {
      const date = new Date()
      const firstDay = new Date(date.getFullYear(), value, 1).getTime()
      const lastDay = new Date(date.getFullYear(), value + 1, 0).getTime()
      dispatch(
        getTurnoverStore({
          id: +storeId,
          dateFrom: Math.round(firstDay / 1000),
          dateTo: Math.round(lastDay / 1000),
        })
      )
    }
  }, [value, storeId])

  return (
    <Grid container spacing={2}>
      <Grid item>
        <TextField
          id="standard-select-currency"
          select
          value={value}
          onChange={(e) => setValue(e.target.value)}
        >
          {statusTurnover.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
      <Grid item xs={12}>
        <CardWrapper border={false} content={false}>
          <Box sx={{ p: 2.25 }}>
            <Grid container direction="column">
              <Grid item>
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Avatar
                      variant="rounded"
                      sx={{
                        ...theme.typography.commonAvatar,
                        ...theme.typography.largeAvatar,
                        backgroundColor: theme.palette.secondary[800],
                        mt: 1,
                      }}
                    >
                      <AttachMoneyOutlinedIcon />
                    </Avatar>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container alignItems="center">
                  <Grid item>
                    <Typography
                      sx={{
                        fontSize: '2.125rem',
                        fontWeight: 500,
                        mr: 1,
                        mt: 1.75,
                        mb: 0.75,
                      }}
                    >
                      {`${formatPrice(turnoverStore?.totalPrice || 0)} VND`}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sx={{ mb: 1.25 }}>
                <Typography
                  sx={{
                    fontSize: '1rem',
                    fontWeight: 500,
                  }}
                >
                  Total Price
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </CardWrapper>
      </Grid>

      <Grid item xs={12}>
        <CardWrapper border={false} content={false}>
          <Box sx={{ p: 2.25 }}>
            <Grid container direction="column">
              <Grid item>
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Avatar
                      variant="rounded"
                      sx={{
                        ...theme.typography.commonAvatar,
                        ...theme.typography.largeAvatar,
                        backgroundColor: theme.palette.secondary[800],
                        mt: 1,
                      }}
                    >
                      <AttachMoneyOutlinedIcon />
                    </Avatar>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container alignItems="center">
                  <Grid item>
                    <Typography
                      sx={{
                        fontSize: '2.125rem',
                        fontWeight: 500,
                        mr: 1,
                        mt: 1.75,
                        mb: 0.75,
                      }}
                    >
                      {`${formatPrice(
                        turnoverStore?.totalPriceDiscount || 0
                      )} VND`}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sx={{ mb: 1.25 }}>
                <Typography
                  sx={{
                    fontSize: '1rem',
                    fontWeight: 500,
                  }}
                >
                  Total Price Discount
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </CardWrapper>
      </Grid>

      <Grid item xs={12}>
        <CardWrapper border={false} content={false}>
          <Box sx={{ p: 2.25 }}>
            <Grid container direction="column">
              <Grid item>
                <Grid container justifyContent="space-between">
                  <Grid item>
                    <Avatar
                      variant="rounded"
                      sx={{
                        ...theme.typography.commonAvatar,
                        ...theme.typography.largeAvatar,
                        backgroundColor: theme.palette.secondary[800],
                        mt: 1,
                      }}
                    >
                      <AttachMoneyOutlinedIcon />
                    </Avatar>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid container alignItems="center">
                  <Grid item>
                    <Typography
                      sx={{
                        fontSize: '2.125rem',
                        fontWeight: 500,
                        mr: 1,
                        mt: 1.75,
                        mb: 0.75,
                      }}
                    >
                      {`${formatPrice(
                        turnoverStore?.totalPriceProductOrigin || 0
                      )} VND`}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item sx={{ mb: 1.25 }}>
                <Typography
                  sx={{
                    fontSize: '1rem',
                    fontWeight: 500,
                  }}
                >
                  Total Price Product Origin
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </CardWrapper>
      </Grid>
    </Grid>
  )
}

export default TurnoverTab
