import { Avatar, Box, Stack, Typography } from '@mui/material'
import React from 'react'
import { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { useAppTheme } from '../../../features/theme/hooks'
import {
  getDetailStore,
  selectDetailStore,
} from '../../../features/store/storeSlice'

const DetailTab = ({ storeId }) => {
  const dispatch = useAppDispatch()
  const theme = useAppTheme()

  const detailStore = useAppSelector(selectDetailStore)

  useEffect(() => {
    if (storeId) {
      dispatch(getDetailStore({ page: 0, size: 10, id: +storeId }))
    }
  }, [dispatch, storeId])

  return (
    <Box
      sx={{
        borderRadius: '5px',
        border: `2px dashed ${theme.palette.grey[500]}`,
        height: '100%',
      }}
      pt={3}
      pb={2}
      px={2}
      mb={3}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
        mb={2}
      >
        <Stack direction="row" justifyContent="center">
          <Avatar
            sx={{ width: '200px', height: '200px' }}
            src={detailStore?.banner}
            alt="user_image"
          >
            {detailStore?.name || '-'}
          </Avatar>
        </Stack>
        <Typography variant="h2" p={2}>
          {detailStore?.name || '-'}
        </Typography>
      </Box>

      <Box>
        <Box p={1} sx={{ display: 'flex', columnGap: '30px' }}>
          <Box>
            <Typography variant="h4">Address</Typography>
            <Typography variant="body1" noWrap ml={2}>
              {`${detailStore?.address?.streetAddress}, ${detailStore?.address?.stateProvince}, ${detailStore?.address?.city}, ${detailStore?.address?.country}` ||
                '-'}
            </Typography>
          </Box>
          <Box>
            <Typography variant="h4">Phone</Typography>
            <Typography variant="body1" noWrap ml={2}>
              {detailStore?.phone || '-'}
            </Typography>
          </Box>
        </Box>
        <Box p={1}>
          <Typography variant="h4">Description</Typography>
          <Typography variant="body1" noWrap ml={2}>
            {detailStore?.description || '-'}
          </Typography>
        </Box>
      </Box>
    </Box>
  )
}

export default DetailTab
