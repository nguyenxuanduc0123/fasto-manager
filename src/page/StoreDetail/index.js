import { Box, Tab, Tabs, Typography } from '@mui/material'
import { useState } from 'react'
import { useParams } from 'react-router-dom'
import MainLayout from '../../layout/MainLayout'
import MainCard from '../../ui-component/cards/MainCard'
import DetailTab from './Tab/DetailTab'
import ProductTab from './Tab/ProductTab'
import TurnoverTab from './Tab/TurnoverTab'
import VoucherTab from './Tab/VoucherTab'

const StoreDetail = () => {
  const { storeId } = useParams()

  const [currentTab, setCurrentTab] = useState(0)

  function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    )
  }

  const handleChange = (event, newValue) => {
    setCurrentTab(newValue)
  }

  return (
    <MainLayout>
      <MainCard content isBack title="Detail Store">
        <Box>
          <Box>
            <Tabs value={currentTab} onChange={handleChange}>
              <Tab label="Detail Store" />
              <Tab label="Product of Store" />
              <Tab label="Voucher of Store" />
              <Tab label="Turnover of Store" />
            </Tabs>
          </Box>
          <TabPanel value={currentTab} index={0}>
            <DetailTab storeId={storeId} />
          </TabPanel>
          <TabPanel value={currentTab} index={1}>
            <ProductTab storeId={storeId} />
          </TabPanel>
          <TabPanel value={currentTab} index={2}>
            <VoucherTab storeId={storeId} />
          </TabPanel>
          <TabPanel value={currentTab} index={3}>
            <TurnoverTab storeId={storeId} />
          </TabPanel>
        </Box>
      </MainCard>
    </MainLayout>
  )
}

export default StoreDetail
