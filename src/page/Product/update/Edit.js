import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  getDetailProduct,
  selectDetailProduct,
} from '../../../features/product/productSlice'
import MainLayout from '../../../layout/MainLayout'
import MainCard from '../../../ui-component/cards/MainCard'
import UpdateForm from './UpdateForm'

const Edit = () => {
  const dispatch = useAppDispatch()
  const { productId } = useParams()

  const productDetail = useAppSelector(selectDetailProduct)

  useEffect(() => {
    if (productId) {
      dispatch(getDetailProduct({ id: +productId }))
    }
  }, [dispatch, productId])

  return (
    <MainLayout>
      <MainCard content isBack title="Edit Product">
        {productDetail?.id && <UpdateForm initValue={productDetail} />}
      </MainCard>
    </MainLayout>
  )
}

export default Edit
