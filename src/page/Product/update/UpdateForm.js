import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material'
import { useFormik } from 'formik'
import React, { useEffect } from 'react'
import AppTextField from '../../../ui-component/AppTextField'
import { LoadingButton } from '@mui/lab'
import { productSchema } from '../../../validate/product'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import {
  createProduct,
  editProduct,
  selectStatusCreateProduct,
  selectStatusEditProduct,
} from '../../../features/product/productSlice'
import Dropzone from '../../../ui-component/Dropzone/Dropzone'
import { imageType, statusProduct } from '../../../app/constant'
import { handleRemoveImage } from '../../../util/helpers'
import { NumberFormatCustom } from '../../../ui-component/NumberFormatField'
import {
  getListCategory,
  selectListCategory,
  selectListCategoryQuery,
} from '../../../features/category/categorySlice'

const initialValues = {
  categoryId: '',
  countPay: 0,
  description: '',
  image: [
    {
      url: '',
      type: '',
    },
  ],
  name: '',
  price: 0,
  status: 'NEW',
}

const UpdateForm = ({ initValue }) => {
  const dispatch = useAppDispatch()

  const convertToFormValues = (values) => {
    const categoryId = values.category.id
    const image = [
      {
        url: values.image,
        type: '',
      },
    ]
    return { ...values, categoryId, image }
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: initValue ? convertToFormValues(initValue) : initialValues,
    validationSchema: productSchema,
    onSubmit: (values) => {
      const newForm = convertData(values)
      if (initValue) {
        dispatch(editProduct({ ...newForm }))
      } else {
        dispatch(createProduct({ ...newForm }))
      }
    },
  })

  const convertData = (values) => {
    const image = values.image[0].url
    return {
      ...values,
      image,
    }
  }

  const status = useAppSelector(
    initValue ? selectStatusEditProduct : selectStatusCreateProduct
  )

  const query = useAppSelector(selectListCategoryQuery)
  const listCategory = useAppSelector(selectListCategory)

  useEffect(() => {
    fetchDataList({ ...query, page: 0 })
  }, [dispatch])

  const fetchDataList = ({ page = 0, size = 10, query = '' }) => {
    dispatch(getListCategory({ page, size, query }))
  }

  return (
    <Box
      component="form"
      onSubmit={formik.handleSubmit}
      sx={{ p: 1 }}
      noValidate
    >
      <Grid container xs={12} mb={3}>
        <Grid flex={1} minWidth="180px">
          <Grid item xs={12}>
            <Box
              display="flex"
              flexWrap="wrap"
              sx={{
                justifyContent: 'center',
                marginBottom: { xs: '20px', sm: '0px' },
              }}
            >
              {formik.values.image.map((media, index) => (
                <Box sx={{ position: 'relative', mr: 1, mb: 1 }} key={index}>
                  <Dropzone
                    values={formik.values}
                    setFieldValue={formik.setFieldValue}
                    setFieldError={formik.setFieldError}
                    fieldValue="image"
                    acceptFileType={imageType}
                    maxFile={1}
                    media={media}
                    index={index}
                    handleClickMedia={null}
                    handleRemoveMedia={handleRemoveImage('image', formik)}
                    handleEditImage={null}
                    label={'Image'}
                  />
                </Box>
              ))}
            </Box>
            <Box
              sx={{ justifyContent: { xs: 'center', sm: 'start' } }}
              display="flex"
              mb={2}
            >
              {formik.errors['image'] && (
                <FormHelperText error={Boolean(formik.errors['image'])}>
                  {formik.errors.image}
                </FormHelperText>
              )}
            </Box>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <AppTextField formik={formik} label="Name" name="name" required />
            </Grid>

            <Grid item xs={12} md={6}>
              <TextField
                label="Price"
                value={formik.values['price']}
                onChange={(event) => {
                  const value = event.target.value
                  formik.setFieldValue('price', Math.abs(Number(value)))
                }}
                name="price"
                required
                fullWidth
                InputProps={{
                  inputComponent: NumberFormatCustom,
                }}
              />
              {formik.touched['price'] && (
                <FormHelperText error={Boolean(formik.errors['price'])}>
                  {formik.errors['price']}
                </FormHelperText>
              )}
            </Grid>

            <Grid item xs={12} md={6}>
              <FormControl fullWidth>
                <InputLabel
                  error={
                    formik.touched['categoryId'] &&
                    Boolean(formik.errors['categoryId'])
                  }
                  id="demo-simple-select-helper-label"
                >
                  {'Category'}
                </InputLabel>
                <Select
                  value={formik.values.categoryId}
                  label={'Category'}
                  onChange={(event) => {
                    formik.setFieldValue('categoryId', event.target.value)
                  }}
                  error={
                    formik.touched['categoryId'] &&
                    Boolean(formik.errors['categoryId'])
                  }
                >
                  {listCategory?.content?.length > 0 ? (
                    listCategory.content.map((category) => (
                      <MenuItem key={category.id} value={category.id}>
                        {category?.name || ''}
                      </MenuItem>
                    ))
                  ) : (
                    <MenuItem value={''}>No Categories</MenuItem>
                  )}
                </Select>
                <FormHelperText error={Boolean(formik.errors['categoryId'])}>
                  {formik.errors['categoryId']}
                </FormHelperText>
              </FormControl>
            </Grid>

            <Grid item xs={12} md={6}>
              <FormControl fullWidth>
                <InputLabel
                  error={
                    formik.touched['status'] && Boolean(formik.errors['status'])
                  }
                  required
                  id="demo-simple-select-helper-label"
                >
                  Status
                </InputLabel>
                <Select
                  value={formik.values.status}
                  label="Status"
                  onChange={(event) => {
                    formik.setFieldValue('status', event.target.value)
                  }}
                  error={
                    formik.touched['status'] && Boolean(formik.errors['status'])
                  }
                >
                  {statusProduct.map((status) => (
                    <MenuItem value={status.value}>{status.label}</MenuItem>
                  ))}
                </Select>
                {formik.touched['status'] && (
                  <FormHelperText error={Boolean(formik.errors['status'])}>
                    {formik.errors['status']}
                  </FormHelperText>
                )}
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <AppTextField
                formik={formik}
                name="description"
                label="Description"
                multiline={true}
                rows={3}
                required
              />
            </Grid>

            <Grid
              container
              direction="row"
              alignItems="center"
              justifyContent="flex-end"
              marginTop={0.5}
              spacing={2}
            >
              <Grid item xs={2}>
                <Button
                  color="primary"
                  variant="outlined"
                  fullWidth
                  type="button"
                  disabled={status.processing}
                  onClick={() => formik.resetForm()}
                >
                  Clear
                </Button>
              </Grid>

              <Grid item xs={2}>
                <LoadingButton
                  color="primary"
                  variant="contained"
                  fullWidth
                  type="submit"
                  startIcon={<div></div>}
                  disabled={status.processing}
                  loading={status.processing}
                  loadingPosition="start"
                >
                  Submit
                </LoadingButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  )
}

export default UpdateForm
