import { Search } from '@mui/icons-material'
import { Box, InputAdornment, TextField } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { disableMenu } from '../../app/constant'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  deleteProduct,
  getListProduct,
  selectListProduct,
  selectListProductQuery,
} from '../../features/product/productSlice'
import TableAction from '../../features/TableActions'
import useDebounce from '../../hooks/useDebounce'
import MainLayout from '../../layout/MainLayout'
import { RedirectButton } from '../../ui-component/Button'
import MainCard from '../../ui-component/cards/MainCard'
import AppDataGrid from '../../ui-component/DataGrid/AppDataGrid'
import DeleteDialog from '../../ui-component/DeleteDialog/DeleteDialog'
import { formatPrice } from '../../util/helpers'

const Product = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const [searchValue, setSearchValue] = useState('')
  const [idDelete, setIdDelete] = useState(0)
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false)

  const debouncedValue = useDebounce(searchValue)

  const query = useAppSelector(selectListProductQuery)
  const listProduct = useAppSelector(selectListProduct)

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false)
  }

  const handleSubmitDeleteGroup = async () => {
    try {
      dispatch(deleteProduct({ id: idDelete }))
      handleCloseDeleteDialog()
    } catch {
      console.error('Error')
    }
  }

  useEffect(() => {
    fetchDataList({ ...query, page: 0, query: debouncedValue })
  }, [dispatch, debouncedValue])

  const fetchDataList = ({
    sort = 'id,desc',
    page = 0,
    size = 10,
    query = '',
  }) => {
    dispatch(getListProduct({ page, size, query, sort }))
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 100,
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Name',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'price',
      headerName: 'Price',
      flex: 1,
      disableColumnMenu: true,
      renderCell: (params) => <p>{formatPrice(params?.value)} đ</p>,
    },
    {
      field: 'deleteFlag',
      headerName: 'Delete',
      flex: 1,
      disableColumnMenu: true,
    },
    {
      field: 'action',
      headerName: 'Action',
      flex: 1,
      renderCell: ({ row }) => (
        <TableAction
          showDeleteButton={row.deleteFlag === false}
          // showRecoverButton={row.deleteFlag === true}
          deleteAction={() => {
            setIdDelete(row.id)
            setOpenDeleteDialog(true)
          }}
          edit={{
            href: `/product/edit/${row.id}`,
            as: `/product/edit/${row.id}`,
          }}
        />
      ),
      ...disableMenu,
    },
  ]

  return (
    <MainLayout>
      <MainCard content title="Product">
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          marginBottom="1rem"
        >
          <TextField
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            placeholder="Search Product"
            sx={{ minWidth: '300px' }}
            InputProps={{
              endAdornment: (
                <InputAdornment sx={{ cursor: 'pointer' }} position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          <RedirectButton href="/product/create" text="Make Product" />
        </Box>
        <AppDataGrid
          rows={listProduct?.content || []}
          columns={columns}
          pageSize={10}
          rowCount={listProduct?.totalElements || 0}
          page={listProduct?.number}
          onPageChange={(newPage) => {
            fetchDataList({ ...query, page: newPage })
          }}
          rowsPerPageOptions={[10]}
          onRowClick={(row) => {
            navigate(`/product/detail/${row?.id}`)
          }}
        />
        <DeleteDialog
          open={openDeleteDialog}
          handleClose={handleCloseDeleteDialog}
          handleSubmit={handleSubmitDeleteGroup}
          title="Confirm Delete Group"
          description="Are you sure, you want to delete this category?"
        />
      </MainCard>
    </MainLayout>
  )
}

export default Product
