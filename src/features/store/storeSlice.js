import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListStore: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  activeStore: {
    status: { ...INIT_REQUEST_STATUS },
  },
  blockStore: {
    status: { ...INIT_REQUEST_STATUS },
  },
  getDetailStore: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getDetailVoucherStore: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getListProduct: {
    status: { ...INIT_REQUEST_STATUS },
    list: { ...INIT_LIST },
  },
  getListVoucher: {
    status: { ...INIT_REQUEST_STATUS },
    list: { ...INIT_LIST },
  },
  getTurnoverStore: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
}

export const storeSlice = createSlice({
  name: '@store',
  initialState,
  reducers: {
    // GET LIST STORE
    getListStore: (state, action) => {
      state.getListStore.status.processing = !!action.payload
      state.getListStore.status.success = false
      state.getListStore.status.fail = false
      state.getListStore.query = action.payload
    },
    successGetListStore: (state, action) => {
      state.getListStore.list = action.payload
      state.getListStore.status.success = true
      state.getListStore.status.processing = false
    },
    failGetListStore: (state) => {
      state.getListStore.status.fail = true
      state.getListStore.status.processing = false
    },

    // GET PRODUCT STORE
    getListProduct: (state, action) => {
      state.getListProduct.status.processing = !!action.payload
      state.getListProduct.status.success = false
      state.getListProduct.status.fail = false
      state.getListProduct.query = action.payload
    },
    successGetListProduct: (state, action) => {
      state.getListProduct.list = action.payload
      state.getListProduct.status.success = true
      state.getListProduct.status.processing = false
    },
    failGetListProduct: (state) => {
      state.getListProduct.status.fail = true
      state.getListProduct.status.processing = false
    },

    // GET VOUCHER STORE
    getListVoucher: (state, action) => {
      state.getListVoucher.status.processing = !!action.payload
      state.getListVoucher.status.success = false
      state.getListVoucher.status.fail = false
      state.getListVoucher.query = action.payload
    },
    successGetListVoucher: (state, action) => {
      state.getListVoucher.list = action.payload
      state.getListVoucher.status.success = true
      state.getListVoucher.status.processing = false
    },
    failGetListVoucher: (state) => {
      state.getListVoucher.status.fail = true
      state.getListVoucher.status.processing = false
    },

    // ACTIVE STORE
    activeStore: (state, action) => {
      state.activeStore.status.processing = !!action.payload
      state.activeStore.status.fail = false
      state.activeStore.status.success = false
    },
    stopActiveStore: (state) => {
      state.activeStore.status.processing = false
    },
    failActiveStore: (state) => {
      state.activeStore.status.fail = true
      state.activeStore.status.processing = false
    },
    successActiveStore: (state) => {
      state.activeStore.status.success = true
      state.activeStore.status.processing = false
    },

    // BLOCK STORE
    blockStore: (state, action) => {
      state.blockStore.status.processing = !!action.payload
      state.blockStore.status.fail = false
      state.blockStore.status.success = false
    },
    stopBlockStore: (state) => {
      state.blockStore.status.processing = false
    },
    failBlockStore: (state) => {
      state.blockStore.status.fail = true
      state.blockStore.status.processing = false
    },
    successBlockStore: (state) => {
      state.blockStore.status.success = true
      state.blockStore.status.processing = false
    },

    // GET DETAIL STORE
    getDetailStore: (state, action) => {
      state.getDetailStore.status.processing = !!action.payload
      state.getDetailStore.status.fail = false
      state.getDetailStore.status.success = false
    },
    stopGetDetailStore: (state) => {
      state.getDetailStore.status.processing = false
    },
    failGetDetailStore: (state) => {
      state.getDetailStore.state.fail = true
    },
    successGetDetailStore: (state, action) => {
      state.getDetailStore.detail = action.payload
      state.getDetailStore.status.success = true
    },

    getDetailVoucherStore: (state, action) => {
      state.getDetailVoucherStore.status.processing = !!action.payload
      state.getDetailVoucherStore.status.fail = false
      state.getDetailVoucherStore.status.success = false
    },
    stopGetDetailVoucherStore: (state) => {
      state.getDetailVoucherStore.status.processing = false
    },
    failGetDetailVoucherStore: (state) => {
      state.getDetailVoucherStore.state.fail = true
    },
    successGetDetailVoucherStore: (state, action) => {
      state.getDetailVoucherStore.detail = action.payload
      state.getDetailVoucherStore.status.success = true
    },

    getTurnoverStore: (state, action) => {
      state.getTurnoverStore.status.processing = !!action.payload
      state.getTurnoverStore.status.fail = false
      state.getTurnoverStore.status.success = false
    },
    stopGetTurnoverStore: (state) => {
      state.getTurnoverStore.status.processing = false
    },
    failGetTurnoverStore: (state) => {
      state.getTurnoverStore.state.fail = true
    },
    successGetTurnoverStore: (state, action) => {
      state.getTurnoverStore.detail = action.payload
      state.getTurnoverStore.status.success = true
    },

    setQuery: (state, action) => {
      state.getListStore.query = action.payload
    },
  },
})

export const {
  getListStore,
  successGetListStore,
  failGetListStore,

  getListProduct,
  successGetListProduct,
  failGetListProduct,

  getListVoucher,
  successGetListVoucher,
  failGetListVoucher,

  activeStore,
  stopActiveStore,
  failActiveStore,
  successActiveStore,

  blockStore,
  stopBlockStore,
  failBlockStore,
  successBlockStore,

  getDetailStore,
  stopGetDetailStore,
  failGetDetailStore,
  successGetDetailStore,

  getDetailVoucherStore,
  stopGetDetailVoucherStore,
  failGetDetailVoucherStore,
  successGetDetailVoucherStore,

  getTurnoverStore,
  stopGetTurnoverStore,
  failGetTurnoverStore,
  successGetTurnoverStore,

  setQuery,
} = storeSlice.actions

export const selectListStore = (state) => state.store.getListStore.list
export const selectListStoreQuery = (state) => state.store.getListStore.query
export const selectActiveStoreStatus = (state) => state.store.activeStore.status
export const selectDetailStore = (state) => state.store.getDetailStore.detail
export const selectDetailVoucherStore = (state) =>
  state.store.getDetailVoucherStore.detail
export const selectListProduct = (state) => state.store.getListProduct.list
export const selectListVoucher = (state) => state.store.getListVoucher.list
export const selectTurnoverStore = (state) =>
  state.store.getTurnoverStore.detail

export default storeSlice.reducer
