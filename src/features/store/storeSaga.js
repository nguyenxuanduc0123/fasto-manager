import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import StoreAPI from '../../api/store/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  activeStore,
  blockStore,
  failActiveStore,
  failBlockStore,
  failGetDetailStore,
  failGetDetailVoucherStore,
  failGetListProduct,
  failGetListStore,
  failGetListVoucher,
  failGetTurnoverStore,
  getDetailStore,
  getDetailVoucherStore,
  getListProduct,
  getListStore,
  getListVoucher,
  getTurnoverStore,
  selectListStoreQuery,
  successActiveStore,
  successBlockStore,
  successGetDetailStore,
  successGetDetailVoucherStore,
  successGetListProduct,
  successGetListStore,
  successGetListVoucher,
  successGetTurnoverStore,
} from './storeSlice'

function* _getListStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getListStore, payload)
    const result = response.data
    yield put(successGetListStore(result))
  } catch (error) {
    yield put(failGetListStore())
    yield put(
      enqueueSnackbar({
        message: 'Get list store failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getListProduct({ payload }) {
  try {
    const response = yield call(StoreAPI.getProductOfStore, payload)
    const result = response.data
    yield put(successGetListProduct(result))
  } catch (error) {
    yield put(failGetListProduct())
    yield put(
      enqueueSnackbar({
        message: 'Get list product of store failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getListVoucher({ payload }) {
  try {
    const response = yield call(StoreAPI.getVoucherOfStore, payload)
    const result = response.data
    yield put(successGetListVoucher(result))
  } catch (error) {
    yield put(failGetListVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Get list voucher of store failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _activeStore({ payload }) {
  try {
    yield call(StoreAPI.activeStore, payload)
    yield put(successActiveStore())
    yield put(
      enqueueSnackbar({
        message: 'Active store successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListStoreQuery)
    yield put(getListStore(query))
  } catch (error) {
    yield put(failActiveStore())
    yield put(
      enqueueSnackbar({
        message: 'Active store failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _blockStore({ payload }) {
  try {
    yield call(StoreAPI.blockStore, payload)
    yield put(successBlockStore())
    yield put(
      enqueueSnackbar({
        message: 'Block store successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListStoreQuery)
    yield put(getListStore(query))
  } catch (error) {
    yield put(failBlockStore())
    yield put(
      enqueueSnackbar({
        message: 'Block store failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getDetailStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getDetailStore, payload)
    const result = response.data
    yield put(successGetDetailStore(result))
  } catch (error) {
    yield put(failGetDetailStore())
    yield put(
      enqueueSnackbar({
        message: 'Get detail store failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getDetailVoucherStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getDetailVoucherOfStore, payload)
    const result = response.data
    yield put(successGetDetailVoucherStore(result))
  } catch (error) {
    yield put(failGetDetailVoucherStore())
    yield put(
      enqueueSnackbar({
        message: 'Get detail voucher store failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getTurnoverStore({ payload }) {
  try {
    const response = yield call(StoreAPI.getStoreTurnover, payload)
    const result = response.data
    yield put(successGetTurnoverStore(result))
  } catch (error) {
    yield put(failGetTurnoverStore())
    yield put(
      enqueueSnackbar({
        message: 'Get turnover store failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* storeSaga() {
  yield all([
    takeLatest(getListStore, _getListStore),
    takeLatest(getListProduct, _getListProduct),
    takeLatest(getListVoucher, _getListVoucher),
    takeLatest(activeStore, _activeStore),
    takeLatest(blockStore, _blockStore),
    takeLatest(getDetailStore, _getDetailStore),
    takeLatest(getDetailVoucherStore, _getDetailVoucherStore),
    takeLatest(getTurnoverStore, _getTurnoverStore),
  ])
}
