import { Delete, LocationOn } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'
import {
  Avatar,
  Box,
  Button,
  Divider,
  FormHelperText,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Stack,
  Typography,
} from '@mui/material'
import { FieldArray, FormikProvider, useFormik } from 'formik'
import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import AppTextField from '../../../ui-component/AppTextField'
import GoogleMaps from '../../../ui-component/FormPlaceInput/GoogleMaps'
import { registerStoreSchema } from '../../../validate/registerStore'
import { enqueueSnackbar } from '../../snackbar/snackbarSlice'
import { useAppTheme } from '../../theme/hooks'
import { register, selectRegisterStatus } from '../authSlice'

const initialValues = {
  name: '',
  email: '',
  password: '',
  phoneNumber: '',
  description: '',
  location: [],
  logo: '',
}

const RegisterStore = ({ setIsOpen }) => {
  const dispatch = useAppDispatch()
  const [defaultValue, setDefaultValue] = useState('')
  const theme = useAppTheme()

  const registerStaus = useAppSelector(selectRegisterStatus)

  const convertToFormValues = (values) => {
    const { description, email, name, password, phoneNumber } = values
    const banner = ''
    const address = values.location[0].address.split(', ')
    const latitude = values.location[0].latitude.split(' , ')
    const [streetAddress, , stateProvince, city, country] = address
    const x = Number(latitude[0])
    const y = Number(latitude[1])

    return {
      banner,
      description,
      email,
      name,
      password,
      phoneNumber,
      locationDto: {
        streetAddress,
        stateProvince,
        city,
        country,
        x,
        y,
        postalCode: '550000',
      },
    }
  }

  const formik = useFormik({
    initialValues: initialValues,
    validateOnChange: false,
    validationSchema: registerStoreSchema,
    onSubmit: async (values) => {
      const formData = convertToFormValues(values)
      try {
        dispatch(register(formData))
        if (registerStaus.success) {
          setIsOpen(false)
        }
      } catch (err) {
        console.error(err)
      }
    },
  })

  return (
    <>
      <Box
        component="form"
        onSubmit={formik.handleSubmit}
        noValidate
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          bgcolor: 'background.paper',
          boxShadow: 24,
          width: { md: '50vw', xs: '90vw' },
          p: 2,
          overflowY: 'auto',
          maxHeight: '90vh',
          borderRadius: '12px',
        }}
      >
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={12} mb={1}>
            <Grid
              container
              direction={'row'}
              alignItems="center"
              justifyContent="center"
            >
              <Grid
                item
                xs={12}
                display="flex"
                alignItems="center"
                justifyContent="center"
              >
                <Avatar
                  alt="Logo"
                  src="images/logo2.png"
                  sx={{
                    width: '100px',
                    height: '100px',
                    marginBottom: '16px',
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <Stack alignItems="center" justifyContent="center">
                  <Typography
                    color={theme.palette.secondary.main}
                    gutterBottom
                    variant={'h3'}
                  >
                    Shop Register
                  </Typography>
                </Stack>
              </Grid>

              <Grid item xs={12}>
                <Box display="flex" alignItems="center">
                  <Divider sx={{ flexGrow: 1 }} orientation="horizontal" />
                </Box>
              </Grid>
            </Grid>
          </Grid>

          <Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <FormikProvider value={formik}>
                  <FieldArray
                    name="location"
                    render={(arrayHelpers) => (
                      <Box>
                        <GoogleMaps
                          setFieldValue={(_, value) => {
                            const { locations } = arrayHelpers.form.values
                            formik.setFieldValue('location', [value])
                            const isExternal = locations?.some(
                              (e) => e.name === value.name
                            )
                            if (isExternal) {
                              enqueueSnackbar(
                                `Can't choose 1 location more than once`,
                                { variant: 'warning' }
                              )
                            }
                          }}
                          defaultValue={defaultValue}
                          name="location"
                        />

                        {formik.values.location &&
                        formik.values.location.length > 0 ? (
                          <List>
                            {formik.values.location.map((location, index) => (
                              <ListItem
                                key={location.latitude}
                                sx={{
                                  '&:hover': {
                                    backgroundColor: theme.palette.grey[200],
                                    transition: '0.2s ease-in-out',
                                    color: 'white',
                                  },
                                }}
                                secondaryAction={
                                  <IconButton
                                    edge="end"
                                    aria-label="delete"
                                    onClick={() => arrayHelpers.remove(index)}
                                  >
                                    <Delete />
                                  </IconButton>
                                }
                              >
                                <ListItemIcon>
                                  <LocationOn />
                                </ListItemIcon>
                                <ListItemText
                                  primary={location.name}
                                  secondary={location.address}
                                />
                              </ListItem>
                            ))}
                          </List>
                        ) : (
                          'The currently has no location'
                        )}
                        <Box sx={{ marginLeft: '14px' }}>
                          {formik.touched['location'] && (
                            <FormHelperText
                              error={Boolean(formik.errors['location'])}
                            >
                              {formik.errors.location}
                            </FormHelperText>
                          )}
                        </Box>
                      </Box>
                    )}
                  />
                </FormikProvider>
              </Grid>
              <Grid item xs={12}>
                <AppTextField
                  formik={formik}
                  name="name"
                  label="name"
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <AppTextField
                  formik={formik}
                  name="email"
                  label="email"
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <AppTextField
                  formik={formik}
                  name="password"
                  label="password"
                  type="password"
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <AppTextField
                  formik={formik}
                  name="phoneNumber"
                  label="phoneNumber"
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <AppTextField
                  formik={formik}
                  name="description"
                  label="description"
                  required
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid
            container
            direction="row"
            alignItems="center"
            justifyContent="flex-end"
            marginTop={0.5}
            spacing={2}
          >
            <Grid item xs={3}>
              <Button
                color="primary"
                variant="outlined"
                fullWidth
                type="button"
                // disabled={status.processing}
                onClick={() => formik.resetForm()}
              >
                Clear
              </Button>
            </Grid>

            <Grid item xs={3}>
              <LoadingButton
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
                startIcon={<div></div>}
                // disabled={status.processing}
                // loading={status.processing}
                loadingPosition="start"
              >
                Submit
              </LoadingButton>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </>
  )
}

export default RegisterStore
