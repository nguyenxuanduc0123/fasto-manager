import { Formik } from 'formik'
import { useState } from 'react'
import * as Yup from 'yup'

// mui
import Visibility from '@mui/icons-material/Visibility'
import VisibilityOff from '@mui/icons-material/VisibilityOff'
import {
  Box,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  Modal,
  OutlinedInput,
  Radio,
  RadioGroup,
  Stack,
  Typography,
} from '@mui/material'

// logic
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { useAppTheme } from '../../theme/hooks'
import { selectAuthResponse } from '../authSlice'

// ui
import { LoadingButton } from '@mui/lab'
import ErrorMessage from '../../../ErrorMessage'
import { signIn, signInShop } from '../authAction'
import RegisterStore from './RegisterStore'
import { useNavigate } from 'react-router-dom'

const AuthLogin = ({ ...others }) => {
  const navigate = useNavigate()
  const theme = useAppTheme()
  const dispatch = useAppDispatch()
  const [isOpen, setIsOpen] = useState(false)
  const [selectedRole, setSelectedRole] = useState('admin')

  const response = useAppSelector(selectAuthResponse)

  const [showPassword, setShowPassword] = useState(false)

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }

  return (
    <>
      <Grid container direction="column" justifyContent="center" spacing={2}>
        <Grid
          item
          xs={12}
          container
          alignItems="center"
          justifyContent="center"
        >
          <Box sx={{ mb: 2 }}>
            <Typography variant="subtitle1">
              Sign in with Email address to continue
            </Typography>
          </Box>
        </Grid>
      </Grid>

      <Formik
        initialValues={{
          email: 'admin@gmail.com',
          password: '',
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email('Must be a valid email')
            .max(255)
            .required('Email is required'),
          password: Yup.string()
            .min(6, 'Should be 8 chars minimum.')
            .max(255)
            .required('Password is required'),
        })}
        onSubmit={async (values, { setStatus }) => {
          try {
            setStatus({ success: true })
            switch (selectedRole) {
              case 'admin':
                dispatch(
                  signIn({
                    email: values.email,
                    password: values.password,
                    role: selectedRole,
                  })
                )
                break
              case 'shop':
                dispatch(
                  signInShop({
                    email: values.email,
                    password: values.password,
                    role: selectedRole,
                  })
                )
                break
              default:
                return
            }
          } catch (err) {
            console.error(err)
          }
        }}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          touched,
          values,
        }) => (
          <form noValidate onSubmit={handleSubmit} {...others}>
            <FormControl
              fullWidth
              error={Boolean(touched.email && errors.email)}
              sx={{ ...theme.typography.customInput }}
            >
              <InputLabel htmlFor="outlined-adornment-email-login">
                Email Address
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-email-login"
                type="email"
                value={values.email}
                name="email"
                onBlur={handleBlur}
                onChange={handleChange}
                label="Email Address"
                inputProps={{}}
              />
              <ErrorMessage name="email" />
            </FormControl>

            <FormControl
              fullWidth
              error={Boolean(touched.password && errors.password)}
              sx={{ ...theme.typography.customInput }}
            >
              <InputLabel htmlFor="outlined-adornment-password-login">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password-login"
                type={showPassword ? 'text' : 'password'}
                value={values.password}
                name="password"
                onBlur={handleBlur}
                onChange={handleChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                      size="large"
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
                inputProps={{}}
              />
              <ErrorMessage name="password" />
            </FormControl>

            {response.error && (
              <FormHelperText error>{response.error}</FormHelperText>
            )}

            <Box
              sx={{
                display: 'flex',
                justifyContent: 'flex-end',
                cursor: 'pointer',
              }}
            >
              <Typography onClick={() => navigate('/forgotPassword')}>
                Forgot password?
              </Typography>
            </Box>

            <Stack alignItems="center" justifyContent="center">
              <FormControl>
                <RadioGroup
                  aria-labelledby="demo-radio-buttons-group-label"
                  defaultValue="admin"
                  name="radio-buttons-group"
                  row
                  onChange={(e) => setSelectedRole(e.target.value)}
                >
                  <FormControlLabel
                    value="admin"
                    control={<Radio />}
                    label="Admin"
                  />
                  <FormControlLabel
                    value="shop"
                    control={<Radio />}
                    label="Shop"
                  />
                </RadioGroup>
              </FormControl>
            </Stack>

            <Box sx={{ mt: 2 }}>
              <LoadingButton
                disableElevation
                disabled={response.status === 'pending'}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                color="secondary"
              >
                Sign in
              </LoadingButton>
            </Box>
            <Stack alignItems="center" justifyContent="center" mt={2}>
              <Typography
                color={theme.palette.secondary.main}
                variant="h4"
                sx={{ cursor: 'pointer' }}
                onClick={() => {
                  setIsOpen(true)
                }}
              >
                Become a Partner
              </Typography>
            </Stack>
          </form>
        )}
      </Formik>

      <Modal open={isOpen} onClose={() => setIsOpen(false)}>
        <RegisterStore setIsOpen={setIsOpen} />
      </Modal>
    </>
  )
}

export default AuthLogin
