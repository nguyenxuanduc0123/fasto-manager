import { createAction } from '@reduxjs/toolkit'

const signIn = createAction('auth/signIn')
const signInShop = createAction('auth/signInShop')

export { signIn, signInShop }
