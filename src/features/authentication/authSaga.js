import { all, call, put, takeLatest } from 'redux-saga/effects'
import UserAPI from '../../api/authentication/api'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import { signIn, signInShop } from './authAction'
import {
  changePassword,
  changeStatus,
  failChangePassword,
  register,
  successChangePassword,
  successRegister,
} from './authSlice'

function* _signIn({ payload }) {
  try {
    yield put(changeStatus({ status: 'pending' }))
    const response = yield call(UserAPI.signIn, payload)
    yield put(changeStatus({ status: 'fullfiledd' }))

    StorageUtil.set(STORAGE_KEY.JWT, response.data.id_token)
    StorageUtil.set(STORAGE_KEY.ROLE, payload.role)
    yield put(
      enqueueSnackbar({
        message: 'Login successful',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/')
  } catch (e) {
    yield put(
      changeStatus({
        status: 'rejected',
        error: e.response?.data?.message,
      })
    )
  }
}

function* _signInShop({ payload }) {
  try {
    yield put(changeStatus({ status: 'pending' }))
    const response = yield call(UserAPI.signInShop, payload)
    yield put(changeStatus({ status: 'fullfiledd' }))

    StorageUtil.set(STORAGE_KEY.JWT, response.data.id_token)
    StorageUtil.set(STORAGE_KEY.ROLE, payload.role)

    yield put(
      enqueueSnackbar({
        message: 'Login successful',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/')
  } catch (e) {
    yield put(
      changeStatus({
        status: 'rejected',
        error: e.response?.data?.message,
      })
    )
  }
}

function* _changePassword({ payload }) {
  try {
    yield call(UserAPI.changePassword, payload)

    yield put(successChangePassword())
    yield put(
      enqueueSnackbar({
        message: 'Change password successed',
        options: {
          variant: 'success',
        },
      })
    )
  } catch (e) {
    yield put(failChangePassword())

    yield put(
      enqueueSnackbar({
        message: e.response.data.message,
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _register({ payload }) {
  try {
    yield call(UserAPI.register, payload)

    yield put(successRegister())
    yield put(
      enqueueSnackbar({
        message: 'Register successed',
        options: {
          variant: 'success',
        },
      })
    )
  } catch (e) {
    yield put(successRegister())

    yield put(
      enqueueSnackbar({
        message: 'Register failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export default function* authSaga() {
  yield all([
    takeLatest(signIn, _signIn),
    takeLatest(signInShop, _signInShop),
    takeLatest(changePassword, _changePassword),
    takeLatest(register, _register),
  ])
}
