import { createSlice } from '@reduxjs/toolkit'
import { INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getInfo: {
    status: { ...INIT_REQUEST_STATUS },
    detail: undefined,
  },
  updateInfo: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const accountSlice = createSlice({
  name: '@account',
  initialState,
  reducers: {
    // GET INFO
    getInfo: (state, action) => {
      state.getInfo.status.processing = !!action.payload
      state.getInfo.status.fail = false
      state.getInfo.status.success = false
    },
    stopGetInfo: (state) => {
      state.getInfo.status.processing = false
    },
    failGetInfo: (state) => {
      state.getInfo.state.fail = true
    },
    successGetInfo: (state, action) => {
      state.getInfo.detail = action.payload
      state.getInfo.status.success = true
    },

    // UPDATE INFO
    updateInfo: (state, action) => {
      state.updateInfo.status.processing = !!action.payload
      state.updateInfo.status.fail = false
      state.updateInfo.status.success = false
    },
    stopUpdateInfo: (state) => {
      state.updateInfo.status.processing = false
    },
    failUpdateInfo: (state) => {
      state.updateInfo.status.fail = true
      state.updateInfo.status.processing = false
    },
    successUpdateInfo: (state) => {
      state.updateInfo.status.process = false
      state.updateInfo.status.success = true
    },
  },
})

export const {
  getInfo,
  stopGetInfo,
  failGetInfo,
  successGetInfo,
  updateInfo,
  stopUpdateInfo,
  failUpdateInfo,
  successUpdateInfo,
} = accountSlice.actions

export const selectStatusUpdateInfo = (state) => state.account.updateInfo.status

export const selectInfo = (state) => state.account.getInfo.detail

export default accountSlice.reducer
