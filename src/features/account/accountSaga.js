import { all, call, put, takeLatest } from 'redux-saga/effects'
import AccountAPI from '../../api/account/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  failGetInfo,
  failUpdateInfo,
  getInfo,
  successGetInfo,
  successUpdateInfo,
  updateInfo,
} from './accountSlice'

function* _updateInfo({ payload }) {
  try {
    yield call(AccountAPI.updateInfo, payload)
    yield put(successUpdateInfo())
    yield put(
      enqueueSnackbar({
        message: 'Update info success!',
        options: {
          variant: 'success',
        },
      })
    )
    yield put(getInfo())
  } catch (error) {
    yield put(failUpdateInfo())
  }
}

function* _getInfo({ payload }) {
  try {
    const response = yield call(AccountAPI.getInfo, payload)
    const result = response.data

    yield put(successGetInfo(result))
  } catch (error) {
    yield put(failGetInfo())
    yield put(
      enqueueSnackbar({
        message: 'Get info failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* accountSaga() {
  yield all([
    takeLatest(updateInfo, _updateInfo),
    takeLatest(getInfo, _getInfo),
  ])
}
