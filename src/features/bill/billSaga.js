import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import BillAPI from '../../api/bill/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  failGetDetailBill,
  failGetListBill,
  failValidBill,
  getDetailBill,
  getDetailBillById,
  getListBill,
  selectListBillQuery,
  successGetDetailBill,
  successGetListBill,
  successValidBill,
  validBill,
} from './billSlice'

function* _getListBill({ payload }) {
  try {
    const response = yield call(BillAPI.getListBill, payload)
    const result = response.data
    yield put(successGetListBill(result))
  } catch (error) {
    yield put(failGetListBill())
    yield put(
      enqueueSnackbar({
        message: 'Get list bill failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getDetailBill({ payload }) {
  try {
    const response = yield call(BillAPI.getDetailBill, payload)
    const result = response.data

    yield put(successGetDetailBill(result))
  } catch (error) {
    yield put(failGetDetailBill())
    yield put(
      enqueueSnackbar({
        message: 'Get detail bill failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getDetailBillById({ payload }) {
  try {
    const response = yield call(BillAPI.getDetailBillById, payload)
    const result = response.data

    yield put(successGetDetailBill(result))
  } catch (error) {
    yield put(failGetDetailBill())
    yield put(
      enqueueSnackbar({
        message: 'Get detail bill failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _validBill({ payload }) {
  try {
    yield call(BillAPI.validBill, payload)
    yield put(successValidBill())
    yield put(
      enqueueSnackbar({
        message: 'valid bill successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListBillQuery)
    yield put(getListBill(query))
  } catch (error) {
    yield put(failValidBill())
    yield put(
      enqueueSnackbar({
        message: 'valid bill failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* billSaga() {
  yield all([
    takeLatest(getListBill, _getListBill),
    takeLatest(getDetailBill, _getDetailBill),
    takeLatest(getDetailBillById, _getDetailBillById),
    takeLatest(validBill, _validBill),
  ])
}
