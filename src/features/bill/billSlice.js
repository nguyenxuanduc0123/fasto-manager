import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListBill: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  getDetailBill: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getDetailBillById: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  validBill: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const billSlice = createSlice({
  name: '@bill',
  initialState,
  reducers: {
    // GET LIST BILL
    getListBill: (state, action) => {
      state.getListBill.status.processing = !!action.payload
      state.getListBill.status.success = false
      state.getListBill.status.fail = false
      state.getListBill.query = action.payload
    },
    successGetListBill: (state, action) => {
      state.getListBill.list = action.payload
      state.getListBill.status.success = true
      state.getListBill.status.processing = false
    },
    failGetListBill: (state) => {
      state.getListBill.status.fail = true
      state.getListBill.status.processing = false
    },

    // GET DETAIL BILL
    getDetailBill: (state, action) => {
      state.getDetailBill.status.processing = !!action.payload
      state.getDetailBill.status.fail = false
      state.getDetailBill.status.success = false
    },
    stopGetDetailBill: (state) => {
      state.getDetailBill.status.processing = false
    },
    failGetDetailBill: (state) => {
      state.getDetailBill.state.fail = true
    },
    successGetDetailBill: (state, action) => {
      state.getDetailBill.detail = action.payload
      state.getDetailBill.status.success = true
    },

    // GET DETAIL BILL BY ID
    getDetailBillById: (state, action) => {
      state.getDetailBillById.status.processing = !!action.payload
      state.getDetailBillById.status.fail = false
      state.getDetailBillById.status.success = false
    },
    stopGetDetailBillById: (state) => {
      state.getDetailBillById.status.processing = false
    },
    failGetDetailBillById: (state) => {
      state.getDetailBillById.state.fail = true
    },
    successGetDetailBillById: (state, action) => {
      state.getDetailBillById.detail = action.payload
      state.getDetailBillById.status.success = true
    },

    // VALID BILL
    validBill: (state, action) => {
      state.validBill.status.processing = !!action.payload
      state.validBill.status.fail = false
      state.validBill.status.success = false
    },
    stopValidBill: (state) => {
      state.validBill.status.processing = false
    },
    failValidBill: (state) => {
      state.validBill.status.fail = true
      state.validBill.status.processing = false
    },
    successValidBill: (state) => {
      state.validBill.status.success = true
      state.validBill.status.processing = false
    },
  },
})

export const {
  getListBill,
  successGetListBill,
  failGetListBill,
  getDetailBill,
  stopGetDetailBill,
  failGetDetailBill,
  successGetDetailBill,
  getDetailBillById,
  stopGetDetailBillById,
  failGetDetailBillById,
  successGetDetailBillById,
  validBill,
  stopValidBill,
  failValidBill,
  successValidBill,
} = billSlice.actions

export const selectListBill = (state) => state.bill.getListBill.list
export const selectDetailBill = (state) => state.bill.getDetailBill.detail

export const selectDetailBillById = (state) => state.bill.getDetailBill.detail

export const selectListBillQuery = (state) => state.bill.getListBill.query

export default billSlice.reducer
