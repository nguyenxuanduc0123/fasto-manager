import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import VoucherAPI from '../../api/voucher/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  createVoucher,
  deleteVoucher,
  editVoucher,
  failCreateVoucher,
  failDeleteVoucher,
  failEditVoucher,
  failGetDetailVoucher,
  failGetListVoucher,
  getDetailVoucher,
  getListVoucher,
  selectListVoucherQuery,
  successCreateVoucher,
  successDeleteVoucher,
  successEditVoucher,
  successGetDetailVoucher,
  successGetListVoucher,
} from './voucherSlice'

function* _getListVoucher({ payload }) {
  try {
    const response = yield call(VoucherAPI.getListVoucher, payload)
    const result = response.data
    yield put(successGetListVoucher(result))
  } catch (error) {
    yield put(failGetListVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Get list voucher failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _createVoucher({ payload }) {
  try {
    yield call(VoucherAPI.createVoucher, payload)
    yield put(successCreateVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Create voucher success',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/voucher')
  } catch (error) {
    yield put(failCreateVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Create voucher failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _editVoucher({ payload }) {
  try {
    yield call(VoucherAPI.editVoucher, payload)
    yield put(successEditVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Update voucher success!',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/voucher')
  } catch (error) {
    yield put(failEditVoucher())
  }
}

function* _getDetailVoucher({ payload }) {
  try {
    const response = yield call(VoucherAPI.detailVoucher, payload)
    const result = response.data

    yield put(successGetDetailVoucher(result))
  } catch (error) {
    yield put(failGetDetailVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Get detail voucher failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _deleteVoucher({ payload }) {
  try {
    yield call(VoucherAPI.deleteVoucher, payload)
    yield put(successDeleteVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Delete voucher successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListVoucherQuery)
    yield put(getListVoucher(query))
  } catch (error) {
    yield put(failDeleteVoucher())
    yield put(
      enqueueSnackbar({
        message: 'Delete voucher failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* voucherSaga() {
  yield all([
    takeLatest(getListVoucher, _getListVoucher),
    takeLatest(createVoucher, _createVoucher),
    takeLatest(editVoucher, _editVoucher),
    takeLatest(getDetailVoucher, _getDetailVoucher),
    takeLatest(deleteVoucher, _deleteVoucher),
  ])
}
