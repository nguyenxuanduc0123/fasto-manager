import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListVoucher: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  createVoucher: {
    status: { ...INIT_REQUEST_STATUS },
  },
  editVoucher: {
    status: { ...INIT_REQUEST_STATUS },
  },
  getDetailVoucher: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  deleteVoucher: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const voucherSlice = createSlice({
  name: '@voucher',
  initialState,
  reducers: {
    // GET LIST VOUCHER
    getListVoucher: (state, action) => {
      state.getListVoucher.status.processing = !!action.payload
      state.getListVoucher.status.success = false
      state.getListVoucher.status.fail = false
      state.getListVoucher.query = action.payload
    },
    successGetListVoucher: (state, action) => {
      state.getListVoucher.list = action.payload
      state.getListVoucher.status.success = true
      state.getListVoucher.status.processing = false
    },
    failGetListVoucher: (state) => {
      state.getListVoucher.status.fail = true
      state.getListVoucher.status.processing = false
    },

    // CREATE VOUCHER
    createVoucher: (state, action) => {
      state.createVoucher.status.processing = !!action.payload
      state.createVoucher.status.fail = false
      state.createVoucher.status.success = false
    },
    stopCreateVoucher: (state) => {
      state.createVoucher.status.processing = false
    },
    failCreateVoucher: (state) => {
      state.createVoucher.status.fail = true
      state.createVoucher.status.processing = false
    },
    successCreateVoucher: (state) => {
      state.createVoucher.status.success = true
      state.createVoucher.status.processing = false
    },

    // EDIT VOUCHER
    editVoucher: (state, action) => {
      state.editVoucher.status.processing = !!action.payload
      state.editVoucher.status.fail = false
      state.editVoucher.status.success = false
    },
    stopEditVoucher: (state) => {
      state.editVoucher.status.processing = false
    },
    failEditVoucher: (state) => {
      state.editVoucher.status.fail = true
      state.editVoucher.status.processing = false
    },
    successEditVoucher: (state) => {
      state.editVoucher.status.success = true
      state.editVoucher.status.processing = false
    },

    // GET DETAIL VOUCHER
    getDetailVoucher: (state, action) => {
      state.getDetailVoucher.status.processing = !!action.payload
      state.getDetailVoucher.status.fail = false
      state.getDetailVoucher.status.success = false
    },
    stopGetDetailVoucher: (state) => {
      state.getDetailVoucher.status.processing = false
    },
    failGetDetailVoucher: (state) => {
      state.getDetailVoucher.state.fail = true
    },
    successGetDetailVoucher: (state, action) => {
      state.getDetailVoucher.detail = action.payload
      state.getDetailVoucher.status.success = true
    },

    // DELETE VOUCHER
    deleteVoucher: (state, action) => {
      state.deleteVoucher.status.processing = !!action.payload
      state.deleteVoucher.status.fail = false
      state.deleteVoucher.status.success = false
    },
    stopDeleteVoucher: (state) => {
      state.deleteVoucher.status.processing = false
    },
    failDeleteVoucher: (state) => {
      state.deleteVoucher.status.fail = true
      state.deleteVoucher.status.processing = false
    },
    successDeleteVoucher: (state) => {
      state.deleteVoucher.status.process = false
      state.deleteVoucher.status.success = true
    },
  },
})

export const {
  getListVoucher,
  failGetListVoucher,
  successGetListVoucher,

  createVoucher,
  stopCreateVoucher,
  failCreateVoucher,
  successCreateVoucher,

  editVoucher,
  stopEditVoucher,
  failEditVoucher,
  successEditVoucher,

  getDetailVoucher,
  stopGetDetailVoucher,
  failGetDetailVoucher,
  successGetDetailVoucher,

  deleteVoucher,
  stopDeleteVoucher,
  failDeleteVoucher,
  successDeleteVoucher,
} = voucherSlice.actions

export const selectListVoucher = (state) => state.voucher.getListVoucher.list

export const selectListVoucherQuery = (state) =>
  state.voucher.getListVoucher.query

export const selectStatusCreateVoucher = (state) =>
  state.voucher.createVoucher.status

export const selectStatusEditVoucher = (state) =>
  state.voucher.editVoucher.status

export const selectDetailVoucher = (state) =>
  state.voucher.getDetailVoucher.detail

export default voucherSlice.reducer
