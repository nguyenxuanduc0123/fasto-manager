import { createSlice } from '@reduxjs/toolkit'
import config from '../../config'

export const initialState = {
  isOpen: [],
  borderRadius: config.borderRadius,
  opened: true,
}

export const themeSlice = createSlice({
  name: 'customization',
  initialState,
  reducers: {
    setMenu: (state, action) => {
      state.opened = action.payload
    },
    setBorderRadius: (state, action) => {
      state.borderRadius = action.payload
    },
    menuOpen: (state, action) => {
      state.isOpen = [action.payload]
    },
  },
})

export const { setMenu, setBorderRadius, menuOpen } = themeSlice.actions

export const isMenuOpened = (state) => state.theme.opened

export default themeSlice.reducer
