import { CssBaseline } from '@mui/material'
import { ThemeProvider as MuiThemeProvider } from '@mui/material/styles'

import { useAppSelector } from '../../app/hooks'
import theme from './index'

const ThemeProvider = (props) => {
  const customization = useAppSelector((state) => state.theme)

  return (
    <MuiThemeProvider theme={theme(customization)}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
      <CssBaseline />
      {props.children}
    </MuiThemeProvider>
  )
}

export default ThemeProvider
