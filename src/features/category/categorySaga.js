import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import CategoryAPI from '../../api/category/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  createCategory,
  deleteCategory,
  editCategory,
  failCreateCategory,
  failDeleteCategory,
  failEditCategory,
  failGetDetailCategory,
  failGetListCategory,
  getDetailCategory,
  getListCategory,
  selectListCategoryQuery,
  successCreateCategory,
  successDeleteCategory,
  successEditCategory,
  successGetDetailCategory,
  successGetListCategory,
} from './categorySlice'

function* _getListCategory({ payload }) {
  try {
    const response = yield call(CategoryAPI.getListCategory, payload)
    const result = response.data
    yield put(successGetListCategory(result))
  } catch (error) {
    yield put(failGetListCategory())
    yield put(
      enqueueSnackbar({
        message: 'Get list category failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _createCategory({ payload }) {
  try {
    yield call(CategoryAPI.createCategory, payload)
    yield put(successCreateCategory())
    yield put(
      enqueueSnackbar({
        message: 'Create category success',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/category')
  } catch (error) {
    yield put(failCreateCategory())
    yield put(
      enqueueSnackbar({
        message: 'Create category failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _editCategory({ payload }) {
  try {
    yield call(CategoryAPI.editCategory, payload)
    yield put(successEditCategory())
    yield put(
      enqueueSnackbar({
        message: 'Update category success!',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/category')
  } catch (error) {
    yield put(failEditCategory())
  }
}

function* _getDetailCategory({ payload }) {
  try {
    const response = yield call(CategoryAPI.detailCategory, payload)
    const result = response.data

    yield put(successGetDetailCategory(result))
  } catch (error) {
    yield put(failGetDetailCategory())
    yield put(
      enqueueSnackbar({
        message: 'Get detail category failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _deleteCategory({ payload }) {
  try {
    yield call(CategoryAPI.deleteCategory, payload)
    yield put(successDeleteCategory())
    yield put(
      enqueueSnackbar({
        message: 'Delete category successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListCategoryQuery)
    yield put(getListCategory(query))
  } catch (error) {
    yield put(failDeleteCategory())
    yield put(
      enqueueSnackbar({
        message: 'Delete category failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* categorySaga() {
  yield all([
    takeLatest(getListCategory, _getListCategory),
    takeLatest(createCategory, _createCategory),
    takeLatest(editCategory, _editCategory),
    takeLatest(getDetailCategory, _getDetailCategory),
    takeLatest(deleteCategory, _deleteCategory),
  ])
}
