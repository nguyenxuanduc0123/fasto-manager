import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_REQUEST_STATUS, INIT_QUERY } from '../../app/constant'

const initialState = {
  getListCategory: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  createCategory: {
    status: { ...INIT_REQUEST_STATUS },
  },
  editCategory: {
    status: { ...INIT_REQUEST_STATUS },
  },
  getDetailCategory: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  deleteCategory: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const categorySlice = createSlice({
  name: '@category',
  initialState,
  reducers: {
    // GET LIST CATEGORY
    getListCategory: (state, action) => {
      state.getListCategory.status.processing = !!action.payload
      state.getListCategory.status.success = false
      state.getListCategory.status.fail = false
      state.getListCategory.query = action.payload
    },
    successGetListCategory: (state, action) => {
      state.getListCategory.list = action.payload
      state.getListCategory.status.success = true
      state.getListCategory.status.processing = false
    },
    failGetListCategory: (state) => {
      state.getListCategory.status.fail = true
      state.getListCategory.status.processing = false
    },

    // CREATE CATEGORY
    createCategory: (state, action) => {
      state.createCategory.status.processing = !!action.payload
      state.createCategory.status.fail = false
      state.createCategory.status.success = false
    },
    stopCreateCategory: (state) => {
      state.createCategory.status.processing = false
    },
    failCreateCategory: (state) => {
      state.createCategory.status.fail = true
      state.createCategory.status.processing = false
    },
    successCreateCategory: (state) => {
      state.createCategory.status.success = true
      state.createCategory.status.processing = false
    },

    // EDIT CATEGORY
    editCategory: (state, action) => {
      state.editCategory.status.processing = !!action.payload
      state.editCategory.status.fail = false
      state.editCategory.status.success = false
    },
    stopEditCategory: (state) => {
      state.editCategory.status.processing = false
    },
    failEditCategory: (state) => {
      state.editCategory.status.fail = true
      state.editCategory.status.processing = false
    },
    successEditCategory: (state) => {
      state.editCategory.status.success = true
      state.editCategory.status.processing = false
    },

    // GET DETAIL CATEGORY
    getDetailCategory: (state, action) => {
      state.getDetailCategory.status.processing = !!action.payload
      state.getDetailCategory.status.fail = false
      state.getDetailCategory.status.success = false
    },
    stopGetDetailCategory: (state) => {
      state.getDetailCategory.status.processing = false
    },
    failGetDetailCategory: (state) => {
      state.getDetailCategory.state.fail = true
    },
    successGetDetailCategory: (state, action) => {
      state.getDetailCategory.detail = action.payload
      state.getDetailCategory.status.success = true
    },

    // DELETE CATEGORY
    deleteCategory: (state, action) => {
      state.deleteCategory.status.processing = !!action.payload
      state.deleteCategory.status.fail = false
      state.deleteCategory.status.success = false
    },
    stopDeleteCategory: (state) => {
      state.deleteCategory.status.processing = false
    },
    failDeleteCategory: (state) => {
      state.deleteCategory.status.fail = true
      state.deleteCategory.status.processing = false
    },
    successDeleteCategory: (state) => {
      state.deleteCategory.status.process = false
      state.deleteCategory.status.success = true
    },
  },
})

export const {
  getListCategory,
  failGetListCategory,
  successGetListCategory,

  createCategory,
  stopCreateCategory,
  failCreateCategory,
  successCreateCategory,

  editCategory,
  stopEditCategory,
  failEditCategory,
  successEditCategory,

  getDetailCategory,
  stopGetDetailCategory,
  failGetDetailCategory,
  successGetDetailCategory,

  deleteCategory,
  stopDeleteCategory,
  failDeleteCategory,
  successDeleteCategory,
} = categorySlice.actions

export const selectListCategory = (state) => state.category.getListCategory.list

export const selectListCategoryQuery = (state) =>
  state.category.getListCategory.query

export const selectStatusCreateCategory = (state) =>
  state.category.createCategory.status

export const selectStatusEditCategory = (state) =>
  state.category.editCategory.status

export const selectDetailCategory = (state) =>
  state.category.getDetailCategory.detail

export default categorySlice.reducer
