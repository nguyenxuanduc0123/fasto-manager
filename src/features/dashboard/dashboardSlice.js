import { createSlice } from '@reduxjs/toolkit'
import { INIT_REQUEST_STATUS } from '../../app/constant'

const initialState = {
  getTopTurnover: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  getTopTurnoverShop: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
}

export const dashboardSlice = createSlice({
  name: '@dashboard',
  initialState,
  reducers: {
    getTopTurnover: (state, action) => {
      state.getTopTurnover.status.processing = !!action.payload
      state.getTopTurnover.status.success = false
      state.getTopTurnover.status.fail = false
    },
    stopGetTopTurnover: (state) => {
      state.getTopTurnover.status.processing = false
    },
    failGetTopTurnover: (state) => {
      state.getTopTurnover.status.fail = true
    },
    successGetTopTurnover: (state, action) => {
      state.getTopTurnover.status.success = true
      state.getTopTurnover.detail = action.payload
    },

    getTopTurnoverShop: (state, action) => {
      state.getTopTurnoverShop.status.processing = !!action.payload
      state.getTopTurnoverShop.status.success = false
      state.getTopTurnoverShop.status.fail = false
    },
    stopGetTopTurnoverShop: (state) => {
      state.getTopTurnoverShop.status.processing = false
    },
    failGetTopTurnoverShop: (state) => {
      state.getTopTurnoverShop.status.fail = true
    },
    successGetTopTurnoverShop: (state, action) => {
      state.getTopTurnoverShop.status.success = true
      state.getTopTurnoverShop.detail = action.payload
    },
  },
})

export const {
  getTopTurnover,
  failGetTopTurnover,
  stopGetTopTurnover,
  successGetTopTurnover,

  getTopTurnoverShop,
  stopGetTopTurnoverShop,
  failGetTopTurnoverShop,
  successGetTopTurnoverShop,
} = dashboardSlice.actions

export const selectTopTurnover = (state) =>
  state.dashboard.getTopTurnover.detail

export const selectTopTurnoverShop = (state) =>
  state.dashboard.getTopTurnoverShop.detail

export default dashboardSlice.reducer
