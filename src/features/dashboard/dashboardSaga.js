import { put, call, all, takeLatest } from 'redux-saga/effects'
import DashboardAPI from '../../api/dashboard/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  failGetTopTurnover,
  failGetTopTurnoverShop,
  getTopTurnover,
  getTopTurnoverShop,
  successGetTopTurnover,
  successGetTopTurnoverShop,
} from './dashboardSlice'

function* _getTopTurnover({ payload }) {
  try {
    const response = yield call(DashboardAPI.getTopTurnover, payload)
    const result = response.data
    yield put(successGetTopTurnover(result))
  } catch {
    yield put(failGetTopTurnover())
    yield put(
      enqueueSnackbar({
        message: 'Get top turnover failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _getTopTurnoverShop({ payload }) {
  try {
    const response = yield call(DashboardAPI.getTopTurnoverShop, payload)
    const result = response.data
    yield put(successGetTopTurnoverShop(result))
  } catch {
    yield put(failGetTopTurnoverShop())
    yield put(
      enqueueSnackbar({
        message: 'Get top turnover failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* dashboardSaga() {
  yield all([takeLatest(getTopTurnover, _getTopTurnover)])
  yield all([takeLatest(getTopTurnoverShop, _getTopTurnoverShop)])
}
