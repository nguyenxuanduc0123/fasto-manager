import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import ProductAPI from '../../api/product/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  createProduct,
  deleteProduct,
  editProduct,
  failCreateProduct,
  failDeleteProduct,
  failEditProduct,
  failGetDetailProduct,
  failGetListProduct,
  getDetailProduct,
  getListProduct,
  selectListProductQuery,
  successCreateProduct,
  successDeleteProduct,
  successEditProduct,
  successGetDetailProduct,
  successGetListProduct,
} from './productSlice'

function* _getListProduct({ payload }) {
  try {
    const response = yield call(ProductAPI.getListProduct, payload)
    const result = response.data
    yield put(successGetListProduct(result))
  } catch (error) {
    yield put(failGetListProduct())
    yield put(
      enqueueSnackbar({
        message: 'Get list product failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _createProduct({ payload }) {
  try {
    yield call(ProductAPI.createProduct, payload)
    yield put(successCreateProduct())
    yield put(
      enqueueSnackbar({
        message: 'Create product success',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/product')
  } catch (error) {
    yield put(failCreateProduct())
    yield put(
      enqueueSnackbar({
        message: 'Create product failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _editProduct({ payload }) {
  try {
    yield call(ProductAPI.editProduct, payload)
    yield put(successEditProduct())
    yield put(
      enqueueSnackbar({
        message: 'Update product success!',
        options: {
          variant: 'success',
        },
      })
    )
    window.location.replace('/product')
  } catch (error) {
    yield put(failEditProduct())
  }
}

function* _getDetailProduct({ payload }) {
  try {
    const response = yield call(ProductAPI.detailProduct, payload)
    const result = response.data

    yield put(successGetDetailProduct(result))
  } catch (error) {
    yield put(failGetDetailProduct())
    yield put(
      enqueueSnackbar({
        message: 'Get detail product failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _deleteProduct({ payload }) {
  try {
    yield call(ProductAPI.deleteProduct, payload)
    yield put(successDeleteProduct())
    yield put(
      enqueueSnackbar({
        message: 'Delete product successed',
        options: {
          variant: 'success',
        },
      })
    )
    const query = yield select(selectListProductQuery)
    yield put(getListProduct(query))
  } catch (error) {
    yield put(failDeleteProduct())
    yield put(
      enqueueSnackbar({
        message: 'Delete product failed!',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* productSaga() {
  yield all([
    takeLatest(getListProduct, _getListProduct),
    takeLatest(createProduct, _createProduct),
    takeLatest(editProduct, _editProduct),
    takeLatest(getDetailProduct, _getDetailProduct),
    takeLatest(deleteProduct, _deleteProduct),
  ])
}
