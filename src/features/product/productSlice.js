import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListProduct: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  createProduct: {
    status: { ...INIT_REQUEST_STATUS },
  },
  editProduct: {
    status: { ...INIT_REQUEST_STATUS },
  },
  getDetailProduct: {
    status: { ...INIT_REQUEST_STATUS },
    detail: {},
  },
  deleteProduct: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const productSlice = createSlice({
  name: '@product',
  initialState,
  reducers: {
    // GET LIST PRODUCT
    getListProduct: (state, action) => {
      state.getListProduct.status.processing = !!action.payload
      state.getListProduct.status.success = false
      state.getListProduct.status.fail = false
      state.getListProduct.query = action.payload
    },
    successGetListProduct: (state, action) => {
      state.getListProduct.list = action.payload
      state.getListProduct.status.success = true
      state.getListProduct.status.processing = false
    },
    failGetListProduct: (state) => {
      state.getListProduct.status.fail = true
      state.getListProduct.status.processing = false
    },

    // CREATE PRODUCT
    createProduct: (state, action) => {
      state.createProduct.status.processing = !!action.payload
      state.createProduct.status.fail = false
      state.createProduct.status.success = false
    },
    stopCreateProduct: (state) => {
      state.createProduct.status.processing = false
    },
    failCreateProduct: (state) => {
      state.createProduct.status.fail = true
      state.createProduct.status.processing = false
    },
    successCreateProduct: (state) => {
      state.createProduct.status.success = true
      state.createProduct.status.processing = false
    },

    // EDIT PRODUCT
    editProduct: (state, action) => {
      state.editProduct.status.processing = !!action.payload
      state.editProduct.status.fail = false
      state.editProduct.status.success = false
    },
    stopEditProduct: (state) => {
      state.editProduct.status.processing = false
    },
    failEditProduct: (state) => {
      state.editProduct.status.fail = true
      state.editProduct.status.processing = false
    },
    successEditProduct: (state) => {
      state.editProduct.status.success = true
      state.editProduct.status.processing = false
    },

    // GET DETAIL PRODUCT
    getDetailProduct: (state, action) => {
      state.getDetailProduct.status.processing = !!action.payload
      state.getDetailProduct.status.fail = false
      state.getDetailProduct.status.success = false
    },
    stopGetDetailProduct: (state) => {
      state.getDetailProduct.status.processing = false
    },
    failGetDetailProduct: (state) => {
      state.getDetailProduct.state.fail = true
    },
    successGetDetailProduct: (state, action) => {
      state.getDetailProduct.detail = action.payload
      state.getDetailProduct.status.success = true
    },

    // DELETE PRODUCT
    deleteProduct: (state, action) => {
      state.deleteProduct.status.processing = !!action.payload
      state.deleteProduct.status.fail = false
      state.deleteProduct.status.success = false
    },
    stopDeleteProduct: (state) => {
      state.deleteProduct.status.processing = false
    },
    failDeleteProduct: (state) => {
      state.deleteProduct.status.fail = true
      state.deleteProduct.status.processing = false
    },
    successDeleteProduct: (state) => {
      state.deleteProduct.status.process = false
      state.deleteProduct.status.success = true
    },
  },
})

export const {
  getListProduct,
  failGetListProduct,
  successGetListProduct,

  createProduct,
  stopCreateProduct,
  failCreateProduct,
  successCreateProduct,

  editProduct,
  stopEditProduct,
  failEditProduct,
  successEditProduct,

  getDetailProduct,
  stopGetDetailProduct,
  failGetDetailProduct,
  successGetDetailProduct,

  deleteProduct,
  stopDeleteProduct,
  failDeleteProduct,
  successDeleteProduct,
} = productSlice.actions

export const selectListProduct = (state) => state.product.getListProduct.list

export const selectListProductQuery = (state) =>
  state.product.getListProduct.query

export const selectStatusCreateProduct = (state) =>
  state.product.createProduct.status

export const selectStatusEditProduct = (state) =>
  state.product.editProduct.status

export const selectDetailProduct = (state) =>
  state.product.getDetailProduct.detail

export default productSlice.reducer
