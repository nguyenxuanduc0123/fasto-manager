import { createSlice } from '@reduxjs/toolkit'

export const initialState = {
  notifications: [],
}

export const snackbarSlice = createSlice({
  name: '@snackbar',
  initialState,
  reducers: {
    enqueueSnackbar: (state, action) => {
      const key = action.payload.options && action.payload.options.key
      state.notifications.push({
        ...action.payload,
        key: key || new Date().getTime() + Math.random(),
      })
    },
    closeSnackbar: (state, action) => {
      const dismissAll = !action.payload.key

      state.notifications = state.notifications.map((notification) =>
        dismissAll || notification.key === action.payload.key
          ? { ...notification, dismissed: true }
          : { ...notification }
      )
    },
    removeSnackbar: (state, action) => {
      state.notifications = state.notifications.filter(
        (notification) => notification.key !== action.payload
      )
    },
  },
})

export const { enqueueSnackbar, closeSnackbar, removeSnackbar } =
  snackbarSlice.actions

export const selectSnackbarNotifications = (state) =>
  state.snackbar.notifications

export default snackbarSlice.reducer
