import { createSlice } from '@reduxjs/toolkit'
import { INIT_LIST, INIT_QUERY, INIT_REQUEST_STATUS } from '../../app/constant'

export const initialState = {
  getListUser: {
    list: { ...INIT_LIST },
    status: { ...INIT_REQUEST_STATUS },
    query: { ...INIT_QUERY },
  },
  blockUser: {
    status: { ...INIT_REQUEST_STATUS },
  },
}

export const userSlice = createSlice({
  name: '@user',
  initialState,
  reducers: {
    // GET LIST USER
    getListUser: (state, action) => {
      state.getListUser.status.processing = !!action.payload
      state.getListUser.status.success = false
      state.getListUser.status.fail = false
      state.getListUser.query = action.payload
    },
    successGetListUser: (state, action) => {
      state.getListUser.list = action.payload
      state.getListUser.status.success = true
      state.getListUser.status.processing = false
    },
    failGetListUser: (state) => {
      state.getListUser.status.fail = true
      state.getListUser.status.processing = false
    },

    // BLOCK USER
    blockUser: (state, action) => {
      state.blockUser.status.processing = !!action.payload
      state.blockUser.status.fail = false
      state.blockUser.status.success = false
    },
    stopeBlockUser: (state) => {
      state.blockUser.status.processing = false
    },
    failBlockUser: (state) => {
      state.blockUser.status.fail = true
      state.blockUser.status.processing = false
    },
    successBlockUser: (state) => {
      state.blockUser.status.success = true
      state.blockUser.status.processing = false
    },

    setQuery: (state, action) => {
      state.getListUser.query = action.payload
    },
  },
})

export const {
  getListUser,
  successGetListUser,
  failGetListUser,

  blockUser,
  stopeBlockUser,
  failBlockUser,
  successBlockUser,

  setQuery,
} = userSlice.actions

export const selectListUser = (state) => state.user.getListUser.list
export const selectListUserQuery = (state) => state.user.getListUser.query
export const selectBlockUserStatus = (state) => state.user.blockUser.status

export default userSlice.reducer
