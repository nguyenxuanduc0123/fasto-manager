import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import UserAPI from '../../api/user/api'
import { enqueueSnackbar } from '../snackbar/snackbarSlice'
import {
  blockUser,
  failBlockUser,
  failGetListUser,
  getListUser,
  selectListUserQuery,
  successBlockUser,
  successGetListUser,
} from './userSlice'

function* _getListUser({ payload }) {
  try {
    const response = yield call(UserAPI.getListUser, payload)
    const result = response.data
    yield put(successGetListUser(result))
  } catch (error) {
    yield put(failGetListUser())
    yield put(
      enqueueSnackbar({
        message: 'Get list user failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

function* _blockUser({ payload }) {
  try {
    yield call(UserAPI.blockUser, payload)
    yield put(successBlockUser())
    const query = yield select(selectListUserQuery)
    yield put(getListUser(query))
    yield put(
      enqueueSnackbar({
        message: 'Update user successed',
        options: {
          variant: 'success',
        },
      })
    )
  } catch (error) {
    yield put(failBlockUser())
    yield put(
      enqueueSnackbar({
        message: 'Update user failed',
        options: {
          variant: 'warning',
        },
      })
    )
  }
}

export function* userSaga() {
  yield all([
    takeLatest(getListUser, _getListUser),
    takeLatest(blockUser, _blockUser),
  ])
}
