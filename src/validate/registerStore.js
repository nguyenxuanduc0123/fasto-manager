import * as yup from 'yup'

export const phoneRegExp =
  /^0(([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

export const emailRegExp =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const registerStoreSchema = yup.object().shape({
  name: yup.string().trim().required('Name is required'),
  email: yup
    .string()
    .trim()
    .matches(emailRegExp, 'Invalid email address')
    .required('Email is required'),
  location: yup.array().min(1, 'Location is required'),
  password: yup
    .string()
    .trim()
    .min(8, 'Should be 8 chars minimum')
    .required('Password is required'),
  phoneNumber: yup
    .string()
    .matches(phoneRegExp, 'Phone number is not valid')
    .min(8)
    .required('Please enter your mobile')
    .trim(),
  description: yup.string().trim().required('Description is required'),
})

export { registerStoreSchema }
