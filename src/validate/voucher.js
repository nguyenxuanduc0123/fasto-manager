import * as yup from 'yup'

const voucherSchema = yup.object().shape({
  name: yup.string().trim().required('Name is required'),
  description: yup.string().trim().required('Description is required'),
  limitPerUser: yup
    .number()
    .required('Limit per User is required')
    .min(1, 'Discount is greater than or equal to 1')
    .typeError('Discount is required'),
  maxDiscount: yup
    .number()
    .required('Max Discount is required')
    .min(1, 'Discount is greater than or equal to 1')
    .typeError('Discount is required'),
  quantity: yup
    .number()
    .required('Quantity is required')
    .min(1, 'Discount is greater than or equal to 1')
    .typeError('Discount is required'),
  valueDiscount: yup
    .number()
    .required('Value Discount is required')
    .min(1, 'Discount is greater than or equal to 1')
    .typeError('Discount is required'),
  valueNeed: yup
    .number()
    .required('Value Need is required')
    .min(1, 'Discount is greater than or equal to 1')
    .typeError('Discount is required'),
  image: yup
    .array()
    .test('test-card-length', 'Voucher image is required', (value) => {
      return value[0].url
    }),
  startedAt: yup
    .string()
    .required('Start time is required')
    .test(
      'start-time',
      'The start time does not exceed the end time',
      function (value = '') {
        return +value < +this.parent.endedAt
      }
    ),

  endedAt: yup
    .string()
    .required('End time is required')
    .test(
      'endTime',
      'The end time does not less than current day',
      function (value = '') {
        return +value > Date.now()
      }
    ),
})

export { voucherSchema }
