import * as yup from 'yup'

const newsValidationSchema = yup.object({
  title: yup
    .string()
    .max(255, 'The title should not exceed 255')
    .trim()
    .required('The title is required'),
  address: yup.string(),
  content: yup
    .string()
    .nullable()
    .trim()
    .max(2000, 'The content should not exceed 2000 characters')
    .required('Enter a valid content'),
  latitude: yup.string(),
  subTitle: yup
    .string()
    .max(255, 'The title should not exceed 255')
    .trim()
    .required('The subtitle is required'),
  image: yup
    .array()
    .test('test-card-length', 'News image is required', (value) => {
      return value[0].url
    }),
})

export default newsValidationSchema
