import * as yup from 'yup'

const categorySchema = yup.object().shape({
  name: yup.string().trim().required('Name is required'),
})

export { categorySchema }
