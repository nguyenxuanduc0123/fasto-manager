import * as yup from 'yup'

const productSchema = yup.object().shape({
  name: yup.string().trim().required('Name is required'),
  price: yup
    .number()
    .test(
      'len',
      'product price must be less than 1 billion',
      (val = 0) => val < Math.pow(10, 9)
    )
    .required('The price is required'),
  image: yup
    .array()
    .test('voucher-image-length', 'Product image is required', (value) => {
      return !!value[0].url
    }),
  description: yup
    .string()
    .trim()
    .max(2000, 'The description should not exceed 2000 characters')
    .required('The description is required'),
  categoryId: yup.number().required('Category is required'),
})

export { productSchema }
