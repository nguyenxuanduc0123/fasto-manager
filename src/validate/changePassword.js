import * as yup from 'yup'

const changePasswordSchema = yup.object().shape({
  confirmNewPassword: yup
    .string()
    .trim()
    .required('Confirm New Password is required')
    .test(
      'test-card-length',
      'Confirm New Password is incorrect',
      function (value) {
        return this.parent.newPassword === value
      }
    ),
  newPassword: yup.string().trim().required('New Password is required'),
  oldPassword: yup.string().trim().required('Old Password is required'),
})

export { changePasswordSchema }
