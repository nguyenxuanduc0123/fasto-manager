import { configureStore } from '@reduxjs/toolkit'
import { useMemo } from 'react'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './saga'
import snackbarReducer from '../features/snackbar/snackbarSlice'
import themeReducer from '../features/theme/themeSlice'
import authReducer from '../features/authentication/authSlice'
import dashboardReducer from '../features/dashboard/dashboardSlice'
import categoryReducer from '../features/category/categorySlice'
import userReducer from '../features/user/userSlice'
import storeReducer from '../features/store/storeSlice'
import productReducer from '../features/product/productSlice'
import voucherReducer from '../features/voucher/voucherSlice'
import accountReducer from '../features/account/accountSlice'
import billReducer from '../features/bill/billSlice'
import { logger } from './middleware'

export const saveState = (state) => {
  try {
    const seria = JSON.stringify(state)
    localStorage.setItem('state', seria)
  } catch (error) {
    //Ignore writting error
  }
}

let flag = true
let store

const initStore = (preloadedState) => {
  const sagaMiddleware = createSagaMiddleware()

  const store = configureStore({
    reducer: {
      theme: themeReducer,
      auth: authReducer,
      snackbar: snackbarReducer,
      dashboard: dashboardReducer,
      category: categoryReducer,
      user: userReducer,
      store: storeReducer,
      product: productReducer,
      voucher: voucherReducer,
      account: accountReducer,
      bill: billReducer,
    },
    preloadedState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(logger, sagaMiddleware),
  })

  store.subscribe(() => {
    saveState({
      theme: store.getState().theme,
    })
  })

  sagaMiddleware.run(rootSaga)

  return store
}

export const configureAppStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  if (preloadedState && flag) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    flag = false
  }

  if (typeof window === 'undefined') return _store
  if (!flag) {
    store = _store
    flag = true
  }

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => configureAppStore(initialState), [initialState])
  return store
}
