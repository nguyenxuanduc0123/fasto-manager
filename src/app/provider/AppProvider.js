import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { SnackbarProvider } from 'notistack'
import useAppSnackbar from '../../features/snackbar/useAppSnackbar'

const AppSnackbarProvider = ({ children }) => {
  useAppSnackbar()

  return <>{children}</>
}

export default function AppProvider({ children }) {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <SnackbarProvider maxSnack={3}>
        <AppSnackbarProvider>{children}</AppSnackbarProvider>
      </SnackbarProvider>
    </LocalizationProvider>
  )
}
