import { Provider } from 'react-redux'
import ThemeProvider from 'src/features/theme/ThemeProvider'
import AppProvider from './provider/AppProvider'

const Root = ({ store, Component, pageProps }) => {
  const getLayout = Component.getLayout || ((page) => page)

  return (
    <Provider store={store}>
      <ThemeProvider>
        <AppProvider>{getLayout(<Component {...pageProps} />)}</AppProvider>
      </ThemeProvider>
    </Provider>
  )
}
export default Root
