import { all } from 'redux-saga/effects'
import authSaga from '../features/authentication/authSaga'
import { dashboardSaga } from '../features/dashboard/dashboardSaga'
import { categorySaga } from '../features/category/categorySaga'
import { userSaga } from '../features/user/userSaga'
import { storeSaga } from '../features/store/storeSaga'
import { productSaga } from '../features/product/productSaga'
import { voucherSaga } from '../features/voucher/voucherSaga'
import { accountSaga } from '../features/account/accountSaga'
import { billSaga } from '../features/bill/billSaga'

export default function* rootSaga() {
  yield all([
    authSaga(),
    dashboardSaga(),
    categorySaga(),
    userSaga(),
    storeSaga(),
    productSaga(),
    voucherSaga(),
    accountSaga(),
    billSaga(),
  ])
}
