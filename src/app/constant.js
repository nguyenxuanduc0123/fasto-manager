export const gridSpacing = 3
export const drawerWidth = 260
export const appDrawerWidth = 320

export const disableMenu = {
  disableColumnMenu: true,
  sortable: false,
}

export const INIT_LIST = {
  result: [],
  paginate: {
    page: 1,
  },
}

export const INIT_REQUEST_STATUS = {
  processing: false,
  fail: false,
  success: false,
}

export const INIT_QUERY = {
  page: 1,
  size: 10,
  sort: 'id,desc',
}

export const statusDiscountType = [
  {
    value: 'PERCENT',
    label: '%',
  },
  {
    value: 'PRICE',
    label: 'VND',
  },
]

export const mapMimeToExt = {
  'audio/aac': 'aac',
  'application/x-abiword': 'abw',
  'application/x-freearc': 'arc',
  'video/x-msvideo': 'avi',
  'video/avi': 'avi',
  'application/vnd.amazon.ebook': 'azw',
  'application/octet-stream': 'bin',
  'image/bmp': 'bmp',
  'application/x-bzip': 'bz',
  'application/x-bzip2': 'bz2',
  'application/x-cdf': 'cda',
  'application/x-csh': 'csh',
  'text/css': 'css',
  'text/csv': 'csv',
  'application/msword': 'doc',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    'docx',
  'application/vnd.ms-fontobject': 'eot',
  'application/epub+zip': 'epub',
  'application/gzip': 'gz',
  'image/gif': 'epub',
  'text/html': 'html',
  'image/vnd.microsoft.icon': 'ico',
  'text/calendar': 'ics',
  'application/java-archive': 'jar',
  'image/jpeg': 'jpg',
  'text/javascript': 'js',
  'application/json': 'json',
  'application/ld+json': 'jsonld',
  'audio/midi audio/x-midi': 'midi',
  'audio/mpeg': 'mp3',
  'video/mp4': 'mp4',
  'video/mpeg': 'mpeg',
  'application/vnd.apple.installer+xml': 'mpkg',
  'application/vnd.oasis.opendocument.presentation': 'odp',
  'application/vnd.oasis.opendocument.spreadsheet': 'ods',
  'application/vnd.oasis.opendocument.text': 'odt',
  'audio/ogg': 'oga',
  'video/ogg': 'ogv',
  'application/ogg': 'ogx',
  'audio/opus': 'opus',
  'font/otf': 'otf',
  'image/png': 'png',
  'application/pdf': 'pdf',
  'application/x-httpd-php': 'php',
  'application/vnd.ms-powerpoint': 'ppt',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation':
    'pptx',
  'application/vnd.rar': 'rar',
  'application/rtf': 'rtf',
  'application/x-sh': 'sh',
  'image/svg+xml': 'svg',
  'application/x-shockwave-flash': 'swf',
  'application/x-tar': 'tar',
  'image/tiff': 'tiff',
  'video/mp2t': 'ts',
  'font/ttf': 'ttf',
  'text/plain': 'txt',
  'application/vnd.visio': 'vsd',
  'audio/wav': 'wav',
  'audio/webm': 'weba',
  'video/webm': 'webm',
  'image/webp': 'webp',
  'font/woff': 'woff',
  'font/woff2': 'woff2',
  'application/xhtml+xml': 'xhtml',
  'application/vnd.ms-excel': 'xls',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx',
  'application/xml': 'xml',
  'application/vnd.mozilla.xul+xml': 'xul',
  'application/zip': 'zip',
  'video/3gpp': '3gp',
  'video/3gpp2': '3g2',
  'application/x-7z-compressed': '7z',
  'video/quicktime': 'MOV',
  'video/x-flv': 'flv',
  'video/x-ms-wmv': 'wmv',
}

export const imageType = {
  'image/png': ['.png'],
  'image/jpeg': ['.jpeg'],
  'image/gif': ['.gif'],
}

export const videoType = {
  'video/mp4': ['.mp4'],
  'video/quicktime': ['.quicktime'],
}
export const mediaType = { ...imageType, ...videoType }

export const statusTurnover = [
  {
    value: 0,
    label: 'January',
  },
  {
    value: 1,
    label: 'February',
  },
  {
    value: 2,
    label: 'March',
  },
  {
    value: 3,
    label: 'April',
  },
  {
    value: 4,
    label: 'May',
  },
  {
    value: 5,
    label: 'June',
  },
  {
    value: 6,
    label: 'July',
  },
  {
    value: 7,
    label: 'August',
  },
  {
    value: 8,
    label: 'September',
  },
  {
    value: 9,
    label: 'October',
  },
  {
    value: 10,
    label: 'November',
  },
  {
    value: 11,
    label: 'December',
  },
]

export const statusUser = [
  {
    value: '',
    label: 'ALL',
  },
  {
    value: 'ACTIVATED',
    label: 'ACTIVATED',
  },
  {
    value: 'INACTIVE',
    label: 'INACTIVE',
  },
  {
    value: 'BLOCKED',
    label: 'BLOCKED',
  },
  {
    value: 'DELETED',
    label: 'DELETED',
  },
]

export const statusStore = [
  {
    value: '',
    label: 'ALL',
  },
  {
    value: 'ACTIVATED',
    label: 'ACTIVATED',
  },
  {
    value: 'DELETED',
    label: 'DELETED',
  },
  {
    value: 'BLOCKED',
    label: 'BLOCKED',
  },
]

export const statusProduct = [
  {
    value: 'NEW',
    label: 'NEW',
  },
  {
    value: 'HOT',
    label: 'HOT',
  },
]

export const statusVoucher = [
  {
    value: '',
    label: 'ALL',
  },
  {
    value: 'ACTIVATED',
    label: 'ACTIVATED',
  },
  {
    value: 'INACTIVE',
    label: 'INACTIVE',
  },
  {
    value: 'DELETED',
    label: 'DELETED',
  },
]

export const statusOfBill = [
  {
    value: 'PAID',
    label: 'PAID',
  },
  {
    value: 'PENDING',
    label: 'PENDING',
  },
  {
    value: 'DONE',
    label: 'DONE',
  },
  {
    value: 'FAILED',
    label: 'FAILED',
  },
  {
    value: 'CREATED',
    label: 'CREATED',
  },
]
