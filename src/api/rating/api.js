import appAxios from '../../constant/axios-config'
import { shopVersion } from '../../constant/version'

export const API_RATING = {
  GET_LIST_RATING: `${shopVersion}/management/ratings`,
}

export default class RatingAPI {
  static getListRating = (params = {}) =>
    appAxios.get(API_RATING.GET_LIST_RATING, params)
}
