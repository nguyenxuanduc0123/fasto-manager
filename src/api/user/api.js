import appAxios from '../..//constant/axios-config'
import { adminVersion } from '../..//constant/version'

export const API_USER = {
  GET_LIST_USER: `${adminVersion}/management/users`,
  BLOCK_USER: (id) => `${adminVersion}/management/users/block/${id}`,
}

export default class UserAPI {
  static getListUser = (params = {}) =>
    appAxios.get(API_USER.GET_LIST_USER, params)

  static blockUser = (params = {}) =>
    appAxios.delete(API_USER.BLOCK_USER(params.id), params)
}
