import appAxios from '../../constant/axios-config'
import { adminVersion, shopVersion } from '../../constant/version'

export const API_USER = {
  SIGN_IN: `${adminVersion}/authenticate`,
  SIGN_IN_SHOP: `${shopVersion}/authenticate`,
  CHANGE_PASSWORD: `${shopVersion}/authenticate/change-password`,
  REGISTER: `${shopVersion}/authenticate/register`,
  FORGOT_PASSWORD: `${shopVersion}/authenticate/forgot-password`,
  VERIFY_CODE: `${shopVersion}/authenticate/forgot-password/verify-code`,
  RESET_PASSWORD: `${shopVersion}/authenticate/forgot-password/reset`,
}

export default class UserAPI {
  static signIn = (payload) => appAxios.post(API_USER.SIGN_IN, payload)
  static signInShop = (payload) => appAxios.post(API_USER.SIGN_IN_SHOP, payload)
  static changePassword = (payload) =>
    appAxios.post(API_USER.CHANGE_PASSWORD, payload)

  static register = (payload) => appAxios.post(API_USER.REGISTER, payload)
  static forgotPassword = (payload) =>
    appAxios.post(API_USER.FORGOT_PASSWORD, payload)

  static verifyCode = (payload) => appAxios.post(API_USER.VERIFY_CODE, payload)
  static resetPassword = (payload) =>
    appAxios.post(API_USER.RESET_PASSWORD, payload)
}
