import appAxios from '../../constant/axios-config'
import { adminVersion, shopVersion } from '../../constant/version'

export const API_DASHBOARD = {
  GET_TOP_TURNOVER: `${adminVersion}/management/turnover/shops`,
  GET_TOP_TURNOVER_SHOP: `${shopVersion}/management/revenue`,
}

export default class DashboardAPI {
  static getTopTurnover = (params = {}) =>
    appAxios.get(API_DASHBOARD.GET_TOP_TURNOVER, params)

  static getTopTurnoverShop = (params = {}) =>
    appAxios.get(API_DASHBOARD.GET_TOP_TURNOVER_SHOP, params)
}
