import appAxios from '../../constant/axios-config'
import { shopVersion } from '../../constant/version'

export const API_PRODUCT = {
  CREATE_PRODUCT: `${shopVersion}/management/product`,
  GET_LIST_PRODUCT: `${shopVersion}/management/products`,
  EDIT_PRODUCT: `${shopVersion}/management/product/update`,
  DETAIL_PRODUCT: (id) => `${shopVersion}/management/product/detail/${id}`,
  DELETE_PRODUCT: (id) => `${shopVersion}/management/product/delete/${id}`,
}

export default class ProductAPI {
  static getListProduct = (params = {}) =>
    appAxios.get(API_PRODUCT.GET_LIST_PRODUCT, params)

  static createProduct = (params = {}) =>
    appAxios.post(API_PRODUCT.CREATE_PRODUCT, params)

  static editProduct = (params = {}) =>
    appAxios.put(API_PRODUCT.EDIT_PRODUCT, params)

  static detailProduct = (params = {}) =>
    appAxios.get(API_PRODUCT.DETAIL_PRODUCT(params.id), params)

  static deleteProduct = (params = {}) =>
    appAxios.delete(API_PRODUCT.DELETE_PRODUCT(params.id), params)
}
