import appAxios from '../../constant/axios-config'
import { adminVersion, shopVersion } from '../../constant/version'
import StorageUtil, { STORAGE_KEY } from '../../util/storage'

export const API_CATEGORY = {
  GET_LIST_CATEGORY: `${
    StorageUtil.get(STORAGE_KEY.ROLE) === 'shop' ? shopVersion : adminVersion
  }/management/categories`,
  CREATE_CATEGORY: `${adminVersion}/management/category`,
  EDIT_CATEGORY: (id) => `${adminVersion}/management/category/${id}`,
  DETAIL_CATEGORY: (id) => `${adminVersion}/management/categories/${id}`,
  DELETE_CATEGORY: (id) => `${adminVersion}/management/category/${id}`,
}

export default class CategoryAPI {
  static getListCategory = (params = {}) =>
    appAxios.get(API_CATEGORY.GET_LIST_CATEGORY, params)

  static createCategory = (params = {}) =>
    appAxios.post(API_CATEGORY.CREATE_CATEGORY, params)

  static editCategory = (params = {}) =>
    appAxios.put(API_CATEGORY.EDIT_CATEGORY(params.id), params)

  static detailCategory = (params = {}) =>
    appAxios.get(API_CATEGORY.DETAIL_CATEGORY(params.id), params)

  static deleteCategory = (params = {}) =>
    appAxios.delete(API_CATEGORY.DELETE_CATEGORY(params.id), params)
}
