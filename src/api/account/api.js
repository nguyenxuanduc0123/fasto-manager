import appAxios from '../../constant/axios-config'
import { shopVersion } from '../../constant/version'

export const API_ACCOUNT = {
  GET_INFORMATION: `${shopVersion}/information`,
  UPDATE_INFORMATION: `${shopVersion}/information`,
}

export default class AccountAPI {
  static getInfo = (params = {}) =>
    appAxios.get(API_ACCOUNT.GET_INFORMATION, params)

  static updateInfo = (params = {}) =>
    appAxios.patch(API_ACCOUNT.UPDATE_INFORMATION, params)
}
