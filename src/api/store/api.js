import appAxios from '../../constant/axios-config'
import { adminVersion } from '../../constant/version'

export const API_STORE = {
  GET_LIST_STORE: `${adminVersion}/management/shops`,
  ACTIVE_STORE: (id) => `${adminVersion}/management/shops/active/${id}`,
  BLOCK_STORE: (id) => `${adminVersion}/management/shops/shops/block/${id}`,
  DETAIL_STORE: (id) => `${adminVersion}/management/shops/shops/${id}`,
  PRODUCT_STORE: (id) => `${adminVersion}/management/products/shop/${id}`,
  VOUCHER_STORE: (id) => `${adminVersion}/management/vouchers/shops/${id}`,
  VOUCHER_DETAIL_STORE: (id) =>
    `${adminVersion}/management/vouchers/shops/detail/${id}`,
  GET_STORE_TURNOVER: (id) => `${adminVersion}/management/turnover/shops/${id}`,
}

export default class StoreAPI {
  static getListStore = (params = {}) =>
    appAxios.get(API_STORE.GET_LIST_STORE, params)

  static activeStore = (params = {}) =>
    appAxios.patch(API_STORE.ACTIVE_STORE(params.id), params)

  static blockStore = (params = {}) =>
    appAxios.patch(API_STORE.BLOCK_STORE(params.id), params)

  static getDetailStore = (params = {}) =>
    appAxios.get(API_STORE.DETAIL_STORE(params.id), params)

  static getProductOfStore = (params = {}) =>
    appAxios.get(API_STORE.PRODUCT_STORE(params.id), params)

  static getVoucherOfStore = (params = {}) =>
    appAxios.get(API_STORE.VOUCHER_STORE(params.id), params)

  static getDetailVoucherOfStore = (params = {}) =>
    appAxios.get(API_STORE.VOUCHER_DETAIL_STORE(params.id), params)

  static getStoreTurnover = (params = {}) =>
    appAxios.get(API_STORE.GET_STORE_TURNOVER(params.id), params)
}
