import appAxios from '../..//constant/axios-config'
import { shopVersion } from '../..//constant/version'

export const API_BILL = {
  GET_LIST_BILL: `${shopVersion}/management/bills`,
  GET_DETAIL_BILL: `${shopVersion}/management/bill/code`,
  GET_DETAIL_BILL_BY_ID: (id) => `${shopVersion}/management/bill/${id}`,
  VALID_BILL: (code) =>
    `${shopVersion}/management/bill/valid-code?code=${code}`,
}

export default class BillAPI {
  static getListBill = (params = {}) =>
    appAxios.get(API_BILL.GET_LIST_BILL, params)

  static getDetailBill = (params = {}) =>
    appAxios.get(API_BILL.GET_DETAIL_BILL, params)

  static getDetailBillById = (params = {}) =>
    appAxios.get(API_BILL.GET_DETAIL_BILL_BY_ID(params.id), params)

  static validBill = (params = {}) =>
    appAxios.patch(API_BILL.VALID_BILL(params.code), params)
}
