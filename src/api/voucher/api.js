import appAxios from '../../constant/axios-config'
import { shopVersion } from '../../constant/version'

export const API_VOUCHER = {
  CREATE_VOUCHER: `${shopVersion}/management/voucher`,
  GET_LIST_VOUCHER: `${shopVersion}/management/vouchers`,
  EDIT_VOUCHER: (id) => `${shopVersion}/management/voucher/update/${id}`,
  DETAIL_VOUCHER: (id) => `${shopVersion}/management/voucher/detail/${id}`,
  DELETE_VOUCHER: (id) => `${shopVersion}/management/voucher/delete/${id}`,
}

export default class VoucherAPI {
  static getListVoucher = (params = {}) =>
    appAxios.get(API_VOUCHER.GET_LIST_VOUCHER, params)

  static createVoucher = (params = {}) =>
    appAxios.post(API_VOUCHER.CREATE_VOUCHER, params)

  static editVoucher = (params = {}) =>
    appAxios.put(API_VOUCHER.EDIT_VOUCHER(params.id), params)

  static detailVoucher = (params = {}) =>
    appAxios.get(API_VOUCHER.DETAIL_VOUCHER(params.id), params)

  static deleteVoucher = (params = {}) =>
    appAxios.delete(API_VOUCHER.DELETE_VOUCHER(params.id), params)
}
