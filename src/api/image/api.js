import appAxios from '../../constant/axios-config'

export const API_IMAGE = {
  PRE_SIGNED: `/user/management/images/pre-signed`,
}

export default class ImageAPI {
  static preSignedImage = (fileList) =>
    appAxios.post(API_IMAGE.PRE_SIGNED, fileList)

  static uploadImage = (
    preSignedURL,
    file,
    config = {
      headers: { 'content-type': file.type },
    }
  ) => appAxios.put(preSignedURL, file, config)
}
