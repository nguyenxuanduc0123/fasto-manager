import ImageAPI from '../../api/image/api'
import { mapMimeToExt } from '../../app/constant'

const useUploadImage = () => {
  const onUploadImages = async (images) => {
    if (images.length > 0) {
      const fileList = images.map((file) => ({
        fileName: file.name,
        fileType: mapMimeToExt[file.type],
        imageFolderType: 'PRODUCT',
      }))

      let responsePresigned

      try {
        responsePresigned = await ImageAPI.preSignedImage(fileList)

        const uploadImagePromises = responsePresigned.data.map(
          (presigned, index) =>
            ImageAPI.uploadImage(presigned.preSignedURL, images[index])
        )
        await Promise.all(uploadImagePromises)
      } catch (error) {
        console.error('ERROR', error)
      }

      const listImagePatching =
        responsePresigned?.data?.map((v, index) => ({
          url: v.url,
          type: fileList[index].fileType,
        })) || []
      return listImagePatching
    }
    return []
  }

  return {
    onUploadImages,
  }
}
export default useUploadImage
