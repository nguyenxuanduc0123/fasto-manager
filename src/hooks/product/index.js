import useUploadImageProduct from './useUploadImage'
import useLoadingPlaceholder from './useLoadingPlaceholder'
import useValidatorMedia from './useValidatorMedia'

const ERROR = {
  ERROR_NHAP_CHI_TIET_KICH_CO: 'ERROR_NHAP_CHI_TIET_KICH_CO',
  ERROR_NHAP_DANH_MUC_SAN_PHAM: 'ERROR_NHAP_DANH_MUC_SAN_PHAM',
  ERROR_NHAP_ANH_SAN_PHAM: 'ERROR_NHAP_ANH_SAN_PHAM',
  ERROR_VUOT_QUA_15_ANH: 'ERROR_VUOT_QUA_15_ANH',
  ERROR_PRODUCT_API: 'ERROR_PRODUCT_API',
  ERROR_NHAP_CHI_TIET_PHAN_LOAI_HANG: 'ERROR_NHAP_CHI_TIET_PHAN_LOAI_HANG',
}

export {
  useUploadImageProduct,
  useLoadingPlaceholder,
  useValidatorMedia,
  ERROR,
}
