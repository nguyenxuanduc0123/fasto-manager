const useLoadingPlaceholder = ({
  fieldValue,
  setFieldValue,
  index,
  maxFile,
}) => {
  const addLoadingPlaceholder = ({ acceptedFiles, values }) => {
    let idx = 0
    // Set up loading if there is !v.url place to fill in
    const newValues = values[fieldValue].map((v, itemPos) => {
      if (itemPos < index) return v
      if ((!v.url || index === itemPos) && idx < acceptedFiles.length) {
        ++idx
        return { ...v, isLoading: true }
      }
      return v
    })

    // Set up (acceptedFiles.length - idx) loading place
    setFieldValue(fieldValue, [
      ...newValues,
      ...new Array(acceptedFiles.length - idx).fill(0).map(() => {
        return {
          url: '',
          type: '',
          isLoading: true,
        }
      }),
      ...new Array(
        maxFile - (acceptedFiles.length - idx + newValues.length) > 0 ? 1 : 0
      )
        .fill(0)
        .map(() => {
          return {
            url: '',
            type: '',
          }
        }),
    ])
  }

  const removeLoadingPlaceholder = ({ numberOfRemoveElements, values }) => {
    let count = 0
    setFieldValue(
      fieldValue,
      values[fieldValue].filter((v) => {
        if (count == numberOfRemoveElements) return true
        if (v.isLoading) {
          ++count
          return false
        }
        return true
      })
    )
  }

  const patchLoadingPlaceholder = ({ responseFile, values }) => {
    let idx = 0
    setFieldValue(
      fieldValue,
      values[fieldValue].map((v) => {
        if (v.isLoading && idx < responseFile.length) {
          return { ...v, isLoading: false, ...responseFile[idx++] }
        }
        return v
      })
    )
  }

  return {
    addLoadingPlaceholder,
    patchLoadingPlaceholder,
    removeLoadingPlaceholder,
  }
}

export default useLoadingPlaceholder
