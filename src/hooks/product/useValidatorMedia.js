/* eslint-disable no-prototype-builtins */
const useValidatorMedia = () => {
  const validator = (file) => {
    if (file.hasOwnProperty('duration')) {
      if (!file.duration || file.duration >= 91) {
        return {
          code: 'VIDEO_TOO_LONG',
          message: '',
        }
      }
    }
    return null
  }

  return { validator }
}

export default useValidatorMedia
