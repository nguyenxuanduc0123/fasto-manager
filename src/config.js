const config = {
  basename: '/',
  defaultPath: '/dashboard',
  borderRadius: 12,
}

export default config
