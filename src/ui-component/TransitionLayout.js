import { motion, AnimatePresence } from 'framer-motion'

function TransitionLayout({ children }) {
  const variant = {
    start: { opacity: [0.8] },
    middle: {
      opacity: [0.8, 1],
    },
  }
  return (
    <>
      <AnimatePresence exitBeforeEnter>
        <motion.main
          layout
          initial="start"
          animate="middle"
          transition={{ duration: 3, type: 'tween' }}
          variants={variant}
        >
          {children}
        </motion.main>
      </AnimatePresence>
    </>
  )
}

export default TransitionLayout
