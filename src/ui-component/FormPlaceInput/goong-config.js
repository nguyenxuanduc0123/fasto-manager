import axios from "axios"

const GoongClient = axios.create({
  baseURL: "https://rsapi.goong.io",
  params: {
    api_key: process.env.NEXT_PUBLIC_GOONG_KEY,
  },
})

export default GoongClient

export const GoongApi = {
  PLACES: "/Place/AutoComplete",
  PLACE_DETAIL: "/Place/Detail",
}
