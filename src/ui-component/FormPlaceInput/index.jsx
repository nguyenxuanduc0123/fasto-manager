import { useField } from 'formik'
import dynamic from 'next/dynamic'
import { useState } from 'react'
import { getPlaceDetail, getPlaces } from './goong.api'

const FormAutocomplete = dynamic(() => import('../FormAutocomplete'), {
  ssr: false,
})

const FormPlaceInput = ({ callback, ...props }) => {
  const [field] = useField(props.name)
  const [errGoongio, setErrGoongio] = useState(null)

  const mapResponseToOptions = (data) =>
    data.map((v) => ({
      label: v.description,
      value: {
        structured_formatting: v.structured_formatting,
        place_id: v.place_id,
      },
    }))

  const handleSuggestion = async (inputValue) => {
    try {
      const response = await getPlaces({
        input: inputValue || field.value.name || 'a',
        location: '10.7757,106.7004',
        radius: '1000',
      })
      setErrGoongio(null)
      return mapResponseToOptions(response.predictions)
    } catch (error) {
      console.error({ error })
      setErrGoongio(error?.response || null)
      return []
    }
  }

  const handleSelect = async (selected, setValue) => {
    try {
      setValue({
        name: selected.label,
        lat: '',
        lng: '',
      })
      const response = await getPlaceDetail({
        place_id: selected.value.place_id,
      })
      const data = response.result
      setValue({
        name: selected.label,
        lat: data.geometry.location.lat,
        lng: data.geometry.location.lng,
      })
      if (callback)
        callback({
          name: selected.label,
          lat: data.geometry.location.lat,
          lng: data.geometry.location.lng,
        })
    } catch (error) {
      setValue({
        name: '',
        lat: '',
        lng: '',
      })
      if (callback) {
        callback({
          name: '',
          lat: '',
          lng: '',
        })
      }
    }
  }

  return (
    <FormAutocomplete
      {...props}
      handleSuggestion={handleSuggestion}
      handleSelect={handleSelect}
      className="pito__place-container"
      classNamePrefix="pito__place"
      errGoongio={errGoongio}
    />
  )
}

export default FormPlaceInput
