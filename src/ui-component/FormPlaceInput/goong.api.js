import GoongClient, { GoongApi } from "./goong-config"

function getPlaces(params) {
  return GoongClient.get(GoongApi.PLACES, { params }).then(res => res.data)
}

function getPlaceDetail(params) {
  return GoongClient.get(GoongApi.PLACE_DETAIL, { params }).then(
    res => res.data
  )
}

export { getPlaces, getPlaceDetail }
