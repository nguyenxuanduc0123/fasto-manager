import { forwardRef } from 'react'

// material-ui
import { Card, CardContent, CardHeader, Divider } from '@mui/material'
import { useAppTheme } from '../../features/theme/hooks'
import HeaderContent from '../../ui-component/HeaderContent'

// constant
const headerSX = {
  '& .MuiCardHeader-action': { mr: 0 },
}

const MainCard = forwardRef(
  (
    {
      border = false,
      boxShadow,
      children,
      content = true,
      contentClass = '',
      contentSX = {},
      darkTitle,
      secondary,
      shadow,
      sx = {},
      title,
      isBack = false,
      ...others
    },
    ref
  ) => {
    const theme = useAppTheme()

    return (
      <Card
        ref={ref}
        {...others}
        sx={{
          border: border ? '1px solid' : 'none',
          borderColor: theme.palette.primary[200] + 75,
          ':hover': {
            boxShadow: boxShadow
              ? shadow || '0 2px 14px 0 rgb(32 40 45 / 8%)'
              : 'inherit',
          },
          ...sx,
        }}
      >
        {/* card header and action */}
        {!darkTitle && title && (
          <CardHeader
            sx={{ ...headerSX, padding: isBack ? '10px 20px' : '' }}
            title={<HeaderContent isBack={isBack}>{title}</HeaderContent>}
            action={secondary}
          />
        )}
        {darkTitle && title && (
          <CardHeader
            sx={{ ...headerSX, padding: isBack ? '10px 20px' : '' }}
            title={<HeaderContent isBack={isBack}>{title}</HeaderContent>}
            action={secondary}
          />
        )}

        {/* content & header divider */}
        {title && <Divider />}

        {/* card content */}
        {content && (
          <CardContent sx={contentSX} className={contentClass}>
            {children}
          </CardContent>
        )}
        {!content && children}
      </Card>
    )
  }
)

MainCard.displayName = 'MainCard'

export default MainCard
