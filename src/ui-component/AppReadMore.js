import React, { useState } from 'react'
import { Box, Typography } from '@mui/material'
import { useAppTheme } from 'src/features/theme/hooks'

const AppReadMore = ({ children }) => {
  const theme = useAppTheme()
  const text = children
  const [isReadMore, setIsReadMore] = useState(true)
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore)
  }
  return (
    <Box className="text">
      {isReadMore ? text?.slice(0, 120) : text} {'  '}
      <Typography
        variant="body1"
        onClick={toggleReadMore}
        sx={{
          cursor: 'pointer',
          color: theme.palette.primary[800],
          display: 'inline-block',
        }}
      >
        {text?.length > 120 && (isReadMore ? '...Read more' : 'Show less')}
      </Typography>
    </Box>
  )
}

export default AppReadMore
