import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Stack,
  Typography,
} from '@mui/material'
import { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import {
  getDetailBill,
  getDetailBillById,
  selectDetailBill,
  validBill,
} from '../../features/bill/billSlice'
import { formatPrice } from '../../util/helpers'

export default function ConfirmDialog({
  openConfirmDialog,
  setOpenConfirmDialog,
  idDetail,
  type = 'id',
}) {
  const dispatch = useAppDispatch()
  const detailBill = useAppSelector(selectDetailBill)

  const fetchDetailBill = () => {
    dispatch(
      type === 'id'
        ? getDetailBillById({ id: idDetail })
        : getDetailBill({ code: { idDetail } })
    )
  }

  useEffect(() => {
    if (idDetail) {
      fetchDetailBill()
    }
  }, [idDetail])

  return (
    <Dialog
      open={openConfirmDialog}
      onClose={() => setOpenConfirmDialog(false)}
      aria-labelledby={'Bill Detail'}
      aria-describedby={'Bill Detail'}
      maxWidth="xs"
      fullWidth
    >
      <DialogTitle>Bill Detail</DialogTitle>

      <DialogContent>
        <Box>
          <Typography variant="h6" fontWeight={600} mb={1}>
            Detail
          </Typography>

          {detailBill?.billItemResponseDtos?.map((item) => (
            <Box
              mt={1}
              maxHeight="65vh"
              sx={{
                overflow: 'hidden',
                overflowY: 'auto',
              }}
            >
              <Box display="flex" alignItems="center" mb={1}>
                <Typography mx={2}>{item?.amount} x </Typography>
                <Typography
                  variant="h6"
                  sx={{
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    WebkitLineClamp: '2',
                    WebkitBoxOrient: 'vertical',
                  }}
                >
                  {item?.nameProduct}
                </Typography>

                <Box
                  display="flex"
                  alignItems="center"
                  sx={{ marginLeft: 'auto' }}
                >
                  <Typography variant="h6">{`${formatPrice(
                    item?.price * item?.amount || 0
                  )} đ`}</Typography>
                </Box>
              </Box>
              <Divider sx={{ my: 0.5 }} />
            </Box>
          ))}

          <Typography variant="h6" fontWeight={600} mb={1}>
            Voucher
          </Typography>

          <Box ml={2}>
            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Voucher code:
              </Typography>
              <Typography variant="body1" fontWeight={600}>
                {detailBill?.voucherName}
              </Typography>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Total (Temporary):
              </Typography>
              <Typography variant="body1" fontWeight={600}>
                {formatPrice(detailBill?.totalOrigin || 0)} đ
              </Typography>
            </Stack>

            <Stack direction="row" justifyContent="space-between">
              <Typography variant="body1" fontWeight={600}>
                Discount:
              </Typography>
              <Typography variant="body1" fontWeight={600} color="red">
                -{' '}
                {detailBill?.voucherId
                  ? formatPrice(detailBill?.totalDiscount)
                  : formatPrice(0)}{' '}
                đ
              </Typography>
            </Stack>

            <Divider sx={{ my: 2 }} />
          </Box>

          <Stack direction="row" justifyContent="space-between">
            <Typography variant="body1" fontWeight={600}>
              Total payment:
            </Typography>
            <Typography variant="body1" fontWeight={600}>
              {formatPrice(detailBill?.totalPayment || 0)} đ
            </Typography>
          </Stack>
        </Box>
      </DialogContent>

      <DialogActions>
        {detailBill.status === 'PAID' && (
          <Button
            onClick={() => {
              setOpenConfirmDialog(false)
              dispatch(validBill({ code: String(detailBill?.code) }))
            }}
          >
            Valid
          </Button>
        )}
        <Button onClick={() => setOpenConfirmDialog(false)}>Close</Button>
      </DialogActions>
    </Dialog>
  )
}
