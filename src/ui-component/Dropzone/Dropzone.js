import { Box, Typography } from '@mui/material'
import { mediaType } from '../../app/constant'
import { useAppTheme } from '../../features/theme/hooks'
import {
  useLoadingPlaceholder,
  useUploadImageProduct,
  useValidatorMedia,
} from '../../hooks/product'
import { useEffect, useRef } from 'react'
import { useDropzone } from 'react-dropzone'
import * as Helpers from '../../util/helpers'
import ListMedia from './ListMedia'

const MAX_SIZE = 5000000

const Dropzone = ({
  setFieldValue,
  setFieldError,
  values,
  fieldValue,
  acceptFileType = mediaType,
  maxFile,
  maxSize = MAX_SIZE,

  media,
  index,
  handleClickMedia,
  handleRemoveMedia,
  handleEditImage,
  label,
}) => {
  const theme = useAppTheme()

  // Using newest value: https://reactjs.org/docs/hooks-faq.html#why-am-i-seeing-stale-props-or-state-inside-my-function
  const ref = useRef(values)

  const { onUploadImages } = useUploadImageProduct()
  const {
    addLoadingPlaceholder,
    removeLoadingPlaceholder,
    patchLoadingPlaceholder,
  } = useLoadingPlaceholder({
    fieldValue,
    setFieldValue,
    index,
    maxFile,
  })
  const { validator } = useValidatorMedia()

  const { getRootProps, getInputProps } = useDropzone({
    accept: acceptFileType,
    maxFiles: maxFile > 0 ? maxFile : 15,
    validator,
    maxSize: maxSize,
    onDropAccepted: async (acceptedFiles) => {
      const imageFiles = acceptedFiles.filter(
        (file) => !Helpers.isVideoURl(file.type)
      )

      try {
        addLoadingPlaceholder({ acceptedFiles, values: ref.current })
        if (imageFiles.length + values[fieldValue].length > maxFile + 1) {
          setFieldError(fieldValue, 'Exceed the maximum number of files')
          throw new Error('Max lenght 10 files')
        }
        const responseUpload = await Promise.all([onUploadImages(imageFiles)])
        if (
          responseUpload &&
          (responseUpload[0]?.length > 0 || responseUpload[1]?.length > 0)
        ) {
          patchLoadingPlaceholder({
            responseFile: [...responseUpload[0]],
            values: ref.current,
          })
        }
      } catch (error) {
        removeLoadingPlaceholder({
          numberOfRemoveElements: acceptedFiles.length,
          values: ref.current,
        })
      }
    },

    onDropRejected: (files) => {
      const arrayFileImages = files.filter((file) =>
        file.file.type.includes('image')
      )
      if (arrayFileImages?.length > maxFile) {
        setFieldError(fieldValue, 'Exceed the maximum number of files')
      }
      if (files.length > 0 && !arrayFileImages.length) {
        setFieldError(fieldValue, 'Invalid uploaded file')
      }
      if (files[0].file.size > maxSize) {
        setFieldError(fieldValue, 'Size of the file is too large')
      }
    },
  })

  useEffect(() => {
    ref.current = values
  })

  return (
    <>
      {label && !media?.url && (
        <Typography
          variant="h5"
          sx={{
            fontSize: '18px',
            position: 'absolute',
            left: '50%',
            top: '75%',
            transform: 'translate(-50%,-50%)',
            opacity: 0.7,
            width: '100%',
            textAlign: 'center',
          }}
        >
          {label}
        </Typography>
      )}
      <Box
        sx={{
          height: '160px',
          width: '160px',
          '.dropzone-product': {
            height: '160px',
            width: '160px',
            cursor: 'pointer',
            borderWidth: '1px',
            borderRadius: '8px',
            borderColor: theme.palette.primary[200],
            backgroundColor: theme.palette.grey[50],
            borderStyle: media.url ? 'hidden' : 'dashed',
            transition: 'all 300ms',
            '&:hover': {
              borderColor: theme.palette.primary.dark,
            },
          },
          '.img-dropzone': {
            objectFit: 'contain',
            borderRadius: '8px',
          },
        }}
      >
        <Box
          {...getRootProps({
            className: 'dropzone-product',
          })}
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <input {...getInputProps()} />
          <ListMedia
            media={media}
            index={index}
            handleClickMedia={handleClickMedia}
            handleRemoveMedia={handleRemoveMedia}
            handleEditImage={handleEditImage}
          />
        </Box>
      </Box>
    </>
  )
}

export default Dropzone
