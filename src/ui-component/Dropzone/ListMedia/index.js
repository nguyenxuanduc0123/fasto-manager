import React, { useCallback, useMemo, useState } from 'react'
import { isVideoURl } from '../../../util/helpers'
import './style.module.scss'
import ProgressCircle from '../../../ui-component/ProgressCircle'
import { Box, ButtonBase, Stack, Paper, CardMedia } from '@mui/material'
import { styled } from '@mui/system'
import { IconFileUpload, IconPencil, IconTrash } from '@tabler/icons'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: '0.25rem',
  textAlign: 'center',
  color: theme.palette.text.secondary,
}))

function secondsToTime(number) {
  const m = Math.floor((number % 3600) / 60)
      .toString()
      .padStart(2, '0'),
    s = Math.floor(number % 60)
      .toString()
      .padStart(2, '0')

  return `${m}:${s}`
}

const VideoBox = ({ video }) => {
  const [videoDuration, setVideoDuration] = useState(0)

  const handleVideoMounted = useCallback((element) => {
    if (element) {
      element.onloadedmetadata = function () {
        element.currentTime = 1
        setVideoDuration(element.duration)
      }
    }
  }, [])

  const videoTime = useMemo(() => {
    return secondsToTime(videoDuration)
  }, [videoDuration])

  return (
    <Box>
      <video
        width={80}
        height={80}
        style={{ objectFit: 'cover' }}
        ref={handleVideoMounted}
      >
        <source src={video.url || video} type="video/mp4" />
      </video>
      {!!videoDuration && <span>{videoTime}</span>}
    </Box>
  )
}

const ImageBox = ({ image }) => {
  return (
    <Box sx={{ height: '100%' }} key={image.url}>
      <CardMedia
        sx={{ height: '100%', objectFit: 'cover', borderRadius: '4px' }}
        component="img"
        src={image.url}
        alt="image"
      />
    </Box>
  )
}

const ListMedia = ({
  media,
  index,
  handleClickMedia,
  handleRemoveMedia,
  handleEditImage,
}) => {
  const isVideo = isVideoURl(media.url || media)
  const isImage = !isVideo
  const { isLoading } = media

  if (!media.url && !isLoading) {
    return (
      <Box height={30} width={30} strokeWidth={1} component={IconFileUpload} />
    )
  }

  return (
    <Box
      sx={{
        height: '100%',
        position: 'relative',
        width: '100%',
      }}
      display="flex"
      alignItems="center"
      justifyContent="center"
      key={media.url || new Date().getTime() + Math.random()}
    >
      <ProgressCircle isLoading={isLoading}>
        <ButtonBase
          sx={{ height: '100%', width: '100%' }}
          type="button"
          onClick={() =>
            handleClickMedia && !isLoading && handleClickMedia({ pos: index })
          }
        >
          <Box sx={{ height: '100%', width: '100%' }}>
            {isVideo ? <VideoBox video={media} /> : <ImageBox image={media} />}
          </Box>
        </ButtonBase>

        <Stack
          sx={{
            position: 'absolute',
            top: '4px',
            right: '4px',
            '.MuiPaper-root': {
              borderRadius: '50%',
            },
          }}
          direction="row"
          spacing={2}
        >
          {isImage && handleEditImage && (
            <Item>
              <ButtonBase
                type="button"
                onClick={() => handleEditImage(media.url, index)}
              >
                <IconPencil />
              </ButtonBase>
            </Item>
          )}
          <Item>
            <ButtonBase
              type="button"
              onClick={(e) => {
                e.stopPropagation()
                handleRemoveMedia({ pos: index })
              }}
            >
              <IconTrash />
            </ButtonBase>
          </Item>
        </Stack>
      </ProgressCircle>
    </Box>
  )
}
export default ListMedia
