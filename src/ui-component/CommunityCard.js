import FavoriteIcon from '@mui/icons-material/Favorite'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined'
import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@mui/material'
import Router from 'next/router'
import React from 'react'
import { useAppDispatch } from 'src/app/hooks'
import {
  deleteComment,
  deleteReviews,
} from 'src/features/community/communitySlice'
import TableAction from 'src/features/TableActions'
import AppReadMore from 'src/ui-component/AppReadMore'
const CommunityCard = ({ review, canDelete = false }) => {
  const dispatch = useAppDispatch()

  const removeReview = (id) => {
    dispatch(deleteReviews({ id }))
  }

  const removeComment = (id) => {
    dispatch(deleteComment({ id }))
  }

  return (
    <Card
      sx={{
        '& .MuiCardContent-root': {
          padding: ' 0 16px 8px',
        },
        '& .MuiCardHeader-root': {
          padding: '16px',
        },
        '& .MuiCardActions-root': {
          padding: '16px 16px 8px',
        },
        '& .MuiButtonBase-root': {
          padding: '0',
          fontSize: 16,
        },
        '& .MuiList-root': {
          padding: '0 0 8px',
        },
      }}
    >
      <CardHeader
        avatar={<Avatar src={review?.user?.imageUrl} aria-label="recipe" />}
        action={
          canDelete && (
            <TableAction
              showDeleteButton
              deleteAction={() => {
                removeReview(review?.id)
              }}
            />
          )
        }
        title={review?.user?.name}
        subheader={review?.createdAt}
      />

      <CardContent>
        <Typography variant="h4">{review?.title}</Typography>
        <Typography variant="body1">
          <AppReadMore>{review?.content}</AppReadMore>
        </Typography>
      </CardContent>

      <CardMedia
        component="img"
        image={review?.images?.[0]}
        alt="Paella dish"
        onClick={() => {
          Router.push(`/community/${review.id}`)
        }}
        sx={{ cursor: 'pointer' }}
      />
      <CardActions>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon sx={{ mr: 0.5 }} /> {review?.totalFavorite}
        </IconButton>
        <IconButton aria-label="share">
          <ChatOutlinedIcon sx={{ mr: 0.5 }} /> {review?.totalReply}
        </IconButton>
      </CardActions>

      {review?.reply && (
        <List>
          {review.reply.map((reply) => {
            return (
              <ListItem
                alignItems="flex-start"
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
              >
                <ListItemAvatar>
                  <Avatar alt="Remy Sharp" src={reply.user.imageUrl} />
                </ListItemAvatar>
                <ListItemText
                  primary={`${reply.user.name} | ${reply.modifiedAt}`}
                  secondary={
                    <React.Fragment>
                      <Typography
                        sx={{ display: 'inline' }}
                        component="span"
                        variant="body2"
                        color="text.primary"
                      >
                        {reply.content}
                      </Typography>
                    </React.Fragment>
                  }
                />
                <Box>
                  <TableAction
                    showDeleteButton
                    deleteAction={() => {
                      removeComment(reply.id)
                    }}
                  />
                </Box>
              </ListItem>
            )
          })}
        </List>
      )}
    </Card>
  )
}

export default CommunityCard
