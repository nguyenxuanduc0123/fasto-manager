import React, { useRef } from 'react'

const r = 12
const circ = 2 * Math.PI * r

const Circle = ({ colour }) => {
  const [count, setCount] = React.useState(0)
  const requestRef = useRef()

  const animate = () => {
    setCount((prevCount) => prevCount + 1)
    requestRef.current = requestAnimationFrame(animate)
  }

  React.useEffect(() => {
    requestRef.current = requestAnimationFrame(animate)
    return () => cancelAnimationFrame(requestRef.current)
  }, [])

  const strokePct = ((100 - count) * circ) / 100

  return (
    <circle
      r={r}
      cx={160}
      cy={46}
      fill="transparent"
      stroke={strokePct !== circ ? colour : ''} // remove colour as 0% sets full circumference
      strokeWidth={'2px'}
      strokeDasharray={circ}
      strokeDashoffset={strokePct}
      strokeLinecap="round"
    />
  )
}

const Text = () => {
  return (
    <text
      x="50%"
      y="75%"
      dominantBaseline="central"
      textAnchor="middle"
      fontSize={'1em'}
      fill={'#9A9A9A'}
    >
      Loading...
    </text>
  )
}

const ProgressCircle = ({ children, isLoading }) => {
  return isLoading ? (
    <svg width={92} height={92}>
      <g transform={`rotate(-90 ${'100 100'})`}>
        <Circle colour="#FFCF6F" />
      </g>
      <Text />
    </svg>
  ) : (
    children
  )
}

export default ProgressCircle
