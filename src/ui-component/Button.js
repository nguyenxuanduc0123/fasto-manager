import LinkTo from '../Link'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import AddOutlinedIcon from '@mui/icons-material/AddOutlined'

export const RedirectButton = ({ href, text }) => (
  <Box
    sx={{
      a: {
        textDecoration: 'none',
      },
    }}
  >
    <LinkTo href={href}>
      <Button startIcon={<AddOutlinedIcon />} size="large" variant="outlined">
        {text}
      </Button>
    </LinkTo>
  </Box>
)
