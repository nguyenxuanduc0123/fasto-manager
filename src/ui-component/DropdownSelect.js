import { ArrowDropDown, ArrowDropUp } from '@mui/icons-material'
import { MenuItem, Stack, Box } from '@mui/material'
import { useRef, useState } from 'react'
import MenuPopover from '../ui-component/MenuPopover'
import { SwapVert } from '@mui/icons-material'

const DropdownFilter = ({
  options = [],
  onChange,
  value,
  label = '',
  type,
}) => {
  const ref = useRef(null)
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          positon: 'relative',
          cursor: 'pointer',
          fontWeight: '500',
        }}
        ref={ref}
        onClick={handleOpen}
      >
        {label}
        {type === 'SORT' ? (
          <SwapVert
            fontSize="small"
            sx={{ display: open ? 'none' : 'block' }}
            color={value ? 'primary' : 'inherit'}
            ref={ref}
            onClick={handleOpen}
          />
        ) : (
          <ArrowDropDown
            sx={{
              display: open ? 'none' : 'block',
            }}
            color={value ? 'primary' : 'inherit'}
            ref={ref}
            onClick={handleOpen}
          />
        )}
        <ArrowDropUp
          sx={{ display: open ? 'block' : 'none' }}
          color={value ? 'primary' : 'inherit'}
          ref={ref}
          onClick={handleClose}
        />
      </Box>
      <MenuPopover
        open={open}
        onClose={handleClose}
        anchorEl={ref.current}
        sx={{
          mt: 1.5,
          ml: 0.75,
          width: 'fit-content',
          '& .MuiMenuItem-root': {
            px: 1,
            typography: 'body2',
            borderRadius: 0.75,
          },
        }}
      >
        <Stack spacing={0.75}>
          {options?.map((option) => {
            return (
              <MenuItem
                key={option.value}
                selected={option.value === value}
                onClick={() => {
                  handleClose()
                  onChange(option.value)
                }}
              >
                {option.label}
              </MenuItem>
            )
          })}
        </Stack>
      </MenuPopover>
    </>
  )
}

export default DropdownFilter
