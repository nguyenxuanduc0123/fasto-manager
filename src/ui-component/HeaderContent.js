import Box from '@mui/material/Box'
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos'
import React from 'react'
import { IconButton } from '@mui/material'
import { useNavigate } from 'react-router-dom'

const HeaderContent = ({ children, isBack }) => {
  const navigate = useNavigate()
  return (
    <Box sx={{ fontWeight: 600 }}>
      {isBack && (
        <IconButton
          size="large"
          sx={{ mr: '5px' }}
          onClick={() => navigate(-1)}
        >
          <ArrowBackIosIcon sx={{ position: 'relative', right: '-4px' }} />
        </IconButton>
      )}
      {children}
    </Box>
  )
}

export default HeaderContent
