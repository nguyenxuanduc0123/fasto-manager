import { Box } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import { useUploadImageProduct } from 'src/hooks/product'

const CustomCkeditor = ({ formik, content, isEdit }) => {
  const { onUploadImages } = useUploadImageProduct()

  const editorRef = useRef()
  const [editorLoaded, setEditorLoaded] = useState(false)
  const { CKEditor, ClassicEditor } = editorRef.current || {}

  useEffect(() => {
    editorRef.current = {
      CKEditor: require('@ckeditor/ckeditor5-react'),
      ClassicEditor: require('@ckeditor/ckeditor5-build-classic'),
    }
    setEditorLoaded(true)
  }, [])

  function UploadAdapter(loader) {
    return {
      upload: () => {
        return new Promise((resolve) => {
          loader.file.then((file) => {
            const response = onUploadImages([file])
            response.then((data) => {
              resolve({ default: data[0].url })
            })
          })
        })
      },
    }
  }

  function uploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) =>
      UploadAdapter(loader)
  }

  const styleEditor = (editor) => {
    editor.editing.view.change((writer) => {
      writer.setStyle(
        'min-height',
        '300px',
        editor.editing.view.document.getRoot()
      )
    })
  }

  return (
    <Box>
      {editorLoaded &&
        (isEdit ? (
          <CKEditor
            onInit={(editor) => styleEditor(editor)}
            config={{
              extraPlugins: [uploadAdapterPlugin],
              image: {
                toolbar: [],
              },
            }}
            editor={ClassicEditor}
            data={formik?.values['content']}
            onChange={(event, editor) => {
              const data = editor.getData()
              formik?.setFieldValue('content', data)
            }}
          />
        ) : (
          <CKEditor
            onInit={(editor) => styleEditor(editor)}
            disabled={true}
            editor={ClassicEditor}
            data={content}
            config={{ toolbar: [] }}
          />
        ))}
    </Box>
  )
}

export default CustomCkeditor
