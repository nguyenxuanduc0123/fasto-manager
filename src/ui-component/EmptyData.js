import { SearchOffOutlined } from '@mui/icons-material'
import { Box, Card, CardContent, Typography } from '@mui/material'
import { useAppTheme } from '../features/theme/hooks'
import React from 'react'

const EmptyData = () => {
  const theme = useAppTheme()
  return (
    <Card>
      <CardContent
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box sx={{ color: theme.palette.grey[300] }}>
          <SearchOffOutlined sx={{ width: '100px', height: '100px' }} />
        </Box>
        <Typography
          variant="h3"
          sx={{ color: theme.palette.grey[300], textAlign: 'center' }}
        >
          No Results To Show
        </Typography>
      </CardContent>
    </Card>
  )
}

export default EmptyData
