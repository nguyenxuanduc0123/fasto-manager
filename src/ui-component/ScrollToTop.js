import { ArrowUpward } from '@mui/icons-material'
import { Box } from '@mui/material'
import React from 'react'
import { useAppTheme } from 'src/features/theme/hooks'

const ScrollToTop = () => {
  const theme = useAppTheme()

  const scrollTop = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth',
    })
  }

  return (
    <Box
      onClick={scrollTop}
      sx={{
        position: 'fixed',
        right: 30,
        bottom: 30,
        width: 60,
        height: 60,
        borderRadius: 999,
        backgroundColor: theme.palette.grey[200],
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        cursor: 'pointer',
      }}
    >
      <ArrowUpward />
    </Box>
  )
}

export default ScrollToTop
