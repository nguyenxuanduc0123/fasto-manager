const { IconUser } = require('@tabler/icons')

const category = {
  id: 'category',
  title: 'Category',
  type: 'item',
  url: '/category',
  icon: IconUser,
}

export default category
