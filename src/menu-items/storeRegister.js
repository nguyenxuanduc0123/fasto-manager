const { IconUser } = require('@tabler/icons')

const storeRegister = {
  id: 'storeRegister',
  title: 'Store Register',
  type: 'item',
  url: '/store-register',
  icon: IconUser,
}

export default storeRegister
