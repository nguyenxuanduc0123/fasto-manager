const { IconUser } = require('@tabler/icons')

const users = {
  id: 'user',
  title: 'User',
  type: 'item',
  url: '/user',
  icon: IconUser,
}

export default users
