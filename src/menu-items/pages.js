import StorageUtil, { STORAGE_KEY } from '../util/storage'
import bill from './bill'
import category from './category'
import product from './product'
import rating from './rating'
import store from './store'
import storeRegister from './storeRegister'
import users from './users'
import voucher from './voucher'

const currentRole = StorageUtil.get(STORAGE_KEY.ROLE)

const pages = {
  id: 'manage',
  title: 'Management',
  type: 'group',
  children:
    currentRole === 'admin'
      ? [users, category, store, storeRegister]
      : [product, voucher, bill, rating],
}

export default pages
