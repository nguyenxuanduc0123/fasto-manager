// assets
import account from './account'
import system from './system'

const appearance = {
  id: 'manage',
  title: 'Setting',
  type: 'group',
  children: [system, account],
}

export default appearance
