const { IconUser } = require('@tabler/icons')

const voucher = {
  id: 'voucher',
  title: 'Voucher',
  type: 'item',
  url: '/voucher',
  icon: IconUser,
}

export default voucher
