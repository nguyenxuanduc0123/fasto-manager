import StorageUtil, { STORAGE_KEY } from '../util/storage'
import dashboard from './dashboard'
import pages from './pages'
import appearance from './setting'

const currentRole = StorageUtil.get(STORAGE_KEY.ROLE)

const menuItems = {
  items:
    currentRole === 'admin'
      ? [dashboard, pages]
      : [dashboard, pages, appearance],
}

export default menuItems
