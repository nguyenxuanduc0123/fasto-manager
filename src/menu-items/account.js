// assets
import { IconSettings } from '@tabler/icons'

const account = {
  id: 'account',
  title: 'Account',
  type: 'item',
  icon: IconSettings,
  url: '/account',
}

export default account
