const { IconUser } = require('@tabler/icons')

const store = {
  id: 'store',
  title: 'Store',
  type: 'item',
  url: '/store',
  icon: IconUser,
}

export default store
