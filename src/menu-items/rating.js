const { IconUser } = require('@tabler/icons')

const rating = {
  id: 'rating',
  title: 'Rating',
  type: 'item',
  url: '/rating',
  icon: IconUser,
}

export default rating
