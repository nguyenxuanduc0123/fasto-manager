// assets
import { IconSettings } from '@tabler/icons'

const system = {
  id: 'change-password',
  title: 'Change Password',
  type: 'item',
  icon: IconSettings,
  url: '/change-password',
}

export default system
