const { IconUser } = require('@tabler/icons')

const product = {
  id: 'product',
  title: 'Product',
  type: 'item',
  url: '/product',
  icon: IconUser,
}

export default product
