const { IconUser } = require('@tabler/icons')

const bill = {
  id: 'bill',
  title: 'Bill',
  type: 'item',
  url: '/bill',
  icon: IconUser,
}

export default bill
