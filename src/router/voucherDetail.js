import VoucherDetail from '../page/Voucher/Detail'

const voucherDetail = {
  path: '/voucher/detail/:voucherId',
  element: <VoucherDetail />,
}

export default voucherDetail
