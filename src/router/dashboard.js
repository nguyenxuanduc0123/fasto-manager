import Dashboard from '../page/Dashboard'

const dashboard = {
  path: '/',
  element: <Dashboard />,
}

export default dashboard
