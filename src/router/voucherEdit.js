import VoucherEdit from '../page/Voucher/update/Edit'

const voucherEdit = {
  path: '/voucher/edit/:voucherId',
  element: <VoucherEdit />,
}

export default voucherEdit
