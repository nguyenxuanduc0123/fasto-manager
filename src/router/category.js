import Category from '../page/Category'

const category = {
  path: '/category',
  element: <Category />,
}

export default category
