import VoucherDetail from '../page/Voucher/Detail'

const voucherDetailShop = {
  path: '/store/voucher/:voucherId',
  element: <VoucherDetail isAdmin={true} />,
}

export default voucherDetailShop
