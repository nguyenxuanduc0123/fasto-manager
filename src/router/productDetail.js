import ProductDetail from '../page/Product/Detail'

const productDetail = {
  path: '/product/detail/:productId',
  element: <ProductDetail />,
}

export default productDetail
