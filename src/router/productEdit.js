import ProductEdit from '../page/Product/update/Edit'

const productEdit = {
  path: '/product/edit/:productId',
  element: <ProductEdit />,
}

export default productEdit
