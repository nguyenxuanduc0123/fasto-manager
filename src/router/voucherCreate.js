import VoucherCreate from '../page/Voucher/update/Create'

const voucherCreate = {
  path: '/voucher/create',
  element: <VoucherCreate />,
}

export default voucherCreate
