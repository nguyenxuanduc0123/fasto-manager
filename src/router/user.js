import User from '../page/User'

const user = {
  path: '/user',
  element: <User />,
}

export default user
