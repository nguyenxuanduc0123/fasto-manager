import Store from '../page/Store'

const store = {
  path: '/store',
  element: <Store />,
}

export default store
