import Login from '../page/Login'

const login = {
  path: '/login',
  element: <Login />,
}

export default login
