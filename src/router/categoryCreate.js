import CategoryCreate from '../page/Category/update/Create'

const categoryCreate = {
  path: '/category/create',
  element: <CategoryCreate />,
}

export default categoryCreate
