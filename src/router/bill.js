import Bill from '../page/Bill'

const bill = {
  path: '/bill',
  element: <Bill />,
}

export default bill
