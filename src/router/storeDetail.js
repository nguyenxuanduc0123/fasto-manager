import StoreDetail from '../page/StoreDetail'

const storeDetail = {
  path: '/store-detail/:storeId',
  element: <StoreDetail />,
}

export default storeDetail
