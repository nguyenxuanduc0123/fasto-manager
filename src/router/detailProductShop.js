import ProductDetail from '../page/StoreDetail/ProductDetail'

const detailProductShop = {
  path: '/store/product/:productId',
  element: <ProductDetail />,
}

export default detailProductShop
