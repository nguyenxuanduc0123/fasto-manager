import StoreRegister from '../page/StoreRegister'

const storeRegister = {
  path: '/store-register',
  element: <StoreRegister />,
}

export default storeRegister
