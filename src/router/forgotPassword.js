import ForgotPassword from '../page/ForgotPassword'

const forgotPassword = {
  path: '/forgotPassword',
  element: <ForgotPassword />,
}

export default forgotPassword
