import Product from '../page/Product'

const product = {
  path: '/product',
  element: <Product />,
}

export default product
