import ProductCreate from '../page/Product/update/Create'

const productCreate = {
  path: '/product/create',
  element: <ProductCreate />,
}

export default productCreate
