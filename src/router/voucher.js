import Voucher from '../page/Voucher'

const voucher = {
  path: '/voucher',
  element: <Voucher />,
}

export default voucher
