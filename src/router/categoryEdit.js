import CategoryEdit from '../page/Category/update/Edit'

const categoryEdit = {
  path: '/category/edit/:categoryId',
  element: <CategoryEdit />,
}

export default categoryEdit
