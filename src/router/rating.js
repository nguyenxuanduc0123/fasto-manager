import Rating from '../page/Rating'

const rating = {
  path: '/rating',
  element: <Rating />,
}

export default rating
