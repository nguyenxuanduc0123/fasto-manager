import category from './category'
import categoryCreate from './categoryCreate'
import CategoryEdit from './categoryEdit'
import changePassword from './changePassword'
import store from './store'
import storeDetail from './storeDetail'
import storeRegister from './storeRegister'
import user from './user'
import login from './login'
import dashboard from './dashboard'
import product from './product'
import voucher from './voucher'
import bill from './bill'
import productCreate from './productCreate'
import productEdit from './productEdit'
import productDetail from './productDetail'
import voucherDetail from './voucherDetail'
import voucherEdit from './voucherEdit'
import voucherCreate from './voucherCreate'
import detailProductShop from './detailProductShop'
import voucherDetailShop from './voucherDetailShop'
import account from './account'
import rating from './rating'
import forgotPassword from './forgotPassword'

const appRouter = [
  login,
  dashboard,
  store,
  changePassword,
  account,
  category,
  categoryCreate,
  CategoryEdit,
  storeRegister,
  storeDetail,
  user,
  product,
  voucher,
  bill,
  productCreate,
  productEdit,
  productDetail,
  voucherDetail,
  voucherEdit,
  voucherCreate,
  detailProductShop,
  voucherDetailShop,
  rating,
  forgotPassword,
]

export default appRouter
