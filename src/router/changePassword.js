import ChangePassword from '../page/ChangePassword'

const changePassword = {
  path: '/change-password',
  element: <ChangePassword />,
}

export default changePassword
