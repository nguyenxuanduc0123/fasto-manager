import { CacheProvider } from '@emotion/react'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import AppProvider from './app/provider/AppProvider'
import { configureAppStore } from './app/store'
import './assets/scss/style.scss'
import createEmotionCache from './createEmotionCache'
import ThemeProvider from './features/theme/ThemeProvider'

import App from './App'

const clientSideEmotionCache = createEmotionCache()
const store = configureAppStore()

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <CacheProvider value={clientSideEmotionCache}>
      <head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </head>
      <Provider store={store}>
        <ThemeProvider>
          <AppProvider>
            <App />
          </AppProvider>
        </ThemeProvider>
      </Provider>
    </CacheProvider>
  </React.StrictMode>
)
