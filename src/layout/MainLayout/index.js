import { IconChevronRight } from '@tabler/icons'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'

// mui
import { AppBar, Box, Toolbar, useMediaQuery } from '@mui/material'
import { styled } from '@mui/material/styles'

// logic
import { drawerWidth } from '../../app/constant'
import { useAppSelector } from '../../app/hooks'
import { useAppTheme } from '../../features/theme/hooks'
import { setMenu } from '../../features/theme/themeSlice'
import navigation from '../../menu-items'
import { RouteGuard } from '../../util/auth/RouteGuard'

// ui
import Breadcrumbs from '../../ui-component/extended/Breadcrumbs'
import Header from './Header'
import Sidebar from './Sidebar'
import TransitionLayout from '../../ui-component/TransitionLayout'

const Main = styled('main', {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  ...theme.typography.mainContent,
  ...(!open && {
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    [theme.breakpoints.up('md')]: {
      marginLeft: -(drawerWidth - 20),
      width: `calc(100% - ${drawerWidth}px)`,
    },
    [theme.breakpoints.down('md')]: {
      marginLeft: '20px',
      width: `calc(100% - ${drawerWidth}px)`,
      padding: '16px',
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: '10px',
      width: `calc(100% - ${drawerWidth}px)`,
      padding: '16px',
      marginRight: '10px',
    },
  }),
  ...(open && {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    width: `calc(100% - ${drawerWidth}px)`,
    [theme.breakpoints.down('md')]: {
      marginLeft: '20px',
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: '10px',
    },
  }),
}))

const MainLayout = ({ children }) => {
  const theme = useAppTheme()
  const matchDownMd = useMediaQuery(theme.breakpoints.down('lg'))

  const leftDrawerOpened = useAppSelector((state) => state.theme.opened)
  const dispatch = useDispatch()
  const handleLeftDrawerToggle = () => {
    dispatch(setMenu(!leftDrawerOpened))
  }

  useEffect(() => {
    dispatch(setMenu(!matchDownMd))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [matchDownMd])

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar
        id="mainLayoutHeader"
        enableColorOnDark
        position="fixed"
        color="inherit"
        elevation={0}
        sx={{
          bgcolor: theme.palette.background.default,
          transition: leftDrawerOpened
            ? theme.transitions.create('width')
            : 'none',
        }}
      >
        <Toolbar>
          <Header handleLeftDrawerToggle={handleLeftDrawerToggle} />
        </Toolbar>
      </AppBar>

      <Sidebar
        drawerOpen={leftDrawerOpened}
        drawerToggle={handleLeftDrawerToggle}
      />

      <Main open={leftDrawerOpened}>
        <Breadcrumbs
          separator={IconChevronRight}
          navigation={navigation}
          icon
          title
          rightAlign
        />
        <RouteGuard>
          <TransitionLayout>{children}</TransitionLayout>
        </RouteGuard>
      </Main>
    </Box>
  )
}

export default MainLayout
