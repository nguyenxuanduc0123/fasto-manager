import { Avatar, ButtonBase, Chip } from '@mui/material'
import { Box } from '@mui/system'
import { IconMenu2 } from '@tabler/icons'
import { useNavigate } from 'react-router-dom'
import { useAppTheme } from '../../../features/theme/hooks'
import ProfileSection from './ProfileSection'

const Header = ({ handleLeftDrawerToggle }) => {
  const theme = useAppTheme()
  const navigate = useNavigate()

  return (
    <>
      <Box
        sx={{
          width: 228,
          display: 'flex',
          [theme.breakpoints.down('md')]: {
            width: 'auto',
          },
        }}
      >
        <Box
          component="span"
          onClick={() => navigate('/')}
          sx={{ display: { xs: 'none', md: 'block' }, flexGrow: 1 }}
        >
          <Chip
            avatar={<Avatar alt="Logo" src="images/logo2.png" />}
            variant="outlined"
            size="medium"
            sx={{
              height: '50px',
              width: '170px',
              border: 'transparent',
              fontSize: '20px',
              '& .MuiChip-avatar': {
                width: '65px',
                height: '65px',
              },
            }}
          />
        </Box>
        <ButtonBase sx={{ borderRadius: '12px', overflow: 'hidden' }}>
          <Avatar
            variant="rounded"
            sx={{
              ...theme.typography.commonAvatar,
              ...theme.typography.mediumAvatar,
              transition: 'all .2s ease-in-out',
              background: theme.palette.secondary.light,
              color: theme.palette.secondary.dark,
              '&:hover': {
                background: theme.palette.secondary.dark,
                color: theme.palette.secondary.light,
              },
            }}
            onClick={handleLeftDrawerToggle}
            color="inherit"
          >
            <IconMenu2 stroke={1.5} size="1.3rem" />
          </Avatar>
        </ButtonBase>
      </Box>

      <Box sx={{ flexGrow: 1 }} />
      <Box sx={{ flexGrow: 1 }} />

      <ProfileSection />
    </>
  )
}

export default Header
