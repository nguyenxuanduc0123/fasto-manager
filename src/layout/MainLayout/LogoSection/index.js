import Link from '../../../Link'

// material-ui
import { ButtonBase } from '@mui/material'

// project imports
import config from '../../../config'
import Logo from '../../../ui-component/Logo'

const LogoSection = () => (
  <ButtonBase disableRipple component={Link} href={config.defaultPath}>
    <Logo />
  </ButtonBase>
)

export default LogoSection
