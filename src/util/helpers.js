export const formatPrice = (price) => {
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export const handleRemoveImage =
  (name, formik) =>
  ({ pos }) => {
    const newImages = formik.values[name].filter((v, index) => index !== pos)
    const data = !newImages.some((obj) => obj.url === '')
      ? [...newImages, { url: '', type: '' }]
      : newImages
    formik.setFieldValue(name, data)
  }

export function isVideoURl(url) {
  if (!url || typeof url != 'string') return false

  const videoExtension = ['mp4', 'webm', 'mov', 'video/mp4', 'video/quicktime']
  const indexLast = url.lastIndexOf('.')
  const extension = url.slice(indexLast + 1, url.length)

  return videoExtension.includes(extension)
}
